#!/usr/bin/env python3

import os
import json
from glob import glob
from subprocess import run

import click


SOURCEDIR = "src/lang/src"
DESTDIR = "src/lang/compiled"
REFERENCE_LANG = "en"


def _get_langs():
    for source in glob(os.path.join(SOURCEDIR, "*.json")):
        yield os.path.splitext(os.path.basename(source))[0]


def _diff_msgs():
    with open(os.path.join(SOURCEDIR, f"{REFERENCE_LANG}.json")) as ref_file:
        ref_msgs = json.load(ref_file)
    for lang in _get_langs():
        if lang == REFERENCE_LANG:
            continue
        with open(os.path.join(SOURCEDIR, f"{lang}.json")) as msg_file:
            msgs = json.load(msg_file)
            msg_keys = set(msgs.keys())
        missing_msgs = set(ref_msgs.keys()) - msg_keys
        if missing_msgs:
            print(f"Missing messages in lang {lang}:")
            for msg in missing_msgs:
                print(f"  {msg}: {ref_msgs[msg]}")
        else:
            print("All strings are translated")
        extra_msgs = msg_keys - set(ref_msgs.keys())
        if extra_msgs:
            print(f"Additional messages in lang {lang}:")
            for msg in extra_msgs:
                print(f"  {msg}: {msgs[msg]}")


@click.group()
def cli():
    pass


@cli.command()
def extract():
    run(["npm", "run", "msg-extract"])
    _diff_msgs()


@cli.command()
def check():
    _diff_msgs()


@cli.command()
def compile():
    for lang in _get_langs():
        src_path = os.path.join(SOURCEDIR, f"{lang}.json")
        dest_path = os.path.join(DESTDIR, f"{lang}.json")
        if os.path.getmtime(src_path) > os.path.getmtime(dest_path):
            run(["npm", "run", "msg-compile", "--", src_path, "--out-file", dest_path])
    _diff_msgs()


if __name__ == "__main__":
    cli()
