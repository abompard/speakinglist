import StatsIcon from "@mui/icons-material/BarChart";
import ListIcon from "@mui/icons-material/FormatListNumbered";
import PrevTurnIcon from "@mui/icons-material/History";
import InfoIcon from "@mui/icons-material/Info";
import ParticipantIcon from "@mui/icons-material/People";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import { SnackbarProvider } from "notistack";
import { useIntl } from "react-intl";
import { Route, Link as RouterLink, Routes, useMatch } from "react-router-dom";
import { WithEventProps, withEvent } from "../features/event/Event";
import { Footer } from "../features/footer/Footer";
import { Header } from "../features/header/Header";
import { WithMe, WithMeAndEventProps } from "../features/me/Me";
import Page from "../features/page/Page";
import useIsLarge from "../features/responsiveness/useIsLarge";
import { EventInfo } from "./features/event_info/EventInfo";
import { Notifications } from "./features/notifications/Notifications";
import { ParticipantsList } from "./features/participants-list/ParticipantsList";
import { useParticipantsList } from "./features/participants-list/hook";
import { PastTurnsList } from "./features/past-turns/PastTurnsList";
import { Statistics } from "./features/statistics/Statistics";
import { EventSubtitle } from "./features/subtitle/EventSubtitle";
import { useTurnsList } from "./features/turns/Turns";
import { WaitingList } from "./features/waitinglist/WaitingList";

export default function Organizer() {
  const isLarge = useIsLarge();
  return (
    <SnackbarProvider maxSnack={3}>
      <Container disableGutters={!isLarge}>
        <Header />
        <OrganizerWithEvent />
        <Footer />
      </Container>
    </SnackbarProvider>
  );
}

const OrganizerWithEvent = withEvent((props: WithEventProps) => {
  return (
    <WithMe {...props} subscribe={false} component={OrganizerWithMeAndEvent} />
  );
}, true);

function OrganizerWithMeAndEvent(props: WithMeAndEventProps) {
  const routeMatch = useMatch("/e/:code/admin/:tabname");
  const intl = useIntl();
  const isLarge = useIsLarge();

  // Data
  const turnsProps = useTurnsList(props.event);
  const participantsListProps = useParticipantsList(props.event);
  //

  return (
    <Page
      title={[
        props.event.title,
        intl.formatMessage({
          defaultMessage: "Administration",
        }),
      ]}
    >
      <EventSubtitle event={props.event} />
      <Tabs
        aria-label="nav tabs"
        value={routeMatch?.params["tabname"] || ""}
        variant={isLarge ? undefined : "scrollable"}
        centered={isLarge}
        allowScrollButtonsMobile
      >
        <LinkTab
          label={isLarge ? intl.formatMessage({ defaultMessage: "Event Info" }) : <InfoIcon />}
          value=""
        />
        <LinkTab
          label={isLarge ? intl.formatMessage({ defaultMessage: "Waiting List" }) : <ListIcon />}
          value="turns"
        />
        <LinkTab
          label={isLarge ? intl.formatMessage({ defaultMessage: "Participants" }) : <ParticipantIcon />}
          value="participants"
        />
        <LinkTab
          label={isLarge ? intl.formatMessage({ defaultMessage: "Past turns" }) : <PrevTurnIcon />}
          value="past-turns"
        />
        <LinkTab
          label={isLarge ? intl.formatMessage({ defaultMessage: "Statistics" }) : <StatsIcon />}
          value="stats"
        />
      </Tabs>
      <div role="tabpanel">
        <Box p={2}>
          <Routes>
            <Route index element={<EventInfo event={props.event} />} />
            <Route
              path="turns"
              element={
                <WaitingList
                  event={props.event}
                  turnsProps={turnsProps}
                  startPolling={participantsListProps.startPolling}
                  stopPolling={participantsListProps.stopPolling}
                />
              }
            />
            <Route
              path="participants"
              element={
                <ParticipantsList
                  event={props.event}
                  participantsListProps={participantsListProps}
                />
              }
            />
            <Route
              path="past-turns"
              element={
                <PastTurnsList event={props.event} turnsProps={turnsProps} />
              }
            />
            <Route
              path="stats"
              element={
                <Statistics
                  event={props.event}
                  participants={participantsListProps.data || []}
                  turnsList={turnsProps.data || []}
                  loading={participantsListProps.loading || turnsProps.loading}
                />
              }
            />
          </Routes>
        </Box>
      </div>
      <Notifications event={props.event} />
    </Page>
  );
}

interface LinkTabProps {
  label: React.ReactNode;
  value: string;
}

function LinkTab(props: LinkTabProps) {
  return <Tab component={RouterLink} to={props.value} {...props} />;
}
