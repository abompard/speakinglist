import { gql } from "@apollo/client";

export const UPDATE_EVENT = gql`
  mutation UpdateEvent($eventCode: ID!, $properties: EventInput!) {
    event(code: $eventCode) {
      code
      update(properties: $properties) {
        code
        title
        socialCategories {
          id
          name
        }
      }
    }
  }
`;

export const DELETE_EVENT = gql`
  mutation DeleteEvent($eventCode: ID!) {
    event(code: $eventCode) {
      code
      delete
    }
  }
`;
