import { useMutation } from "@apollo/client";
import DeleteIcon from "@mui/icons-material/DeleteForever";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import React from "react";
import { FormattedMessage } from "react-intl";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { DELETE_EVENT } from "./queries";

export function DeleteEventButton({ event }: WithEventProps) {
  const [mutate, { loading, error }] = useMutation(DELETE_EVENT, {
    variables: { eventCode: event.code },
    update: (cache, { data }) => {
      cache.evict({
        id: cache.identify({ __typename: "Event", code: event.code }),
      });
      cache.gc();
      return null;
    },
    ignoreResults: true,
  });
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleConfirm = () => {
    mutate().then((result) => {
      result && setOpen(false);
    });
  };
  return (
    <>
      <Button
        variant="contained"
        color="error"
        onClick={handleClickOpen}
        disabled={loading}
        sx={{ mt: 4 }}
        startIcon={<DeleteIcon />}
        size="small"
      >
        <FormattedMessage defaultMessage="Delete event" />
      </Button>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="delete-event"
        aria-describedby="are you sure you want to delete this event"
      >
        <DialogTitle id="delete-event-dialog-title">
          <FormattedMessage defaultMessage="Delete this event?" />
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="delete-event-dialog-description">
            <FormattedMessage
              defaultMessage="
              Are you absolutely sure you want to delete this event?
              All the information about this event and its participants
              will be deleted forever, there is no going back.
            "
            />
          </DialogContentText>
          <ErrorAlert error={error} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="inherit">
            <FormattedMessage defaultMessage="No" />
          </Button>
          <Button onClick={handleConfirm} color="error" autoFocus>
            <FormattedMessage defaultMessage="Yes, delete forever." />
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
