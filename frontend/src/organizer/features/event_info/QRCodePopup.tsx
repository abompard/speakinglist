import CloseIcon from "@mui/icons-material/Close";
import QRIcon from "@mui/icons-material/Dashboard";
import Box from "@mui/material/Box";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";
import { FormattedMessage } from "react-intl";
import QRCode from "react-qr-code";
import ResponsiveButton from "../../../features/responsiveness/ResponsiveButton";

interface QRCodePopupProps {
  title: string;
  url: string;
  code: string;
}
export function QRCodePopup(props: QRCodePopupProps) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  return (
    <React.Fragment>
      <ResponsiveButton onClick={handleOpen} icon={<QRIcon />} color="inherit">
        <FormattedMessage defaultMessage="QR" />
      </ResponsiveButton>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        aria-labelledby="QR Code"
      >
        <Toolbar>
          <Typography variant="h6" sx={{ flex: 1 }}>
            {props.title}
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
            size="large"
          >
            <CloseIcon />
          </IconButton>
        </Toolbar>
        <DialogContent
          sx={{
            display: "flex",
            flexDirection: "column",
            flex: "1 1 auto",
          }}
        >
          <p>
            <FormattedMessage
              defaultMessage="Flash this code on your phone or tablet. You can also go to <code>speakinglist.net</code> and use the code:"
              values={{
                code: (chunks: React.ReactNode[]) => <code>{chunks}</code>,
              }}
            />
            <Typography
              color="primary"
              component="code"
              sx={{
                ml: 1,
                fontWeight: "bold",
                fontSize: "1.2rem",
              }}
            >
              {props.code}
            </Typography>
          </p>
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flex: "1 1 auto",
              pb: 5,
            }}
          >
            <QRCode value={props.url} />
          </Box>
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
