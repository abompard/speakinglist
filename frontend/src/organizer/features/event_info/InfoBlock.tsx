import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import React from "react";

interface InfoBlockProps {
  title: string | React.ReactNode;
  children: React.ReactNode;
}

export const InfoBlock = (props: InfoBlockProps) => {
  return (
    <Paper sx={{ p: 2 }}>
      <Box component="h4" sx={{ mt: 0, textAlign: "center" }}>
        {props.title}
      </Box>
      {props.children}
    </Paper>
  );
};
