import { ApolloError, useMutation } from "@apollo/client";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import FormControl from "@mui/material/FormControl";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";
import ListItemText from "@mui/material/ListItemText";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import { Field, Form, Formik, FormikHelpers, useFormikContext } from "formik";
import * as _ from "lodash";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import {
  SocialCategoryName,
  SOCIAL_CATEGORIES,
} from "../../../app/socialcategories";
import { FormErrors } from "../../../app/types";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { DurationField } from "./DurationField";
import { InfoBlock } from "./InfoBlock";
import { UPDATE_EVENT } from "./queries";

interface FormValues {
  title: string;
  turnmaxduration: number;
  socialcategories: SocialCategoryName[];
}

export const EventSettings = ({ event }: WithEventProps) => {
  const [mutate, { error }] = useMutation(UPDATE_EVENT);
  const intl = useIntl();
  const availableCategories = Object.keys(SOCIAL_CATEGORIES);
  return (
    <InfoBlock title={<FormattedMessage defaultMessage="Settings" />}>
      <Formik
        initialValues={{
          title: event.title || "",
          turnmaxduration: event.turnMaxDuration,
          socialcategories: event.socialCategories.map((sc) => sc.name) || [],
        }}
        validate={(values) => {
          const errors: FormErrors<FormValues> = {};
          if (!values.title) {
            errors.title = intl.formatMessage({ defaultMessage: "Required" });
          }
          (values.socialcategories || []).forEach((v) => {
            if (!availableCategories.includes(v)) {
              errors.socialcategories = intl.formatMessage(
                { defaultMessage: "Category {name} is not available" },
                { name: v }
              );
            }
          });
          return errors;
        }}
        onSubmit={(
          values: FormValues,
          { setSubmitting, setTouched }: FormikHelpers<FormValues>
        ) => {
          mutate({
            variables: {
              eventCode: event.code,
              properties: {
                title: values.title,
                turnMaxDuration: values.turnmaxduration,
                socialCategories: values.socialcategories,
              },
            },
          }).then(
            (result) => {
              setSubmitting(false);
              setTouched(_.mapValues(values, () => false));
            },
            (error) => {
              setSubmitting(false);
              throw error;
            }
          );
        }}
      >
        {({ isSubmitting, errors, values }) => (
          <Form>
            <Box mt={3}>
              <FormControl variant="standard">
                <Field
                  as={TextField}
                  name="title"
                  label={intl.formatMessage({ defaultMessage: "Title" })}
                  error={!!errors.title}
                  helperText={errors.title}
                />
              </FormControl>
            </Box>
            <Box mt={3}>
              <FormControl variant="standard" sx={{ minWidth: "10em" }}>
                <Field
                  as={DurationField}
                  name="turnmaxduration"
                  label={intl.formatMessage({
                    defaultMessage: "Max turn duration",
                  })}
                  error={!!errors.turnmaxduration}
                  helperText={errors.turnmaxduration}
                />
              </FormControl>
            </Box>
            <Box component="p" mt={3}>
              <FormattedMessage
                defaultMessage="
              Choose which social categories you want to ask the participants
              upon registration:
              "
              />
            </Box>
            <FormControl variant="standard" sx={{ minWidth: "12em" }}>
              <InputLabel id="socialcategories-label">
                <FormattedMessage defaultMessage="Social Categories" />
              </InputLabel>
              <Select
                variant="standard"
                labelId="socialcategories-label"
                id="socialcategories"
                multiple
                input={<Field name="socialcategories" as={Input} />}
                renderValue={(selected) =>
                  (selected as SocialCategoryName[])
                    .map((n): string =>
                      intl.formatMessage(SOCIAL_CATEGORIES[n].title)
                    )
                    .join(", ")
                }>
                {availableCategories.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox
                      checked={(values.socialcategories || []).includes(
                        name as SocialCategoryName
                      )}
                    />
                    <ListItemText
                      primary={intl.formatMessage(
                        SOCIAL_CATEGORIES[name as SocialCategoryName].title
                      )}
                    />
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <Box component="p" mt={3}>
              <SaveButton requestError={error} />
            </Box>
          </Form>
        )}
      </Formik>
    </InfoBlock>
  );
};

interface SaveButtonProps {
  requestError?: ApolloError;
}
function SaveButton({ requestError }: SaveButtonProps) {
  const { touched, isSubmitting } = useFormikContext();
  const isTouched = Object.values(touched).filter((v) => !!v).length > 0;
  return (
    <React.Fragment>
      <Button
        variant={isTouched ? "contained" : "outlined"}
        type="submit"
        disabled={isSubmitting}
      >
        <FormattedMessage defaultMessage="Save" />
      </Button>
      <ErrorAlert error={requestError} />
    </React.Fragment>
  );
}
