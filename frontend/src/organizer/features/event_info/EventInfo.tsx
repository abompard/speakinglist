import HowToIcon from "@mui/icons-material/InfoOutlined";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import Tooltip from "@mui/material/Tooltip";
import Typography from "@mui/material/Typography";
import {
  FormattedDate,
  FormattedMessage,
  FormattedRelativeTime,
  FormattedTime,
  useIntl,
} from "react-intl";
import { Link as RouterLink } from "react-router-dom";
import { settings } from "../../../app/config";
import { CopyButton } from "../../../features/copy-button/CopyButton";
import { WithEventProps } from "../../../features/event/Event";
import { EventStateAction } from "../eventstate/EventState";
import { DeleteEventButton } from "./DeleteEventButton";
import { EventSettings } from "./EventSettings";
import { InfoBlock } from "./InfoBlock";
import { QRCodePopup } from "./QRCodePopup";

export const EventInfo = ({ event }: WithEventProps) => {
  return (
    <Grid container spacing={2}>
      <Grid container direction="column" spacing={2} item xs={12} md={6}>
        <Grid item>
          <GeneralInfo event={event} />
        </Grid>
        <Grid item>
          <EventStatus event={event} />
        </Grid>
      </Grid>
      <Grid container direction="column" spacing={2} item xs={12} md={6}>
        <Grid item>
          <EventSettings event={event} />
        </Grid>
      </Grid>
    </Grid>
  );
};

function GeneralInfo({ event }: WithEventProps) {
  const eventUrl = `${settings.PUBLIC_URL}/e/${event.code}`;
  return (
    <InfoBlock title={<FormattedMessage defaultMessage="Info" />}>
      <p>
        <FormattedMessage defaultMessage="Code:" />{" "}
        <Box component="code" sx={{ fontWeight: "bold" }} color="info.main">
          {event.code}
        </Box>
      </p>
      <p>
        <FormattedMessage defaultMessage="Share this URL:" />{" "}
        <input value={eventUrl} readOnly={true} />
        <Box ml={1} component="span">
          <CopyButton value={eventUrl} />
          <QRCodePopup title={event.title} code={event.code} url={eventUrl} />
        </Box>
      </p>
      <Box component="p" mb={0}>
        <Typography color="secondary" component="span">
          <FormattedMessage defaultMessage="Admin code:" />{" "}
        </Typography>
        <Typography
          color="secondary"
          component="code"
          sx={{ fontWeight: "bold" }}
        >
          {event.adminCode}
        </Typography>{" "}
        <Box ml={1} component="span">
          <CopyButton value={event.adminCode} size="small" />
        </Box>
      </Box>
      <Box component="p" mt={0}>
        <em>
          <FormattedMessage defaultMessage="(only for organizers, do not share this with the participants)" />
        </em>
      </Box>
      <Box component="p" color="info.main">
        <Link component={RouterLink} to="/howto" underline="hover">
          <HowToIcon
            fontSize="small"
            sx={{ verticalAlign: "bottom", mr: 0.5 }}
          />
          <FormattedMessage defaultMessage="How does this work?" />
        </Link>
      </Box>
    </InfoBlock>
  );
}

function EventStatus({ event }: WithEventProps) {
  const intl = useIntl();
  return (
    <InfoBlock title={<FormattedMessage defaultMessage="Status" />}>
      <EventStateAction event={event} />
      <p>
        <FormattedMessage defaultMessage="Started at:" />{" "}
        <EventDate
          date={event.startedAt}
          fallback={intl.formatMessage({ defaultMessage: "not started" })}
        />
      </p>
      {event.startedAt && (
        <p>
          <FormattedMessage defaultMessage="Ended at:" />{" "}
          <EventDate
            date={event.endedAt}
            fallback={intl.formatMessage({ defaultMessage: "still running" })}
          />
        </p>
      )}
      {event.startedAt && event.endedAt && (
        <Alert severity="info">
          <FormattedMessage
            defaultMessage="The event will be automatically deleted {time}. Don't forget to download the statistics before then if you want to back them up."
            values={{
              time: <EventDate date={event.expiresAt} />,
            }}
          />
        </Alert>
      )}
      {event.endedAt || !event.startedAt ? (
        <DeleteEventButton event={event} />
      ) : null}
    </InfoBlock>
  );
}

interface EventDateProps {
  date: string | null;
  fallback?: string;
}
function EventDate({ date, fallback }: EventDateProps) {
  if (!date) return !!fallback ? <em>{fallback}</em> : <span>?</span>;
  return (
    <Tooltip
      title={
        <span>
          <FormattedDate value={date} /> <FormattedTime value={date} />
        </span>
      }
      placement="top"
    >
      <span style={{ textDecoration: "underline dotted" }}>
        <FormattedRelativeTime
          value={Math.round((Date.parse(date) - Date.now()) / 1000)}
          numeric="auto"
          updateIntervalInSeconds={1}
        />
      </span>
    </Tooltip>
  );
}
