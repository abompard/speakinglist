import TextField from "@mui/material/TextField";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { DesktopTimePicker } from "@mui/x-date-pickers/DesktopTimePicker";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import addSeconds from "date-fns/addSeconds";
import getMinutes from "date-fns/getMinutes";
import getSeconds from "date-fns/getSeconds";
import isValid from "date-fns/isValid";
import { FieldInputProps, useFormikContext } from "formik";
import { useIntl } from "react-intl";
import { durationToText } from "../../../app/utils";

export function DurationField(props: FieldInputProps<number>) {
  const { setFieldValue } = useFormikContext();
  const intl = useIntl();
  const handleChange = (newValue: any) => {
    if (!isValid(newValue)) {
      return;
    }
    if (!newValue) {
      setFieldValue(props.name, 0);
      return;
    }
    const minutes = getMinutes(newValue);
    const seconds = getSeconds(newValue);
    setFieldValue(props.name, minutes * 60 + seconds);
  };
  const value = addSeconds(new Date(0), props.value);
  const helperText =
    props.value === 0
      ? intl.formatMessage({
          defaultMessage: "No limit",
        })
      : intl.formatMessage(
          { defaultMessage: "Warning color will be at {duration}" },
          { duration: durationToText(props.value - props.value / 5) }
        );
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DesktopTimePicker
        {...props}
        ampm={false}
        openTo="minutes"
        views={["minutes", "seconds"]}
        format="mm:ss"
        timeSteps={{ minutes: 1, seconds: 5 }}
        value={value}
        onChange={handleChange}
        slots={{
          textField: (params) => (
            <TextField variant="standard" {...params} helperText={helperText} />
          ),
        }}
      />
    </LocalizationProvider>
  );
}
