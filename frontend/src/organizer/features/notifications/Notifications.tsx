import { useSubscription } from "@apollo/client";
import { useSnackbar } from "notistack";
import { defineMessages, useIntl } from "react-intl";
import { Notification } from "../../../app/types";
import { WithEventProps } from "../../../features/event/Event";
import { SUBSCRIBE_NOTIFICATIONS } from "./queries";

const MESSAGES = defineMessages({
  PARTICIPANT_ADDED: { defaultMessage: "Participant {name} has joined." },
  WAITING_LIST_MOVED: { defaultMessage: "Waiting list was reorganized." },
  EVENT_STATE_CHANGED: { defaultMessage: "Event changed state: {action}." },
  TURN_REQUESTED: {
    defaultMessage: "Participant {participant_name} requested a turn.",
  },
  TURN_CANCELLED: {
    defaultMessage: "Participant {participant_name} has cancelled their turn.",
  },
  TURN_STARTED: {
    defaultMessage: "Participant {participant_name}'s turn has started.",
  },
  TURN_STOPPED: {
    defaultMessage: "Participant {participant_name}'s turn has ended.",
  },
});

export function Notifications(props: WithEventProps) {
  const { enqueueSnackbar } = useSnackbar();
  const intl = useIntl();

  useSubscription(SUBSCRIBE_NOTIFICATIONS, {
    variables: { eventCode: props.event.code },
    onData: ({ data: subscriptionData }) => {
      console.log("Got notification:", subscriptionData.data.notifications);
      const notif: Notification = subscriptionData.data.notifications;
      let message: string = notif.msgid;
      if (!MESSAGES[notif.msgid]) {
        console.log("Unsupported message id:", notif.msgid);
      } else {
        const values = Object.fromEntries(
          notif.values.map(({ name, value }) => [name, value])
        );
        message = intl.formatMessage(MESSAGES[notif.msgid], values);
      }
      enqueueSnackbar(message);
    },
  });
  return null;
}
