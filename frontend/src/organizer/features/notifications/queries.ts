import { gql } from "@apollo/client";

export const SUBSCRIBE_NOTIFICATIONS = gql`
  subscription OnNotifications($eventCode: ID!) {
    notifications(eventCode: $eventCode) {
      msgid
      values {
        name
        value
      }
    }
  }
`;
