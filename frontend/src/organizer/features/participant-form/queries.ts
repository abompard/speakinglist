import { gql } from "@apollo/client";

export const ADD_PARTICIPANT = gql`
  mutation AddParticipant(
    $eventCode: ID!
    $name: String!
    $socialCategories: [SocialCategoryInput]
    $me: Boolean
  ) {
    event(code: $eventCode) {
      code
      addParticipant(
        name: $name
        socialCategories: $socialCategories
        me: $me
      ) {
        id
        name
        addedByOrga
        turns {
          id
          startedAt
          endedAt
          requestedAt
        }
        socialCategories {
          id
          name
          value
        }
      }
    }
  }
`;
