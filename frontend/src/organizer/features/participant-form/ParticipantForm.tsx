import { DocumentNode, useMutation } from "@apollo/client";
import AddIcon from "@mui/icons-material/Add";
import EditIcon from "@mui/icons-material/Edit";
import AddParticipantIcon from "@mui/icons-material/PersonAdd";
import Box from "@mui/material/Box";
import Button, { ButtonTypeMap } from "@mui/material/Button";
import Checkbox from "@mui/material/Checkbox";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormHelperText from "@mui/material/FormHelperText";
import TextField from "@mui/material/TextField";
import { Field, Form, Formik, FormikHelpers } from "formik";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { SocialCategoryName } from "../../../app/socialcategories";
import { FormErrors, ParticipantAdmin } from "../../../app/types";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { GET_ME } from "../../../features/me/queries";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import {
  SocialCategoriesFormFields,
  getInitialSCValues,
  valuesToVariables,
} from "../../../features/social-categories/SocialCategories";
import { UPDATE_PARTICIPANT } from "../../../member/features/participant/queries";
import { ADD_PARTICIPANT } from "./queries";

interface AddParticipantProps extends WithEventProps {
  me: ParticipantAdmin[];
}
export function AddParticipant(props: AddParticipantProps) {
  const isLarge = useIsLarge();
  return (
    <ParticipantButtonWithDialog
      {...props}
      buttonText={isLarge ? <FormattedMessage defaultMessage="Add a participant" /> : <AddParticipantIcon />}
      buttonIcon={isLarge ? <AddIcon /> : undefined}
      buttonVariant="outlined"
      mutation={ADD_PARTICIPANT}
      dialogTitle={<FormattedMessage defaultMessage="Add a new Participant" />}
      dialogButtonText={<FormattedMessage defaultMessage="Add" />}
    />
  );
}

interface EditParticipantProps extends WithEventProps {
  participant: ParticipantAdmin;
  onClose?(): void;
  me: ParticipantAdmin[];
}
export function EditParticipant(props: EditParticipantProps) {
  return (
    <ParticipantButtonWithDialog
      event={props.event}
      me={props.me}
      participant={props.participant}
      buttonText={<FormattedMessage defaultMessage="Edit" />}
      buttonIcon={<EditIcon />}
      buttonVariant="contained"
      mutation={UPDATE_PARTICIPANT}
      dialogTitle={<FormattedMessage defaultMessage="Edit a Participant" />}
      dialogButtonText={<FormattedMessage defaultMessage="Edit" />}
      onClose={props.onClose}
    />
  );
}

export function AddMyself(props: AddParticipantProps) {
  return (
    <ParticipantButtonWithDialog
      event={props.event}
      me={props.me}
      buttonText={<FormattedMessage defaultMessage="Add yourself" />}
      buttonVariant="text"
      mutation={ADD_PARTICIPANT}
      dialogTitle={
        <FormattedMessage defaultMessage="Add yourself as a participant" />
      }
      dialogButtonText={<FormattedMessage defaultMessage="Add" />}
      forceMe
    />
  );
}

interface CommonParticipantDialogProps extends WithEventProps {
  participant?: ParticipantAdmin | null;
  me: ParticipantAdmin[];
  mutation: DocumentNode;
}

interface ParticipantButtonWithDialogProps
  extends CommonParticipantDialogProps {
  buttonText: React.ReactNode;
  buttonIcon?: React.ReactNode;
  buttonVariant?: ButtonTypeMap["props"]["variant"];
  dialogTitle: React.ReactNode;
  dialogButtonText: React.ReactNode;
  onClose?(): void;
  forceMe?: boolean;
}
export function ParticipantButtonWithDialog(
  props: ParticipantButtonWithDialogProps
) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
    if (props.onClose) props.onClose();
  };

  return (
    <React.Fragment>
      <Button
        variant={props.buttonVariant}
        onClick={handleClickOpen}
        color="inherit"
        size="small"
        startIcon={props.buttonIcon}
      >
        {props.buttonText}
      </Button>
      <ParticipantFormDialog
        isOpen={open}
        onClose={handleClose}
        title={props.dialogTitle}
      >
        <ParticipantForm
          event={props.event}
          me={props.me}
          participant={props.participant}
          onClose={handleClose}
          mutation={props.mutation}
          buttonText={props.dialogButtonText}
          forceMe={props.forceMe}
        />
      </ParticipantFormDialog>
    </React.Fragment>
  );
}

interface FormValues {
  name: string;
  sc: { [key in SocialCategoryName]?: string };
  me: boolean;
}

interface ParticipantFormDialogProps {
  isOpen: boolean;
  onClose(): void;
  title: React.ReactNode;
  children: React.ReactNode;
}

export function ParticipantFormDialog(props: ParticipantFormDialogProps) {
  return (
    <Dialog
      open={props.isOpen}
      onClose={props.onClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">{props.title}</DialogTitle>
      {props.children}
    </Dialog>
  );
}

interface ParticipantFormProps extends CommonParticipantDialogProps {
  onClose(): void;
  buttonText: React.ReactNode;
  forceMe?: boolean;
}

function ParticipantForm(props: ParticipantFormProps) {
  /*
    const mutationUpdate: MutationUpdaterFn = (cache, { data }) => {
      const eventRef = cache.identify({
        __typename: "Event",
        code: props.event.code,
      });
      cache.modify({
        id: eventRef,
        fields: {
          participants(existingRefs = []) {
            const newParticipant = data!.addParticipant;
            if (
              existingRefs
                .map((r: { __ref: string }) => r.__ref)
                .includes(`Participant:${newParticipant.id}`)
            ) {
              // Array has already been updated by the subscription
              console.log("Already updated");
              return existingRefs;
            }
            console.log("before writeFragment");
            console.log(existingRefs);
            const newRef = cache.writeFragment({
              data: newParticipant,
              fragment: gql`
                fragment NewParticipant on Participant {
                  id
                  name
                  turns {
                    id
                  }
                }
              `,
            });
            console.log("after writeFragment");
            return [newRef, ...existingRefs];
          },
        },
      });
    };
    */
  const intl = useIntl();
  const [mutate, { loading, error }] = useMutation(props.mutation, {
    // The subscription will update the list
    //update: mutationUpdate,
  });
  const isMe =
    !!props.participant &&
    props.me.map((p) => p.id).includes(props.participant.id);
  return (
    <Formik
      initialValues={{
        name: props.participant ? props.participant.name : "",
        sc: getInitialSCValues(props.event, props.participant),
        me: !!isMe || !!props.forceMe,
      }}
      validate={(values) => {
        const errors: FormErrors<FormValues> = {};
        if (!values.name) {
          errors.name = intl.formatMessage({ defaultMessage: "Required" });
        }
        return errors;
      }}
      onSubmit={(
        values: FormValues,
        { setSubmitting }: FormikHelpers<FormValues>
      ) => {
        const toMaybeRefetch = {
          query: GET_ME,
          variables: { eventCode: props.event.code },
        };
        mutate({
          variables: {
            eventCode: props.event.code,
            participantId: props.participant ? props.participant.id : undefined,
            name: values.name,
            socialCategories: valuesToVariables(values),
            me: values.me,
          },
          // Refetch the "Me" object if the value of the "me" field changed
          refetchQueries: values.me !== !!isMe ? [toMaybeRefetch] : undefined,
        }).then(
          () => {
            // We're closing the dialog anyway, everything will be unmounted, no need to finish the submission cycle.
            // setSubmitting(false);
            props.onClose();
          },
          () => {
            setSubmitting(false);
          }
        );
      }}
    >
      {({ isSubmitting, errors, values }) => (
        <Form>
          <DialogContent>
            <Box mb={2}>
              <Field
                as={TextField}
                name="name"
                label={intl.formatMessage({ defaultMessage: "Name" })}
                error={!!errors.name}
                autoFocus={true}
                helperText={
                  errors.name ||
                  intl.formatMessage({
                    defaultMessage: "Use the same name as in the video call.",
                  })
                }
              />
            </Box>
            <Box mb={4}>
              <SocialCategoriesFormFields event={props.event} errors={errors} />
            </Box>
            {!props.forceMe && (
              <Box mb={3}>
                <FormControlLabel
                  control={
                    <Field as={Checkbox} name="me" checked={values.me} />
                  }
                  label={intl.formatMessage({ defaultMessage: "It's me!" })}
                />
                <FormHelperText>{errors.me}</FormHelperText>
              </Box>
            )}
            <ErrorAlert error={error} />
          </DialogContent>
          <DialogActions>
            <Button onClick={props.onClose} color="inherit">
              <FormattedMessage defaultMessage="Cancel" />
            </Button>
            <Button
              type="submit"
              color="primary"
              variant="contained"
              disabled={isSubmitting || loading}
            >
              {props.buttonText}
            </Button>
          </DialogActions>
        </Form>
      )}
    </Formik>
  );
}
