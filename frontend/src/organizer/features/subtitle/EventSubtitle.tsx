import { useQuery } from "@apollo/client";
import Alert from "@mui/material/Alert";
import Box from "@mui/material/Box";
import { FormattedMessage } from "react-intl";
import { ParticipantAdmin } from "../../../app/types";
import { WithEventProps } from "../../../features/event/Event";
import { GET_ME } from "../../../features/me/queries";
import { AddMyself } from "../participant-form/ParticipantForm";

export function EventSubtitle({ event }: WithEventProps) {
  const { data } = useQuery(GET_ME, {
    variables: { eventCode: event.code },
  });
  const me: ParticipantAdmin[] = data ? data.event.me.participants : [];
  return (
    <Box px={1} pb={2}>
      {me.length === 0 && (
        <Alert severity="info" action={<AddMyself event={event} me={me} />}>
          <FormattedMessage defaultMessage="You are not a participant yet." />
        </Alert>
      )}
    </Box>
  );
}
