import { gql } from "@apollo/client";

export const SET_EVENT_STATE = gql`
  mutation SetEventState($eventCode: ID!, $action: String!) {
    event(code: $eventCode) {
      code
      setState(action: $action) {
        code
        startedAt
        endedAt
        isPaused
      }
    }
  }
`;
