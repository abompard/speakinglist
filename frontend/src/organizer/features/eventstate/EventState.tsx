import { useMutation } from "@apollo/client";
import PauseIcon from "@mui/icons-material/Pause";
import PlayIcon from "@mui/icons-material/PlayArrow";
import StopIcon from "@mui/icons-material/Stop";
import Button, { ButtonTypeMap } from "@mui/material/Button";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Event } from "../../../app/types";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { SET_EVENT_STATE } from "./queries";

export function EventStateAction({ event }: WithEventProps) {
  let Component = event.endedAt
    ? EventStopped
    : event.isPaused
    ? EventPaused
    : event.startedAt
    ? EventRunning
    : EventNotStarted;
  return (
    <div>
      <Component event={event} />
    </div>
  );
}

function EventStopped({ event }: WithEventProps) {
  const intl = useIntl();
  return (
    <div>
      <p>
        <FormattedMessage defaultMessage="The event is stopped" />
      </p>
      <EventActionButton
        label={intl.formatMessage({ defaultMessage: "Restart" })}
        event={event}
        action="restart"
      />
    </div>
  );
}

function EventPaused({ event }: WithEventProps) {
  const intl = useIntl();
  return (
    <div>
      <p>
        <FormattedMessage defaultMessage="The event is paused" />
      </p>
      <EventActionButton
        label={intl.formatMessage({ defaultMessage: "Continue" })}
        event={event}
        action="unpause"
      />
    </div>
  );
}

function EventRunning({ event }: WithEventProps) {
  const intl = useIntl();
  return (
    <div>
      <p>
        <FormattedMessage defaultMessage="The event is on!" />
      </p>
      <EventActionButton
        label={intl.formatMessage({ defaultMessage: "Pause" })}
        event={event}
        action="pause"
        variant="outlined"
      />
      <EventActionButton
        label={intl.formatMessage({ defaultMessage: "Stop" })}
        event={event}
        action="stop"
      />
    </div>
  );
}

function EventNotStarted({ event }: WithEventProps) {
  const intl = useIntl();
  return (
    <div>
      <p>
        <FormattedMessage defaultMessage="The event has not started yet" />
      </p>
      <EventActionButton
        label={intl.formatMessage({ defaultMessage: "Start" })}
        event={event}
        action="start"
      />
    </div>
  );
}

interface EventActionButtonProps {
  label: string;
  event: Event;
  action: "start" | "stop" | "pause" | "unpause" | "restart";
  color?: ButtonTypeMap["props"]["color"];
  variant?: ButtonTypeMap["props"]["variant"];
}

function EventActionButton(props: EventActionButtonProps) {
  const [mutate, { loading, error }] = useMutation(SET_EVENT_STATE);
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        action: props.action,
      },
    });
  let Icon = PlayIcon;
  switch (props.action) {
    case "start" || "unpause" || "restart":
      Icon = PlayIcon;
      break;
    case "stop":
      Icon = StopIcon;
      break;
    case "pause":
      Icon = PauseIcon;
      break;
  }
  return (
    <React.Fragment>
      <Button
        variant={props.variant || "contained"}
        color={props.color || "primary"}
        onClick={onClick}
        disabled={loading}
        sx={{ mr: 1 }}
        startIcon={<Icon />}
      >
        {props.label}
      </Button>
      <ErrorAlert error={error} />
    </React.Fragment>
  );
}
