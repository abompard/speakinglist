import TopIcon from "@mui/icons-material/VerticalAlignTop";
import { Tooltip } from "@mui/material";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";
import Zoom from "@mui/material/Zoom";
import { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";

export function ScrollToTop() {
  const [isVisible, setIsVisible] = useState(false);

  // Show button when page is scrolled up to given distance
  const toggleVisibility = () => {
    if (window.pageYOffset > 200) {
      setIsVisible(true);
    } else {
      setIsVisible(false);
    }
  };

  // Set the top cordinate to 0
  // make scrolling smooth
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    window.addEventListener("scroll", toggleVisibility);
    return () => window.removeEventListener("scroll", toggleVisibility);
  }, []);

  const theme = useTheme();
  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  return (
    <Box position="fixed" bottom={30} right={30}>
      <Zoom
        in={isVisible}
        timeout={transitionDuration}
        style={{
          transitionDelay: `${isVisible ? transitionDuration.exit : 0}ms`,
        }}
      >
        <Tooltip
          title={<FormattedMessage defaultMessage="Scroll to top" />}
          placement="left"
        >
          <IconButton
            aria-label="scroll to top"
            onClick={scrollToTop}
            sx={{
              border: "1px solid #ddd",
              backgroundColor: "#fcfcfc",
              "&:hover": {
                backgroundColor: "#f7f7f7",
              },
            }}
            size="large">
            <TopIcon />
          </IconButton>
        </Tooltip>
      </Zoom>
    </Box>
  );
}
