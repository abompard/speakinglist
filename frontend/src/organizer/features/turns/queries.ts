import { gql } from "@apollo/client";

export const GET_TURNS = gql`
  query GetTurns($eventCode: ID!) {
    event(code: $eventCode) {
      code
      turns {
        id
        position
        startedAt
        endedAt
        requestedAt
        participantId
      }
    }
  }
`;

export const SUBSCRIBE_TURNS = gql`
  subscription OnTurnChanged($eventCode: ID!) {
    turns(eventCode: $eventCode) {
      id
      position
      startedAt
      endedAt
      requestedAt
      participantId
    }
  }
`;

export const GET_PARTICIPANT_FRAGMENT = gql`
  fragment ThisParticipant on Participant {
    id
    name
    isActive
    addedByOrga
    isOrga
    turns {
      id
      startedAt
      endedAt
      requestedAt
    }
    socialCategories {
      id
      name
      value
    }
  }
`;
