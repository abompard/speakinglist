import { ApolloClient, ApolloError, useQuery } from "@apollo/client";
import { useEffect } from "react";
import { Event, TurnAdmin } from "../../../app/types";
import {
  GET_PARTICIPANT_FRAGMENT,
  GET_TURNS,
  SUBSCRIBE_TURNS,
} from "./queries";

function populateTurnParticipantFromCache(
  client: ApolloClient<any>,
  turn: TurnAdmin
) {
  // Get the participant from the cache, there will be more data.
  const cachedParticipant = client.readFragment({
    id: client.cache.identify({
      __typename: "Participant",
      id: turn.participantId,
    }),
    fragment: GET_PARTICIPANT_FRAGMENT,
  });
  return {
    ...turn,
    participant: cachedParticipant,
  };
}

export function useTurnsList(event: Event) {
  const eventCode = event.code;
  let { loading, error, data, subscribeToMore, refetch, client } = useQuery(
    GET_TURNS,
    {
      variables: { eventCode },
    }
  );
  useEffect(
    () =>
      subscribeToMore({
        document: SUBSCRIBE_TURNS,
        variables: { eventCode },
        updateQuery: (prev, { subscriptionData }) => {
          console.log("Got turns update", subscriptionData.data);
          if (!subscriptionData.data) return prev;
          const newTurns = subscriptionData.data.turns;
          return Object.assign({}, prev, {
            event: {
              ...prev.event,
              // No need to populateTurnParticipantFromCache here, because it will trigger an update
              // of the useQuery call above.
              turns: newTurns,
            },
          });
        },
      }),
    [eventCode, subscribeToMore]
  );
  const turns: TurnAdmin[] | null = data
    ? data.event.turns
        .map((t: TurnAdmin) => populateTurnParticipantFromCache(client, t))
        .filter(
          // If the participant is null, it has been deleted. Filter out those turns
          (t: TurnAdmin) => t.participant !== null
        )
    : null;
  if (!error && !data) {
    error = new ApolloError({ errorMessage: "could not get the turns list" });
  }
  return { loading, error, data: turns, refetch };
}
