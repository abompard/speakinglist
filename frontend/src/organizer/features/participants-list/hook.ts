import { ApolloError, StoreObject, useQuery } from "@apollo/client";
import { useEffect } from "react";
import { Event, ParticipantAdmin } from "../../../app/types";
import { GET_PARTICIPANTS, SUBSCRIBE_PARTICIPANTS } from "./queries";

const LAST_SEEN_INTERVAL = 20 * 1000;

export function useParticipantsList(event: Event) {
  const eventCode = event.code;
  let {
    loading,
    error,
    data,
    subscribeToMore,
    refetch,
    client,
    //startPolling,
    //stopPolling,
  } = useQuery(GET_PARTICIPANTS, {
    variables: { eventCode },
    //pollInterval: LAST_SEEN_INTERVAL,
  });

  /* Redo startPolling and stopPolling because of this bug:
   * https://github.com/apollographql/apollo-client/issues/7221
   * https://github.com/apollographql/apollo-client/issues/6391
   */
  const getPollingFuncs = () => {
    let intervalId: NodeJS.Timeout | null = null;
    const startPolling = (interval: number) => {
      intervalId = setInterval(() => refetch(), interval);
    };
    const stopPolling = () => {
      if (intervalId) clearInterval(intervalId);
    };
    return { startPolling, stopPolling };
  };
  const { startPolling, stopPolling } = getPollingFuncs();
  useEffect(() => {
    startPolling(LAST_SEEN_INTERVAL);
    return stopPolling;
  });
  /* Done redoing startPolling & stopPolling */

  useEffect(() => {
    subscribeToMore({
      document: SUBSCRIBE_PARTICIPANTS,
      variables: { eventCode },
      updateQuery: (prev, { subscriptionData }) => {
        if (!subscriptionData.data) return prev;
        console.log(
          "Got participants list update",
          subscriptionData.data.participants
        );
        const newData = subscriptionData.data.participants;
        // Check if participants have been deleted
        const newIds = newData.map((p: ParticipantAdmin) => p.id);
        const removed = prev.event.participants.filter(
          (p: StoreObject) => newIds.indexOf(p.id) === -1
        );
        removed.forEach((p: StoreObject) => {
          console.log("Removing deleted participant", p.name, p.id);
          client.cache.evict({
            id: client.cache.identify(p),
          });
        });
        if (removed.length > 0) {
          client.cache.gc();
        }
        // Return the new data
        return Object.assign({}, prev, {
          event: {
            ...prev.event,
            participants: newData,
          },
        });
      },
    });
  }, [eventCode, subscribeToMore, client]);

  const participants: ParticipantAdmin[] | null = data
    ? data.event.participants
    : null;
  if (!error && !data) {
    error = new ApolloError({
      errorMessage: "could not get the participants list",
    });
  }

  return {
    loading,
    error,
    data: participants,
    refetch,
    startPolling: () => startPolling(LAST_SEEN_INTERVAL),
    stopPolling: stopPolling,
  };
}
