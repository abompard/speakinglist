import { useMutation } from "@apollo/client";
import DeleteIcon from "@mui/icons-material/Delete";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import IconButton from "@mui/material/IconButton";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import React from "react";
import { FormattedMessage } from "react-intl";
import { ParticipantAdmin } from "../../../app/types";
import { getPreviousTurns } from "../../../app/utils";
import { WithEventProps } from "../../../features/event/Event";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import { EditParticipant } from "../participant-form/ParticipantForm";
import { ParticipantIsOrga } from "./ParticipantsIsOrga";
import { DELETE_PARTICIPANT } from "./queries";

interface ActionProps extends WithEventProps {
  participant: ParticipantAdmin;
  me: ParticipantAdmin[];
}

export function Edit(props: ActionProps) {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const isLarge = useIsLarge();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const isMe =
    !!props.participant &&
    props.me.map((p) => p.id).includes(props.participant.id);

  return (
    <Box mr={1} component="span">
      <IconButton
        aria-label="more"
        aria-haspopup="true"
        onClick={handleClick}
        size="small"
      >
        {open ? <ExpandLess /> : <ExpandMore />}
      </IconButton>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        variant="menu"
      >
        <MenuItem sx={{ display: "block" }} dense>

                {isMe ? (
                  isLarge && (<em>
            <FormattedMessage defaultMessage="(it's you)" />
          </em>)
        ) : (
          <Box display="flex" justifyContent="space-between">
          <Box>
            <FormattedMessage defaultMessage="Organizer:" />{"   "}
          </Box>
          <ParticipantIsOrga
            event={props.event}
            participant={props.participant}
            />
          </Box>
        )}
        </MenuItem>

        <MenuItem sx={{ display: "block" }} dense>
          <EditParticipant {...props} onClose={handleClose} />
        </MenuItem>
        <MenuItem sx={{ display: "block" }} dense>
          <DeleteButton {...props} handleClose={handleClose} />
        </MenuItem>
      </Menu>
    </Box>
  );
}

interface MenuButtonProps extends ActionProps {
  handleClose(): void;
}

function DeleteButton(props: MenuButtonProps) {
  const [confirmOpen, setConfirmOpen] = React.useState(false);
  const [mutate, { loading, error }] = useMutation(DELETE_PARTICIPANT, {
    errorPolicy: "all",
  });
  const deleteParticipant = () => {
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
      },
      /* Don't remove from the local cache, use the async participant list update for that. Otherwise another admin will not see the change
      // TODO: update function for the local cache
      update: (cache, { data }) => {
        console.log("updating with data", data);
        if (!data.event.participant.delete) {
          // Deletion failed
          return;
        }
        // Also remove the cached object itself so it can't be referenced by turns
        console.log("evicting", cache.identify(data.event.participant));
        cache.evict({
          id: cache.identify(data.event.participant),
        });
        const gcResult = cache.gc();
        console.log("gc result:", gcResult);
      },
      */
    }).then((result) => {
      if (!result.errors) {
        props.handleClose();
      }
    });
  };
  const handleButtonClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    // No confirmation if it was added by the orga and it has no previous turn.
    if (
      props.participant.addedByOrga &&
      getPreviousTurns(props.participant).length === 0
    ) {
      deleteParticipant();
    } else {
      setConfirmOpen(true);
    }
  };

  const handleConfirmClose = () => {
    setConfirmOpen(false);
  };
  const handleConfirmValidate = () => {
    setConfirmOpen(false);
    deleteParticipant();
  };

  return (
    <React.Fragment>
      <Button
        color="error"
        variant="contained"
        startIcon={<DeleteIcon />}
        size="small"
        onClick={handleButtonClick}
        disabled={loading}
      >
        <FormattedMessage defaultMessage="Delete" />
      </Button>
      {!!error &&
        error.graphQLErrors.map(({ message }, i) => (
          <Box key={i} color="error.main">
            <small>{message}</small>
          </Box>
        ))}
      <Dialog open={confirmOpen} onClose={handleConfirmClose}>
        <DialogTitle>
          <FormattedMessage defaultMessage="Are you sure?" />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <FormattedMessage defaultMessage="The participant will be deleted. This cannot be undone." />
          </DialogContentText>
          {getPreviousTurns(props.participant).length > 0 && (
            <DialogContentText>
              <FormattedMessage defaultMessage="They have already spoken, data about their speeches will be lost." />
            </DialogContentText>
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleConfirmClose} color="inherit">
            <FormattedMessage defaultMessage="Cancel" />
          </Button>
          <Button
            onClick={handleConfirmValidate}
            variant="contained"
            color="error"
            autoFocus
          >
            <FormattedMessage defaultMessage="Delete" />
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}
