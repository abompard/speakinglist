import { ApolloError, useMutation } from "@apollo/client";
import PlayIcon from "@mui/icons-material/PlayArrow";
import ListAddIcon from "@mui/icons-material/PlaylistAdd";
import StopIcon from "@mui/icons-material/Stop";
import Typography from "@mui/material/Typography";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { ParticipantAdmin, TurnAdmin } from "../../../app/types";
import { hasPendingTurn } from "../../../app/utils";
import { WithEventProps } from "../../../features/event/Event";
import ResponsiveButton from "../../../features/responsiveness/ResponsiveButton";
import {
  REQUEST_TURN,
  mutationTurnUpdate,
} from "../../../member/features/turn/queries";
import { END_TURN } from "../currentTurn/queries";
import { useTurnsList } from "../turns/Turns";
import { START_TURN } from "../waitinglist/queries";
import { getCurrentTurn, useWaitingList } from "../waitinglist/utils";

interface TurnActionProps extends WithEventProps {
  participant: ParticipantAdmin;
}

export function TurnAction(props: TurnActionProps) {
  const intl = useIntl();
  const {data: turnsList} = useTurnsList(props.event);
  const waitingList = useWaitingList(turnsList);
  const hasPending = hasPendingTurn(props.participant);
  const eventIsStopped = !!props.event.endedAt || !props.event.startedAt;
  const disabledMessage = eventIsStopped
    ? intl.formatMessage({ defaultMessage: "Event is stopped" })
    : props.event.isPaused
    ? intl.formatMessage({ defaultMessage: "Event is paused" })
    : hasPending
    ? intl.formatMessage({ defaultMessage: "There is already a pending turn" })
    : undefined;
  const disabled = eventIsStopped ||  props.event.isPaused ||  hasPending ||  !props.participant.isActive
  const waitingListIsEmpty = (waitingList || []).length === 0
  const currentTurn = getCurrentTurn(waitingList || [])
  const userIsSpeaking = currentTurn && currentTurn.participant.id === props.participant.id;
  const Class = userIsSpeaking ? StopTurn : waitingListIsEmpty ? GiveTurn : AddTurn;
  return (
      <Class {...props} disabled={disabled} disabledMessage={disabledMessage} currentTurn={userIsSpeaking ? currentTurn : undefined} />
  );
}

interface ActionButtonProps extends TurnActionProps {
  disabled: boolean;
  disabledMessage?: string;
  currentTurn?: TurnAdmin;
}

export function AddTurn(props: ActionButtonProps) {
  const [mutate, { loading, error }] = useMutation(REQUEST_TURN, {
    errorPolicy: "all",
    // update: mutationTurnUpdate,
  });
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
      },
    });
  return (
    <React.Fragment>
      <ResponsiveButton
        variant="contained"
        color="inherit"
        onClick={onClick}
        size="small"
        icon={<ListAddIcon />}
        disabled={props.disabled || loading}
        tooltipMessage={props.disabledMessage}
      >
        <FormattedMessage defaultMessage="Add" />
      </ResponsiveButton>
      <ErrorBlock error={error} />
    </React.Fragment>
  );
}




export function GiveTurn(props: ActionButtonProps) {
  const [requestTurn, { loading: requestLoading, error: requestError }] = useMutation(REQUEST_TURN, {
    errorPolicy: "all",
    update: mutationTurnUpdate,
  });
  const [startTurn, { loading: startLoading, error: startError }] = useMutation(START_TURN, {
    errorPolicy: "all",
  });
  const onClick = async () => {
    const turn = await requestTurn({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
      },
    });
    console.log(turn);
    if (!turn.data) {
      return;
    }
    await startTurn({variables: {
      eventCode: props.event.code,
        participantId: props.participant.id,
      turnId: turn.data?.event.participant.requestTurn.turn.id,
    }})
  }

  return (
    <React.Fragment>
      <ResponsiveButton
        variant="contained"
        color="inherit"
        onClick={onClick}
        size="small"
        icon={<PlayIcon />}
        disabled={props.disabled || requestLoading || startLoading}
        tooltipMessage={props.disabledMessage}
      >
        <FormattedMessage defaultMessage="Give" />
      </ResponsiveButton>
      <ErrorBlock error={requestError || startError} />
    </React.Fragment>
  );
}





export function StopTurn(props: ActionButtonProps) {
  const [mutate, { loading, error }] = useMutation(END_TURN, {
    errorPolicy: "all",
  });
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
        turnId: props.currentTurn?.id
      },
    });
  return (
    <React.Fragment>
      <ResponsiveButton
        variant="contained"
        color="inherit"
        onClick={onClick}
        size="small"
        icon={<StopIcon />}
        disabled={props.disabled || loading}
        tooltipMessage={props.disabledMessage}
      >
        <FormattedMessage defaultMessage="End Turn" />
      </ResponsiveButton>
      <ErrorBlock error={error} />
    </React.Fragment>
  );
}


function ErrorBlock(props: {error?: ApolloError}) {
  if (!props.error) {
    return null;
  }
  return (
  <>
  {props.error.graphQLErrors.map(({ message }, i) => (
      <Typography key={i} color="error">
        {message}
      </Typography>
    ))
  }
  </>
  );
}
