import { gql } from "@apollo/client";

export const GET_PARTICIPANTS = gql`
  query GetParticipants($eventCode: ID!) {
    event(code: $eventCode) {
      code
      participants {
        id
        name
        isOrga
        isActive
        addedByOrga
        lastSeen
        turns {
          id
          startedAt
          endedAt
          requestedAt
        }
        socialCategories {
          id
          name
          value
        }
      }
    }
  }
`;

export const SUBSCRIBE_PARTICIPANTS = gql`
  subscription OnParticipantsChanged($eventCode: ID!) {
    participants(eventCode: $eventCode) {
      id
      name
      isOrga
      isActive
      addedByOrga
      lastSeen
      turns {
        id
        startedAt
        endedAt
        requestedAt
      }
      socialCategories {
        id
        name
        value
      }
    }
  }
`;

export const PARTICIPANT_SET_ORGA = gql`
  mutation ParticipantSetOrga(
    $eventCode: ID!
    $participantId: ID!
    $isOrga: Boolean!
  ) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        setOrga(isOrga: $isOrga) {
          id
          isOrga
        }
      }
    }
  }
`;

export const DELETE_PARTICIPANT = gql`
  mutation DeleteParticipant($eventCode: ID!, $participantId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        delete
      }
    }
  }
`;
