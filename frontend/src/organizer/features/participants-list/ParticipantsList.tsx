import { useQuery } from "@apollo/client";
import PrevTurnIcon from '@mui/icons-material/BackHand';
import BackspaceIcon from "@mui/icons-material/Backspace";
import CategoryIcon from '@mui/icons-material/LocalOffer';
import ListAddIcon from "@mui/icons-material/PlaylistAdd";
import TotalTimeIcon from '@mui/icons-material/Timer';
import Box from "@mui/material/Box";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import IconButton from "@mui/material/IconButton";
import Input from "@mui/material/Input";
import InputAdornment from "@mui/material/InputAdornment";
import InputLabel from "@mui/material/InputLabel";
import Switch from "@mui/material/Switch";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import React, { useState } from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { ParticipantAdmin } from "../../../app/types";
import {
  durationToText,
  getPreviousTurns,
  getSpokenTime,
} from "../../../app/utils";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { useLastSeen } from "../../../features/last-seen/LastSeen";
import { Loading } from "../../../features/loading/Loading";
import { GET_ME } from "../../../features/me/queries";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import { SocialCategoriesList } from "../../../features/social-categories/SocialCategories";
import { AddParticipant } from "../participant-form/ParticipantForm";
import { ScrollToTop } from "../scroll-to-top/ScrollToTop";
import { Edit } from "./Edit";
import { TurnAction } from "./TurnAction";
import { useParticipantsList } from "./hook";

interface ParticipantsListProps extends WithEventProps {
  participantsListProps: ReturnType<typeof useParticipantsList>;
}
export function ParticipantsList(props: ParticipantsListProps) {
  const { loading, error, data } = props.participantsListProps;
  if (loading)
    return (
      <Loading>
        <FormattedMessage defaultMessage="Loading participants list..." />
      </Loading>
    );
  if (error) return <ErrorAlert error={error} />;
  if (data === null) return null;
  return <ParticipantsListTable event={props.event} participants={data} />;
}

interface ParticipantsListTableProps extends WithEventProps {
  participants: ParticipantAdmin[];
}
function ParticipantsListTable(props: ParticipantsListTableProps) {
  const [filter, setFilter] = useState("");
  const [onlyActive, setOnlyActive] = useState(true);
  const { data } = useQuery(GET_ME, {
    variables: { eventCode: props.event.code },
  });
  const me: ParticipantAdmin[] = data ? data.event.me.participants : [];
  const handleOnlyActiveChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setOnlyActive(event.target.checked);
  };
  const participants = props.participants
    .filter((p) => !onlyActive || p.isActive)
    .filter((p) => {
      if (!filter) return true;
      return p.name.toLowerCase().includes(filter.toLowerCase());
    });
  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="flex-end"
        mb={2}
      >
        <ParticipantsListFilter filter={filter} setFilter={setFilter} />
        <AddParticipant event={props.event} me={me} />
      </Box>
      {props.participants.length === 0 ? (
        <Box component="p" mt={5}>
          <FormattedMessage defaultMessage="There are no participants at the moment." />
        </Box>
      ) : (
        <>
          <FilteredParticipantsListTable
            {...props}
            participants={participants}
            me={me}
          />
          <Box mt={4} pl={2}>
            <FormControlLabel
              control={
                <Switch
                  checked={onlyActive}
                  onChange={handleOnlyActiveChange}
                  name="onlyactive"
                  color={onlyActive ? "primary" : "default"}
                  size="small"
                />
              }
              label={
                <FormattedMessage defaultMessage="Hide participants who left the event" />
              }
            />
          </Box>
          <ScrollToTop />
        </>
      )}
    </>
  );
}

interface ParticipantsListFilterProps {
  filter: string;
  setFilter: (value: string) => void;
}
function ParticipantsListFilter({
  setFilter,
  filter,
}: ParticipantsListFilterProps) {
  const intl = useIntl();
  return (
    <FormControl variant="standard" size="small">
      <InputLabel htmlFor="filter">
        <FormattedMessage defaultMessage="Filter" />
      </InputLabel>
      <Input
        placeholder={intl.formatMessage({
          defaultMessage: "Name",
        })}
        autoFocus={true}
        value={filter}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
          setFilter(e.target.value)
        }
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="clear filter"
              onClick={() => setFilter("")}
              edge="end"
              size="small"
            >
              <BackspaceIcon />
            </IconButton>
          </InputAdornment>
        }
      />
    </FormControl>
  );
}

type SortableColumn = "name" | "turns" | "time";

function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

interface FilteredParticipantsListTableProps
extends ParticipantsListTableProps {
  me: ParticipantAdmin[];
}

function FilteredParticipantsListTable(
  props: FilteredParticipantsListTableProps
  ) {
  const [orderBy, setOrderBy] = React.useState<SortableColumn>("name");
  const [order, setOrder] = React.useState<"asc" | "desc">("asc");
  const isLarge = useIsLarge();
  if (props.participants.length === 0) {
    return (
      <p>
        <FormattedMessage defaultMessage="No participant matching this filter." />
      </p>
    );
  }
  const createSortHandler =
    (column: SortableColumn) => (e: React.MouseEvent<unknown>) => {
      if (orderBy === column) {
        // toggle order
        setOrder(order === "asc" ? "desc" : "asc");
      } else {
        setOrderBy(column);
        setOrder("asc");
      }
    };
  // Sort the participants
  const sortFunction =
    orderBy === "name"
      ? (p1: ParticipantAdmin, p2: ParticipantAdmin) =>
          p1.name.toLowerCase().localeCompare(p2.name.toLowerCase())
      : orderBy === "time"
      ? (p1: ParticipantAdmin, p2: ParticipantAdmin) =>
          getSpokenTime(p1) - getSpokenTime(p2)
      : orderBy === "turns"
      ? (p1: ParticipantAdmin, p2: ParticipantAdmin) =>
          getPreviousTurns(p1).length - getPreviousTurns(p2).length
      : (p1: ParticipantAdmin, p2: ParticipantAdmin) => 0;
  const participants = stableSort(props.participants, sortFunction);
  if (order === "desc") {
    participants.reverse();
  }
  // Place the current participants first
  [...props.me].reverse().forEach((me) => {
    const meIndex = participants.map((p) => p.id).indexOf(me.id);
    if (meIndex === -1) {
      return;
    }
    participants.splice(0, 0, participants.splice(meIndex, 1)[0]);
  });
  const colAlign = isLarge ? "right" : "center"
  return (
    <TableContainer>
      <Table aria-label="participants list">
        <TableHead>
          <TableRow>
            <TableCell>
              <TableSortLabel
                active={orderBy === "name"}
                direction={orderBy === "name" ? order : "asc"}
                onClick={createSortHandler("name")}
                hideSortIcon={!isLarge}
              >
                <FormattedMessage defaultMessage="Name" />
              </TableSortLabel>
            </TableCell>
            <TableCell align={colAlign}>
              {isLarge ? <FormattedMessage defaultMessage="Categories" /> : <CategoryIcon />}
            </TableCell>
            <TableCell align={colAlign}>
              <TableSortLabel
                active={orderBy === "turns"}
                direction={orderBy === "turns" ? order : "asc"}
                onClick={createSortHandler("turns")}
                hideSortIcon={!isLarge}
              >
                {isLarge ? <FormattedMessage defaultMessage="Previous turns" /> : <PrevTurnIcon />}
              </TableSortLabel>
            </TableCell>
            <TableCell align={colAlign}>
              <TableSortLabel
                active={orderBy === "time"}
                direction={orderBy === "time" ? order : "asc"}
                onClick={createSortHandler("time")}
                hideSortIcon={!isLarge}
              >
                {isLarge ? <FormattedMessage defaultMessage="Total spoken time" /> : <TotalTimeIcon />}
              </TableSortLabel>
            </TableCell>
            <TableCell align={colAlign}>
              {isLarge ? <FormattedMessage defaultMessage="Add turn" /> : <ListAddIcon />}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {participants.map((participant) => (
            <ParticipantRow
              event={props.event}
              participant={participant}
              me={props.me}
              key={participant.id}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

interface ParticipantRowProps extends WithEventProps {
  participant: ParticipantAdmin;
  me: ParticipantAdmin[];
}
function ParticipantRow(props: ParticipantRowProps) {
  const { idleClass, idleInfo } = useLastSeen(props.participant);
  const isLarge = useIsLarge();
  const isMe = props.me.map((p) => p.id).includes(props.participant.id);
  const colAlign = isLarge ? "right" : "center"
  // TODO: check error handling
  return (
    <TableRow className={idleClass}>
      <TableCell sx={isMe ? { fontWeight: "bold" } : {}}>
        {props.participant.name}
        {idleInfo}
      </TableCell>
      <TableCell align={colAlign}>
        <SocialCategoriesList participant={props.participant} />
      </TableCell>
      <TableCell align={colAlign}>
        {getPreviousTurns(props.participant).length}
      </TableCell>
      <TableCell align={colAlign}>
        {durationToText(getSpokenTime(props.participant))}
      </TableCell>
      <TableCell align={colAlign}>
        <Edit
          event={props.event}
          me={props.me}
          participant={props.participant}
        />
        <TurnAction event={props.event} participant={props.participant} />
      </TableCell>
    </TableRow>
  );
}
