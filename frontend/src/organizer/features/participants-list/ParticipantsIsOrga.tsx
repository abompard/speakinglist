import { useMutation } from "@apollo/client";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import React from "react";
import { ParticipantAdmin } from "../../../app/types";
import { WithEventProps } from "../../../features/event/Event";
import { PARTICIPANT_SET_ORGA } from "./queries";


interface ParticipantIsOrgaProps extends WithEventProps {
  participant: ParticipantAdmin;
}

export function ParticipantIsOrga(props: ParticipantIsOrgaProps) {
  const [mutate, { loading }] = useMutation(PARTICIPANT_SET_ORGA, {});
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.checked;
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
        isOrga: value,
      },
    });
  };
  return (
    <FormControlLabel
      control={
        <Switch
          checked={props.participant.isOrga}
          onChange={handleChange}
          name="isorga"
          disabled={loading || props.participant.addedByOrga}
          color={props.participant.isOrga ? "primary" : "default"}
          size="small"
        />
      }
      label={undefined}
      style={{ marginRight: 0 }}
    />
  );
}
