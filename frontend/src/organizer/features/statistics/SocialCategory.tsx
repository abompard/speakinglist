import Box from "@mui/material/Box";
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import React from "react";
import { defineMessage, FormattedMessage, useIntl } from "react-intl";
import {
  PRIVATE,
  SocialCategoryName,
  SOCIAL_CATEGORIES,
} from "../../../app/socialcategories";
import { TurnAdmin } from "../../../app/types";
import { GraphsList, GraphsListProps } from "./Graphs";
import { StatisticsProps } from "./types";
import { participantIsOfCategoryGroup } from "./utils";

interface SocialCategoryStatsProps extends StatisticsProps {
  socialCategory: SocialCategoryName;
  turns: TurnAdmin[];
}
export function SocialCategoryStats(props: SocialCategoryStatsProps) {
  const intl = useIntl();
  const scOptions = [
    ...SOCIAL_CATEGORIES[props.socialCategory].options,
    { label: defineMessage({ defaultMessage: "?" }), value: PRIVATE },
  ];
  const scLabels = scOptions.map((option) => intl.formatMessage(option.label));
  const scTurns = scOptions.map((option) =>
    props.turns.filter((t) =>
      participantIsOfCategoryGroup(
        t.participant,
        props.socialCategory,
        option.value
      )
    )
  );
  const scParticipants = scOptions.map((option) =>
    props.participants.filter((p) =>
      participantIsOfCategoryGroup(p, props.socialCategory, option.value)
    )
  );
  return (
    <OrgaFilter
      event={props.event}
      labels={scLabels}
      turns={scTurns}
      participants={scParticipants}
    />
  );
}

function OrgaFilter(props: GraphsListProps) {
  const [withOrgas, setWithOrga] = React.useState(true);
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setWithOrga(event.target.checked);
  };
  const turns = props.turns.map((scTurns) =>
    scTurns.filter((t) => withOrgas || !t.participant.isOrga)
  );
  const participants = props.participants.map((scParticipants) =>
    scParticipants.filter((p) => withOrgas || !p.isOrga)
  );
  return (
    <React.Fragment>
      <Box mb={4} pl={2}>
        <FormControlLabel
          control={
            <Switch
              checked={withOrgas}
              onChange={handleChange}
              name="withorgas"
              color={withOrgas ? "primary" : "default"}
              size="small"
            />
          }
          label={<FormattedMessage defaultMessage="Include organizers" />}
        />
      </Box>
      <GraphsList {...props} turns={turns} participants={participants} />
    </React.Fragment>
  );
}
