import { Event, ParticipantAdmin, TurnAdmin } from "../../../app/types";

export interface StatisticsProps {
  event: Event;
  participants: ParticipantAdmin[];
  turnsList: TurnAdmin[];
  loading: boolean;
}

export interface EventStatsProps extends StatisticsProps {
  turns: TurnAdmin[];
}
