import { useLazyQuery } from "@apollo/client";
import DownloadIcon from "@mui/icons-material/GetApp";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import React, { useEffect, useRef } from "react";
import { FormattedMessage } from "react-intl";
import { settings } from "../../../app/config";
import { durationToText } from "../../../app/utils";
import { WithEventProps } from "../../../features/event/Event";
import { Loading } from "../../../features/loading/Loading";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import { CategoriesStats } from "./Categories";
import {
  PendingTurnsGraph,
  TurnDurationGraph,
  WaitingTimeGraph,
} from "./Graphs";
import { GET_DOWNLOAD_TOKEN } from "./queries";
import { EventStatsProps, StatisticsProps } from "./types";
import { computeTotalDuration } from "./utils";

export function Statistics(props: StatisticsProps) {
  if (props.loading) {
    return <Loading />;
  }
  const pastTurns = props.turnsList
    ? props.turnsList.filter((t) => t.startedAt !== null && t.endedAt !== null)
    : [];
  // Sort by start date
  pastTurns.sort(
    (t1, t2) => Date.parse(t1.startedAt) - Date.parse(t2.startedAt)
  );
  return (
    <Box>
      <EventStats {...props} turns={pastTurns} />
      <CategoriesStats {...props} turns={pastTurns} />
      <WaitingListStats {...props} turns={pastTurns} />
    </Box>
  );
}

function DownloadLink({ event }: WithEventProps) {
  const filename = `${event.title.replace(/\//, "-")}.csv`;
  const baseUrl = `${settings.API_URL}/event/${event.code}/stats/${filename}`;
  const linkRef = useRef<HTMLAnchorElement | null>(null);
  const [getToken, { loading, error, data }] = useLazyQuery(
    GET_DOWNLOAD_TOKEN,
    { fetchPolicy: "no-cache" }
  );
  const token = data ? data.downloadToken : "";
  useEffect(() => {
    if (loading || error || !token || !linkRef.current) {
      return;
    }
    linkRef.current.click();
  }, [linkRef, loading, error, token]);
  const handleClick = () => {
    getToken();
  };
  const isLarge = useIsLarge();
  return (
    <Box>
      <Button
        variant="outlined"
        onClick={handleClick}
        disabled={loading || !!error}
        size="small"
        color="inherit"
        startIcon={isLarge ? <DownloadIcon /> : undefined}
      >
        {isLarge ? <FormattedMessage defaultMessage="Download" /> : <DownloadIcon />}
      </Button>
      <a
        ref={linkRef}
        href={`${baseUrl}?token=${token}`}
        download={filename}
        style={{ display: "none" }}
      >
        download
      </a>
    </Box>
  );
}

function EventStats(props: EventStatsProps) {
  const speakingParticipants = new Set(
    props.turns.map((t) => t.participant.id)
  );
  return (
    <React.Fragment>
      <Box component="h3" mt={4}>
        <FormattedMessage defaultMessage="General" />
      </Box>
      <Box display="flex" justifyContent="space-between">
        <ul>
          <li>
            <FormattedMessage defaultMessage="Event duration:" />{" "}
            <EventDuration event={props.event} />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="Participants: {number}"
              values={{ number: props.participants.length }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="Speaking Participants: {number}"
              values={{ number: speakingParticipants.size }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="Turns: {number}"
              values={{ number: props.turns.length }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="Total speech duration: {duration}"
              values={{
                duration: durationToText(computeTotalDuration(props.turns)),
              }}
            />
          </li>
        </ul>
        <DownloadLink event={props.event} />
      </Box>
    </React.Fragment>
  );
}

function EventDuration({ event }: WithEventProps) {
  if (!event.startedAt) {
    return (
      <em>
        <FormattedMessage defaultMessage="Not started" />
      </em>
    );
  }
  const endedAt = event.endedAt ? Date.parse(event.endedAt) : Date.now();
  const totalDuration = Math.round(
    (endedAt - Date.parse(event.startedAt)) / 1000
  );
  const recordedDuration = event.duration || totalDuration;
  return (
    <React.Fragment>
      {durationToText(recordedDuration)}
      {recordedDuration !== totalDuration && (
        <Box component="span" ml={1}>
          <FormattedMessage
            defaultMessage="({duration} including pauses)"
            values={{
              duration: durationToText(totalDuration),
            }}
          />
        </Box>
      )}
    </React.Fragment>
  );
}

function WaitingListStats(props: EventStatsProps) {
  return (
    <>
      <Box component="h3" mt={4}>
        <FormattedMessage defaultMessage="As the event progressed" />
      </Box>
      <Grid container spacing={0}>
        <PendingTurnsGraph turns={props.turns} />
        <WaitingTimeGraph turns={props.turns} />
        <TurnDurationGraph turns={props.turns} />
      </Grid>
    </>
  );
}
