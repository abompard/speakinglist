import TabContext from "@mui/lab/TabContext";
import TabPanel from "@mui/lab/TabPanel";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { SOCIAL_CATEGORIES } from "../../../app/socialcategories";
import { OrganizerStats } from "./Organizers";
import { SocialCategoryStats } from "./SocialCategory";
import { EventStatsProps } from "./types";

export function CategoriesStats(props: EventStatsProps) {
  const [tabValue, setTabValue] = React.useState("orga");
  const handleTabChange = (event: React.ChangeEvent<{}>, newValue: string) => {
    setTabValue(newValue);
  };
  const intl = useIntl();
  const socialCategories = props.event.socialCategories || [];
  return (
    <React.Fragment>
      <Box component="h3" mt={4}>
        <FormattedMessage defaultMessage="Categories" />
      </Box>
      <TabContext value={tabValue}>
        <Tabs
          value={tabValue}
          indicatorColor="primary"
          textColor="primary"
          onChange={handleTabChange}
          aria-label="categories tabs"
        >
          <Tab
            label={<FormattedMessage defaultMessage="Organizers" />}
            value="orga"
          />
          {socialCategories.map((sc, index) => (
            <Tab
              key={sc.name}
              label={intl.formatMessage(SOCIAL_CATEGORIES[sc.name].title)}
              value={index.toString()}
            />
          ))}
        </Tabs>
        <TabPanel value="orga" sx={{ px: 0 }}>
          <OrganizerStats {...props} />
        </TabPanel>
        {socialCategories.map((sc, index) => (
          <TabPanel value={index.toString()} sx={{ px: 0 }} key={sc.name}>
            <SocialCategoryStats {...props} socialCategory={sc.name} />
          </TabPanel>
        ))}
      </TabContext>
    </React.Fragment>
  );
}
