import { gql } from "@apollo/client";

export const GET_DOWNLOAD_TOKEN = gql`
  query GetDownloadToken {
    downloadToken
  }
`;
