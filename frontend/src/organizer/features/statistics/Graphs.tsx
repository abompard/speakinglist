import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import React from "react";
import { FormattedMessage } from "react-intl";
import { ParticipantAdmin, TurnAdmin } from "../../../app/types";
import {
  durationToText,
  getPreviousTurns,
  getTurnDuration,
} from "../../../app/utils";
import { WithEventProps } from "../../../features/event/Event";
import { GraphWrapper, HistoGraph, LineGraph, PieGraph } from "./Graph";
import {
  buildParticipantsData,
  buildTurnsData,
  computeTotalDuration,
  computeWaitingTime,
  countExceeded,
} from "./utils";

export interface GraphsListProps extends WithEventProps {
  labels: string[];
  turns: TurnAdmin[][];
  participants: ParticipantAdmin[][];
}

export function GraphsList(props: GraphsListProps) {
  return (
    <Grid container spacing={0}>
      <ParticipantsGraph {...props} />
      <SpeakersGraph {...props} />
      <SpeechesGraph {...props} />
      <TotalDurationGraph {...props} />
      <MeanDurationGraph {...props} />
      <ExceededGraph {...props} max={props.event.turnMaxDuration} />
      <RelativeSpeakersGraph {...props} />
      <RelativeSpeechesGraph {...props} />
      <RelativeDurationGraph {...props} />
    </Grid>
  );
}

interface TurnGraphProps {
  labels: string[];
  turns: TurnAdmin[][];
}
interface ParticipantsGraphProps {
  labels: string[];
  participants: ParticipantAdmin[][];
}

function TotalDurationGraph({ labels, turns }: TurnGraphProps) {
  const data = buildTurnsData(labels, turns, (t) => computeTotalDuration(t));
  return (
    <PieGraph
      data={data}
      formatValue={(d) =>
        typeof d === "number" ? durationToText(d) : d.toString()
      }
      title={<FormattedMessage defaultMessage="Total duration" />}
      description={
        <FormattedMessage defaultMessage="Cumulative spoken time over the duration of the event." />
      }
    />
  );
}

function MeanDurationGraph({ labels, turns }: TurnGraphProps) {
  const data = buildTurnsData(labels, turns, (t) =>
    t.length > 0 ? computeTotalDuration(t) / t.length : 0
  );
  return (
    <HistoGraph
      data={data}
      legend={<FormattedMessage defaultMessage="Duration" />}
      formatValue={(d) =>
        typeof d === "number" ? durationToText(d) : d.toString()
      }
      title={<FormattedMessage defaultMessage="Average duration" />}
      description={
        <FormattedMessage defaultMessage="Average duration of a speech." />
      }
    />
  );
}

function SpeechesGraph({ labels, turns }: TurnGraphProps) {
  const data = buildTurnsData(labels, turns, (t) => t.length);
  return (
    <PieGraph
      data={data}
      title={<FormattedMessage defaultMessage="Speeches" />}
      description={
        <FormattedMessage defaultMessage="Total number of speaking turns." />
      }
    />
  );
}

interface ExceededGraphProps extends TurnGraphProps {
  max: number;
}
function ExceededGraph({ labels, turns, max }: ExceededGraphProps) {
  if (max === 0) {
    return (
      <GraphWrapper
        title={<FormattedMessage defaultMessage="Exceeded" />}
        isEmpty={() => false}
        data={[]}
      >
        <Box component="p" mt={3} mb={8}>
          <em>
            <FormattedMessage defaultMessage="No turn duration limit." />
          </em>
        </Box>
      </GraphWrapper>
    );
  }
  const data = buildTurnsData(labels, turns, (t) => countExceeded(t, max));
  return (
    <HistoGraph
      data={data}
      title={<FormattedMessage defaultMessage="Exceeded" />}
      description={
        <FormattedMessage
          defaultMessage="How many turns exceeded the {duration} limit."
          values={{ duration: durationToText(max) }}
        />
      }
      legend={<FormattedMessage defaultMessage="Speeches" />}
    />
  );
}

function SpeakersGraph({ labels, participants }: ParticipantsGraphProps) {
  const data = buildParticipantsData(
    labels,
    participants,
    (ps) => ps.filter((p) => getPreviousTurns(p).length > 0).length
  );
  return (
    <PieGraph
      data={data}
      title={<FormattedMessage defaultMessage="Speakers" />}
      description={
        <FormattedMessage defaultMessage="Number of participants who spoke." />
      }
    />
  );
}

function ParticipantsGraph({ labels, participants }: ParticipantsGraphProps) {
  const data = buildParticipantsData(labels, participants, (ps) => ps.length);
  return (
    <PieGraph
      data={data}
      title={<FormattedMessage defaultMessage="Participants" />}
      description={
        <FormattedMessage defaultMessage="Total number of participants, speaking or not." />
      }
    />
  );
}

function RelativeSpeakersGraph({
  labels,
  participants,
}: ParticipantsGraphProps) {
  const participantsData = buildParticipantsData(
    labels,
    participants,
    (ps) => ps.length
  );
  const speakersData = buildParticipantsData(
    labels,
    participants,
    (ps) => ps.filter((p) => getPreviousTurns(p).length > 0).length
  );
  const data = labels.map((label, index) => {
    const value =
      participantsData[index].value === 0
        ? 0
        : Math.round(
            (speakersData[index].value / participantsData[index].value) * 100
          );
    return {
      id: label,
      value,
    };
  });
  return (
    <HistoGraph
      data={data}
      title={<FormattedMessage defaultMessage="Speakers by Participant" />}
      description={
        <FormattedMessage defaultMessage="Proportion of participants who spoke." />
      }
      legend="%"
      formatValue={(v) => `${v}%`}
      maxValue={100}
    />
  );
}

function RelativeSpeechesGraph({
  labels,
  participants,
  turns,
}: ParticipantsGraphProps & TurnGraphProps) {
  const speakersData = buildParticipantsData(
    labels,
    participants,
    (ps) => ps.filter((p) => getPreviousTurns(p).length > 0).length
  );
  const turnsData = buildTurnsData(labels, turns, (t) => t.length);
  const data = labels.map((label, index) => {
    const value =
      speakersData[index].value === 0
        ? 0
        : turnsData[index].value / speakersData[index].value;
    return {
      id: label,
      value,
    };
  });
  return (
    <HistoGraph
      data={data}
      title={<FormattedMessage defaultMessage="Speeches by Speaker" />}
      description={
        <FormattedMessage defaultMessage="Mean number of speaking turns taken by a speaker." />
      }
      legend={<FormattedMessage defaultMessage="Speeches" />}
      formatValue={(v) => (v as number).toFixed(2)}
    />
  );
}

function RelativeDurationGraph({
  labels,
  participants,
  turns,
}: ParticipantsGraphProps & TurnGraphProps) {
  const speakersData = buildParticipantsData(
    labels,
    participants,
    (ps) => ps.filter((p) => getPreviousTurns(p).length > 0).length
  );
  const durationData = buildTurnsData(labels, turns, (t) =>
    computeTotalDuration(t)
  );
  const data = labels.map((label, index) => {
    const value =
      speakersData[index].value === 0
        ? 0
        : durationData[index].value / speakersData[index].value;
    return {
      id: label,
      value,
    };
  });
  return (
    <HistoGraph
      data={data}
      title={<FormattedMessage defaultMessage="Total duration by Speaker" />}
      description={
        <FormattedMessage defaultMessage="Mean cumulative speech duration of a speaker." />
      }
      legend={<FormattedMessage defaultMessage="Duration" />}
      formatValue={(d) =>
        typeof d === "number" ? durationToText(d) : d.toString()
      }
    />
  );
}

interface GlobalTurnGraphProps {
  labels?: string[];
  turns: TurnAdmin[];
}

export function PendingTurnsGraph({ turns }: GlobalTurnGraphProps) {
  const totalDuration =
    turns.length === 0
      ? -1
      : (Date.parse(turns[turns.length - 1].startedAt) -
          Date.parse(turns[0].startedAt)) /
        1000;
  const sampleSize = 30; // a point every X seconds
  let data = [];
  for (let time = 0; time <= totalDuration; time += sampleSize) {
    const sampleStartTime =
      turns.length === 0 ? 0 : Date.parse(turns[0].startedAt) + time * 1000;
    data.push({
      x: time,
      y: turns.filter(
        (turn) =>
          Date.parse(turn.requestedAt) < sampleStartTime &&
          Date.parse(turn.startedAt) > sampleStartTime
      ).length,
    });
  }
  return (
    <LineGraph
      data={[{ id: "turns", data }]}
      legend={<FormattedMessage defaultMessage="Pending turns" />}
      title={<FormattedMessage defaultMessage="Pending turns" />}
      xFormat={(x) =>
        typeof x === "number" ? durationToText(x) : x.toString()
      }
      enablePoints={false}
      xLegend={<FormattedMessage defaultMessage="Event duration" />}
      description={
        <FormattedMessage defaultMessage="How many turns were in the waiting list during the event." />
      }
    />
  );
}

export function WaitingTimeGraph({ turns }: GlobalTurnGraphProps) {
  const firstTurnStartedAt =
    turns.length === 0 ? 0 : Date.parse(turns[0].startedAt);
  const data = turns.map((turn) => {
    const y = computeWaitingTime(turn);
    return {
      x: (Date.parse(turn.startedAt) - firstTurnStartedAt) / 1000,
      y,
    };
  });
  return (
    <LineGraph
      data={[{ id: "turns", data }]}
      legend={<FormattedMessage defaultMessage="Waiting time" />}
      formatValue={(d) =>
        typeof d === "number" ? durationToText(d) : d.toString()
      }
      xFormat={(v) =>
        typeof v === "number" ? durationToText(v) : v.toString()
      }
      tooltipFormat={(p) => (
        <FormattedMessage
          defaultMessage="Turn {index}: {duration}"
          values={{ index: p.index + 1, duration: p.data.yFormatted }}
        />
      )}
      xLegend={<FormattedMessage defaultMessage="Event duration" />}
      title={<FormattedMessage defaultMessage="Waiting time" />}
      description={
        <FormattedMessage defaultMessage="How long people waited for their turn to start." />
      }
    />
  );
}

export function TurnDurationGraph({ turns }: GlobalTurnGraphProps) {
  const firstTurnStartedAt =
    turns.length === 0 ? 0 : Date.parse(turns[0].startedAt);
  const data = turns.map((turn) => {
    const y = getTurnDuration(turn);
    return {
      x: (Date.parse(turn.startedAt) - firstTurnStartedAt) / 1000,
      y,
    };
  });
  return (
    <LineGraph
      data={[{ id: "turns", data }]}
      legend={<FormattedMessage defaultMessage="Duration" />}
      formatValue={(d) =>
        typeof d === "number" ? durationToText(d) : d.toString()
      }
      xFormat={(v) =>
        typeof v === "number" ? durationToText(v) : v.toString()
      }
      tooltipFormat={(p) => {
        const index = parseInt(p.id.replace(`${p.serieId}.`, ""));
        return (
          <FormattedMessage
            defaultMessage="Turn {index}: {duration}"
            values={{ index: index + 1, duration: p.data.yFormatted }}
          />
        );
      }}
      xLegend={<FormattedMessage defaultMessage="Event duration" />}
      title={<FormattedMessage defaultMessage="Speeches duration" />}
      description={
        <FormattedMessage defaultMessage="How long people talked over the duration of the event." />
      }
    />
  );
}
