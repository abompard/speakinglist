import Box from "@mui/material/Box";
import Grid from "@mui/material/Grid";
import { ResponsiveBar } from "@nivo/bar";
import { getOrdinalColorScale } from "@nivo/colors";
import { DatumValue } from "@nivo/core";
import { Datum, Point, ResponsiveLine, Serie } from "@nivo/line";
import { ResponsivePie } from "@nivo/pie";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";

type RowData = {
  id: string;
  value: number;
  label?: string;
};
interface BaseGraphProps {
  title: React.ReactNode;
  description?: React.ReactNode;
  formatValue?: (value: DatumValue) => string;
  // Formatting of values on the X axis
  formatX?: (x: number | string | Date) => string;
}
interface GraphProps extends BaseGraphProps {
  data: RowData[];
}

const theme = {
  fontSize: 13,
  labels: {
    text: {
      fontWeight: "bold",
    } as React.CSSProperties,
  },
};

interface GraphWrapperProps extends BaseGraphProps {
  wide?: boolean;
  isEmpty: (d: any[]) => boolean;
  data: any;
  children: React.ReactNode;
}

export function GraphWrapper(props: GraphWrapperProps) {
  return (
    <Grid item xs={12} sm={props.wide ? 12 : 6} lg={props.wide ? 12 : 4}>
      <Box textAlign="center" mb={4}>
        <Box component="h4" my={0}>
          {props.title}
        </Box>
        {props.description && (
          <Box component="p" mb={0}>
            <small>{props.description}</small>
          </Box>
        )}
        {props.isEmpty(props.data) ? (
          <Box component="p" mt={3} mb={8}>
            <em>
              <FormattedMessage defaultMessage="No data" />
            </em>
          </Box>
        ) : (
          <Box height={props.wide ? 350 : 300}>{props.children}</Box>
        )}
      </Box>
    </Grid>
  );
}

interface PieGraphProps extends GraphProps {}
export function PieGraph(props: PieGraphProps) {
  return (
    <GraphWrapper {...props} isEmpty={(data) => data.length === 0}>
      <ResponsivePie
        data={props.data}
        colors={{ scheme: "accent" }}
        margin={{ top: 20, bottom: 20, right: 110, left: 110 }}
        valueFormat={(d) =>
          d === 0
            ? ""
            : props.formatValue
            ? props.formatValue(d as number).toString()
            : d.toString()
        }
        theme={theme}
        arcLinkLabelsDiagonalLength={8}
        arcLinkLabelsStraightLength={12}
        arcLinkLabelsColor={{ from: "color" }}
        arcLinkLabelsThickness={2}
        borderWidth={1}
        borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
        arcLabelsTextColor="#333333"
      />
    </GraphWrapper>
  );
}

interface HistoGraphProps extends GraphProps {
  legend: React.ReactNode;
  maxValue?: number;
}
export function HistoGraph(props: HistoGraphProps) {
  return (
    <GraphWrapper
      {...props}
      isEmpty={(data) => data.filter((d) => d.value !== 0).length === 0}
    >
      <ResponsiveBar
        data={props.data}
        colors={getOrdinalColorScale({ scheme: "accent" }, "index")}
        label={(d) =>
          d.value === 0 || d.value === null
            ? ""
            : props.formatValue
            ? props.formatValue(d.value as number).toString()
            : d.value.toString()
        }
        tooltip={(d) => (
          <span>
            {d.indexValue}: {d.data.label || d.value.toString()}
          </span>
        )}
        isInteractive={false}
        margin={{ top: 30, bottom: 40, right: 30, left: 30 }}
        theme={theme}
        borderWidth={1}
        borderColor={{ from: "color", modifiers: [["darker", 0.2]] }}
        axisLeft={{
          legend: props.legend,
          legendPosition: "middle",
          legendOffset: -10,
          tickValues: [],
        }}
        maxValue={props.maxValue}
      />
    </GraphWrapper>
  );
}

interface LineGraphProps extends BaseGraphProps {
  legend: React.ReactNode;
  xLegend?: React.ReactNode;
  data: Serie[];
  enablePoints?: boolean;
  tooltipFormat?: (point: Point) => React.ReactNode;
  xFormat?: (value: DatumValue) => string;
}
export function LineGraph(props: LineGraphProps) {
  const intl = useIntl();
  const isEmptyFn = (data: Serie[]) => data[0].data.length === 0;
  const average = isEmptyFn(props.data)
    ? 0
    : props.data[0].data
        .map((d: Datum) => (d.y as number) || 0)
        .reduce((a: number, b: number) => a + b) / props.data[0].data.length;
  const maxValueLength = Math.max(
    ...props.data[0].data.map((d) =>
      props.formatValue
        ? props.formatValue(d.y as number).toString().length
        : (d.y as number).toString().length
    )
  );
  return (
    <GraphWrapper {...props} isEmpty={isEmptyFn} wide>
      <ResponsiveLine
        data={props.data}
        colors={{ scheme: "category10" }}
        useMesh
        enablePoints={props.enablePoints}
        curve="monotoneX"
        margin={{
          top: 30,
          bottom: props.xLegend ? 90 : 70,
          right: 100,
          left: 100,
        }}
        markers={[
          {
            axis: "y",
            value: average,
            lineStyle: { stroke: "#b0413e", strokeWidth: 1 },
            legend: intl.formatMessage(
              { defaultMessage: "average: {value}" },
              {
                value: props.formatValue
                  ? props.formatValue(average)
                  : average.toFixed(2),
              }
            ),
            textStyle: { fontSize: "85%" },
          },
        ]}
        theme={theme}
        xScale={{ type: "linear" }}
        xFormat={props.xFormat}
        yFormat={props.formatValue}
        tooltip={(d) => (
          <Box
            sx={{
              backgroundColor: "white",
              padding: "3px",
              border: "1px solid #eeeeee",
            }}
          >
            {props.tooltipFormat
              ? props.tooltipFormat(d.point)
              : d.point.data.yFormatted}
          </Box>
        )}
        axisLeft={{
          legend: props.legend,
          legendPosition: "middle",
          legendOffset: -Math.max(maxValueLength * 11, 40) - 5,
          format: (value) =>
            props.formatValue
              ? props.formatValue(value as number)
              : (value as number).toString(),
        }}
        axisBottom={{
          legend: props.xLegend,
          legendPosition: "middle",
          legendOffset: props.xLegend ? 40 : 0,
          format: (value: number | string | Date) =>
            props.xFormat && typeof props.xFormat === "function"
              ? props.xFormat(value)
              : value.toString(),
        }}
      />
    </GraphWrapper>
  );
}
