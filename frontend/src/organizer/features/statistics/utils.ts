import { ParticipantAdmin, TurnAdmin } from "../../../app/types";
import { getTurnDuration } from "../../../app/utils";

export function computeTotalDuration(turns: TurnAdmin[]) {
  return turns.reduce(
    (total, current) => total + (getTurnDuration(current) || 0),
    0
  );
}

export function countExceeded(turns: TurnAdmin[], threshold: number) {
  return turns.filter((t) => (getTurnDuration(t) || 0) > threshold).length;
}

export const participantIsOfCategoryGroup = (
  participant: ParticipantAdmin,
  category: string,
  group: string
) =>
  participant.socialCategories &&
  participant.socialCategories.filter(
    (sc) => sc.name === category && sc.value === group
  ).length > 0;

type ComputeTurnDataType = (turns: TurnAdmin[]) => number;
type ComputeParticipantsDataType = (participants: ParticipantAdmin[]) => number;

export const buildTurnsData = (
  labels: string[],
  turns: TurnAdmin[][],
  compute: ComputeTurnDataType
) =>
  labels.map((label, index) => ({
    id: label,
    value: compute(turns[index]),
  }));

export const buildParticipantsData = (
  labels: string[],
  participants: ParticipantAdmin[][],
  compute: ComputeParticipantsDataType
) =>
  labels.map((label, index) => ({
    id: label,
    value: compute(participants[index]),
  }));

export function computeWaitingTime(turn: TurnAdmin) {
  return Math.trunc(
    (Date.parse(turn.startedAt) - Date.parse(turn.requestedAt)) / 1000
  );
}
