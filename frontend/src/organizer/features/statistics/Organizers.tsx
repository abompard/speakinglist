import React from "react";
import { useIntl } from "react-intl";
import { TurnAdmin } from "../../../app/types";
import { GraphsList } from "./Graphs";
import { StatisticsProps } from "./types";

interface OrganizerStatsProps extends StatisticsProps {
  turns: TurnAdmin[];
}
export function OrganizerStats(props: OrganizerStatsProps) {
  const intl = useIntl();
  const labels = [
    intl.formatMessage({ defaultMessage: "Org." }),
    intl.formatMessage({ defaultMessage: "Not org." }),
  ];
  const turns = [
    props.turns.filter((t) => t.participant.isOrga),
    props.turns.filter((t) => !t.participant.isOrga),
  ];
  const participants = [
    props.participants.filter((p) => p.isOrga),
    props.participants.filter((p) => !p.isOrga),
  ];

  return (
    <GraphsList
      event={props.event}
      labels={labels}
      participants={participants}
      turns={turns}
    />
  );
}
