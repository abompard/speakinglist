import { TurnAdmin } from "../../../app/types";

export function getCurrentTurn(waitingList: TurnAdmin[]) {
    const currentTurn = waitingList
    .filter((turn) => turn.startedAt && !turn.endedAt)
    .sort(
      // There can be multiple current turns if we just started a new one while the previous one was
      // still running, because the API does not give us an update on the previous one.
      (t1, t2) => {
        return Date.parse(t1.startedAt) - Date.parse(t2.startedAt);
      }
    );
    return currentTurn.length > 0 ? currentTurn[currentTurn.length - 1] : null;
}

export function useWaitingList(turns: TurnAdmin[] | null){
    return (turns || []).filter((t) => t.endedAt === null);
}
