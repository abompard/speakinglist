import { useMutation } from "@apollo/client";
import PrevTurnIcon from '@mui/icons-material/BackHand';
import DeleteIcon from "@mui/icons-material/Delete";
import CategoryIcon from '@mui/icons-material/LocalOffer';
import PlayIcon from "@mui/icons-material/PlayArrow";
import RefreshIcon from "@mui/icons-material/Refresh";
import DurationIcon from '@mui/icons-material/Timer';
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Tooltip from "@mui/material/Tooltip";
import React from "react";
import {
  DragDropContext,
  Draggable,
  DraggableProvided,
  DraggableStateSnapshot,
  Droppable,
  DropResult,
  ResponderProvided,
} from "react-beautiful-dnd";
import { FormattedMessage, useIntl } from "react-intl";
import { TurnAdmin } from "../../../app/types";
import {
  durationToText,
  getPreviousTurns,
  getSpokenTime,
} from "../../../app/utils";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { useLastSeen } from "../../../features/last-seen/LastSeen";
import { LoadingIndicator } from "../../../features/loading/Loading";
import ResponsiveButton from "../../../features/responsiveness/ResponsiveButton";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import { SocialCategoriesList } from "../../../features/social-categories/SocialCategories";
import { CANCEL_TURN } from "../../../member/features/turn/queries";
import { CurrentTurn } from "../currentTurn/CurrentTurn";
import { useParticipantsList } from "../participants-list/hook";
import { GET_TURNS } from "../turns/queries";
import { useTurnsList } from "../turns/Turns";
import { MOVE_TURN, START_TURN } from "./queries";
import { getCurrentTurn, useWaitingList } from "./utils";

interface WaitingListProps extends WithEventProps {
  turnsProps: ReturnType<typeof useTurnsList>;
  startPolling: ReturnType<typeof useParticipantsList>["startPolling"];
  stopPolling: ReturnType<typeof useParticipantsList>["stopPolling"];
}
export function WaitingList(props: WaitingListProps) {
  const { loading, error, data, refetch } = props.turnsProps;
  const waitingList = useWaitingList(data);
  if (loading) return <p>Loading waiting list...</p>;
  if (error) return <ErrorAlert error={error} />;
  return (
    <WaitingListWithData
      {...props}
      refetch={() => refetch({ eventCode: props.event.code })}
      waitingList={waitingList}
    />
  );
}

interface WaitingListWithDataProps extends WithEventProps {
  waitingList: TurnAdmin[];
  refetch: () => void;
  startPolling: ReturnType<typeof useParticipantsList>["startPolling"];
  stopPolling: ReturnType<typeof useParticipantsList>["stopPolling"];
}

function RefreshButton({ refetch }: Pick<WaitingListWithDataProps, "refetch">) {
  return (
    <IconButton onClick={refetch} size="small">
      <RefreshIcon />
    </IconButton>
  );
}

function WaitingListWithData(props: WaitingListWithDataProps) {
  const currentTurn = getCurrentTurn(props.waitingList);
  return (
    <div>
      {currentTurn && (
        <CurrentTurn
          event={props.event}
          turn={currentTurn}
        />
      )}
      <Box pt={2}>
        <WaitingListTable
          {...props}
          waitingList={props.waitingList
            .filter((turn) => !turn.startedAt && !turn.endedAt)
            .sort((t1, t2) => t1.position - t2.position)}
        />
      </Box>
    </div>
  );
}

function WaitingListTable(props: WaitingListWithDataProps) {
  const [moveTurn, { loading }] = useMutation(MOVE_TURN);
  const isLarge = useIsLarge();

  const onDragStart = () => {
    props.stopPolling();
  };
  const onDragEnd = (result: DropResult) => {
    props.startPolling();
    // dropped outside the list
    if (!result.destination) {
      return;
    }
    if (result.source.index === result.destination.index) {
      return;
    }

    console.log(
      `dragEnd ${result.source.index} to  ${result.destination.index}`
    );
    const optimisticResponse = reorder(
      props.waitingList,
      result.source.index,
      result.destination.index
    ).map((turn, index) => ({
      __typename: "Turn",
      id: turn.id,
      position: index,
    }));
    moveTurn({
      variables: {
        eventCode: props.event.code,
        fromPosition: result.source.index,
        toPosition: result.destination.index,
      },
      optimisticResponse: {
        __typename: "Mutation",
        event: { moveTurn: optimisticResponse },
      },
      update: (proxy, { data }) => {
        // Read the data from our cache for this query.
        const oldData: { event: object } | null = proxy.readQuery({
          query: GET_TURNS,
          variables: { eventCode: props.event.code },
        });
        if (!oldData) return;
        const newData = {
          ...oldData,
          event: {
            ...oldData.event!,
            waitingList: data.event.moveTurn,
          },
        };
        // Write our data back to the cache
        proxy.writeQuery({
          query: GET_TURNS,
          variables: { eventCode: props.event.code },
          data: newData,
        });
      },
    });
  };

  const colAlign = isLarge ? "right" : "center"

  if (props.waitingList.length === 0) {
    return (
      <p>
        <FormattedMessage defaultMessage="The waiting list is empty at the moment." />{" "}
        <RefreshButton refetch={props.refetch} />
      </p>
    );
  }

  return (
    <TableContainer>
      <Table aria-label="waiting list">
        <TableHead>
          <TableRow>
            <TableCell>
              {loading && <LoadingIndicator />}
              <RefreshButton refetch={props.refetch} />
            </TableCell>
            <TableCell>
              <FormattedMessage defaultMessage="Name" />
            </TableCell>
            <TableCell align={colAlign}>
              { isLarge ? <FormattedMessage defaultMessage="Categories" /> : <CategoryIcon /> }
            </TableCell>
            <TableCell align={colAlign}>
              { isLarge ? <FormattedMessage defaultMessage="Previous turns" /> : <PrevTurnIcon /> }
            </TableCell>
            <TableCell align={colAlign}>
              { isLarge ? <FormattedMessage defaultMessage="Total spoken time" /> : <DurationIcon /> }
            </TableCell>
            <TableCell align={colAlign}>
              { isLarge ? <FormattedMessage defaultMessage="Give speech" /> : <PlayIcon /> }
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody component={DroppableComponent(onDragStart, onDragEnd)}>
          {props.waitingList.map((turn, index) => (
            <WaitingTurn {...props} turn={turn} key={turn.id} index={index} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

interface WaitingTurnProps extends WithEventProps {
  turn: TurnAdmin;
  index: number;
}
function WaitingTurn({ event, turn, index }: WaitingTurnProps) {
  const intl = useIntl();
  const { idleClass, idleInfo } = useLastSeen(turn.participant);
  return (
    <TableRow
      component={DraggableComponent(turn.id, index)}
      className={idleClass}
    >
      <TableCell component="th" scope="row" sx={{ pl: 3.5 }}>
        <strong>{index + 1}</strong>
      </TableCell>
      <TableCell>
        {turn.participant.name}
        {idleInfo}
      </TableCell>
      <TableCell align="right">
        <SocialCategoriesList participant={turn.participant} />
      </TableCell>
      <TableCell align="right">
        {getPreviousTurns(turn.participant).length}
      </TableCell>
      <TableCell align="right">
        {durationToText(getSpokenTime(turn.participant))}
      </TableCell>
      <TableCell align="right">
        <TurnActionButton
          event={event}
          turn={turn}
          label={intl.formatMessage({ defaultMessage: "Remove" })}
          color="error"
          icon={<DeleteIcon />}
          iconOnly={true}
          mutation={CANCEL_TURN}
        />
        <TurnActionButton
          event={event}
          turn={turn}
          label={intl.formatMessage({ defaultMessage: "Give" })}
          color="primary"
          icon={<PlayIcon />}
          mutation={START_TURN}
        />
      </TableCell>
    </TableRow>
  );
}

interface TurnButtonProps extends WithEventProps {
  label: string;
  color?: "primary" | "secondary" | "error";
  icon: React.ReactNode;
  iconOnly?: boolean;
  turn: TurnAdmin;
  mutation: typeof START_TURN;
}
function TurnActionButton(props: TurnButtonProps) {
  const [mutate, { loading }] = useMutation(props.mutation);
  const isLarge = useIsLarge();
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.turn.participant.id,
        turnId: props.turn.id,
      },
    });
  const buttonProps = {
    onClick,
    loading,
    icon: props.icon,
    label: props.label,
    event: props.event,
    color: props.color,
  };
  return (
    <Box pl={isLarge ? 2 : 1} component="span">
      {props.iconOnly ? (
        <TurnActionIconButton {...buttonProps} />
      ) : (
        <TurnActionRegularButton {...buttonProps} />
      )}
    </Box>
  );
}
interface TurnActionConcreteButtonProps extends WithEventProps {
  label: string;
  color?: "primary" | "secondary" | "error";
  icon: React.ReactNode;
  loading: boolean;
  onClick: () => void;
}
function TurnActionRegularButton(props: TurnActionConcreteButtonProps) {
  return (
    <ResponsiveButton
      variant="contained"
      onClick={props.onClick}
      disabled={props.loading}
      color={props.color}
      size="small"
      icon={props.icon}
    >
      {props.label}
    </ResponsiveButton>
  );
}
function TurnActionIconButton(props: TurnActionConcreteButtonProps) {
  return (
    <Tooltip title={props.label} placement="top" arrow>
      <IconButton
        aria-label={props.label}
        onClick={props.onClick}
        disabled={props.loading}
        color={props.color}
        size="small"
      >
        {props.icon}
      </IconButton>
    </Tooltip>
  );
}

const reorder = (list: any[], startIndex: number, endIndex: number) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const DraggableComponent = (id: any, index: number) => (props: any) => {
  return (
    <Draggable draggableId={id} index={index}>
      {(provided: DraggableProvided, snapshot: DraggableStateSnapshot) => (
        <TableRow
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          style={provided.draggableProps.style}
          {...props}
          className={props.className || ""}
          sx={
            snapshot.isDragging
              ? {
                  background: "rgb(100,100,100)",
                  "& td,th": {
                    color: "rgb(250,250,250)",
                  },
                }
              : {}
          }
        >
          {props.children}
        </TableRow>
      )}
    </Draggable>
  );
};

const DroppableComponent =
  (
    onDragStart: () => void,
    onDragEnd: (result: DropResult, provided: ResponderProvided) => void
  ) =>
  (props: any) => {
    return (
      <DragDropContext onDragStart={onDragStart} onDragEnd={onDragEnd}>
        <Droppable droppableId={"1"} direction="vertical">
          {(provided) => {
            return (
              <TableBody
                ref={provided.innerRef}
                {...provided.droppableProps}
                {...props}
              >
                {props.children}
                {provided.placeholder}
              </TableBody>
            );
          }}
        </Droppable>
      </DragDropContext>
    );
  };
