import { gql } from "@apollo/client";

export const START_TURN = gql`
  mutation StartTurn($eventCode: ID!, $participantId: ID!, $turnId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        turn(id: $turnId) {
          id
          start {
            turn {
              id
              position
              startedAt
            }
          }
        }
      }
    }
  }
`;

export const MOVE_TURN = gql`
  mutation MoveTurn($eventCode: ID!, $fromPosition: Int!, $toPosition: Int!) {
    event(code: $eventCode) {
      code
      moveTurn(fromPosition: $fromPosition, toPosition: $toPosition) {
        id
        position
      }
    }
  }
`;
