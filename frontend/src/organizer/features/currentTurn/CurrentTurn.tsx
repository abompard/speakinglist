import { useMutation } from "@apollo/client/react";
import StopIcon from "@mui/icons-material/Stop";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { FormattedMessage } from "react-intl";
import { TurnAdmin } from "../../../app/types";
import {
  DurationCounter,
  DurationWidgetProps,
} from "../../../features/duration-counter/DurationCounter";
import { WithEventProps } from "../../../features/event/Event";
import ResponsiveButton from "../../../features/responsiveness/ResponsiveButton";
import { TimerWithProgressBar } from "../../../member/features/duration-counter/DurationCounter";
import { END_TURN } from "./queries";

interface CurrentTurnProps extends WithEventProps {
  turn: TurnAdmin;
}

export function CurrentTurn(props: CurrentTurnProps) {
  return (
    <Card>
      <Box
        component={CardContent}
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Box flexGrow={1} textAlign="right">
          <FormattedMessage defaultMessage="Now speaking:" />{" "}
          {props.turn.participant.name}.
        </Box>
        <Box px={2}>
          <DurationCounter
            turn={props.turn}
            maxTime={props.event.turnMaxDuration}
            widget={CurrentSpeakerDurationWidget}
          />
        </Box>
        <Box flexGrow={1}>
          <EndTurn {...props} />
        </Box>
      </Box>
    </Card>
  );
}

function CurrentSpeakerDurationWidget(props: DurationWidgetProps) {
  return <TimerWithProgressBar {...props} size="small" />;
}

function EndTurn({ event, turn }: CurrentTurnProps) {
  const [mutate, { loading }] = useMutation(END_TURN);
  const onClick = () =>
    mutate({
      variables: {
        eventCode: event.code,
        participantId: turn.participant.id,
        turnId: turn.id,
      },
    });
  return (
    <ResponsiveButton
      variant="contained"
      onClick={onClick}
      disabled={loading}
      size="small"
      color="primary"
      icon={<StopIcon />}
    >
      <FormattedMessage defaultMessage="End Turn" />
    </ResponsiveButton>
  );
}
