import { gql } from "@apollo/client";

export const END_TURN = gql`
  mutation EndTurn($eventCode: ID!, $participantId: ID!, $turnId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        turn(id: $turnId) {
          id
          end {
            turn {
              id
              endedAt
            }
          }
        }
      }
    }
  }
`;
