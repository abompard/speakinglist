import PrevTurnIcon from '@mui/icons-material/BackHand';
import CategoryIcon from '@mui/icons-material/LocalOffer';
import DurationIcon from '@mui/icons-material/Timer';
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography, { TypographyProps } from "@mui/material/Typography";
import { FormattedMessage } from "react-intl";
import { TurnAdmin } from "../../../app/types";
import {
  durationToText,
  getPreviousTurns,
  getTurnDuration,
} from "../../../app/utils";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { LoadingIndicator } from "../../../features/loading/Loading";
import useIsLarge from "../../../features/responsiveness/useIsLarge";
import { SocialCategoriesList } from "../../../features/social-categories/SocialCategories";
import { useTurnsList } from "../turns/Turns";

interface PastTurnsListProps extends WithEventProps {
  turnsProps: ReturnType<typeof useTurnsList>;
}
export function PastTurnsList(props: PastTurnsListProps) {
  const { loading, error, data } = props.turnsProps;
  if (loading) return <p>Loading past turns list...</p>;
  if (error) return <ErrorAlert error={error} />;
  if (data === null) return null;
  const pastTurns = data.filter(
    (t) => t.startedAt !== null && t.endedAt !== null
  );
  return (
    <PastTurnsListWithData {...props} pastTurns={pastTurns} loading={loading} />
  );
}

interface PastTurnsListWithDataProps extends WithEventProps {
  pastTurns: TurnAdmin[];
  loading: boolean;
}
function PastTurnsListWithData(props: PastTurnsListWithDataProps) {
  const isLarge = useIsLarge();
  if (props.pastTurns.length === 0) {
    return (
      <p>
        <FormattedMessage defaultMessage="No one has spoken yet." />
      </p>
    );
  }
  const pastTurns = props.pastTurns.slice();
  pastTurns.sort(
    (t1, t2) => Date.parse(t2.startedAt) - Date.parse(t1.startedAt)
  );
  const colAlign = isLarge ? "right" : "center"
  return (
    <Box pt={2}>
      <TableContainer>
        <Table aria-label="past turns">
          <TableHead>
            <TableRow>
              <TableCell>{props.loading && <LoadingIndicator />}#</TableCell>
              <TableCell>
                <FormattedMessage defaultMessage="Name" />
              </TableCell>
              <TableCell align={colAlign}>
              { isLarge ? <FormattedMessage defaultMessage="Duration" /> : <DurationIcon /> }
              </TableCell>
              <TableCell align={colAlign}>
                { isLarge ? <FormattedMessage defaultMessage="Categories" /> : <CategoryIcon /> }
              </TableCell>
              <TableCell align={colAlign}>
                { isLarge ? <FormattedMessage defaultMessage="Total turns" /> : <PrevTurnIcon /> }
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {pastTurns.map((turn, index) => (
              <PastTurn
                {...props}
                turn={turn}
                key={turn.id}
                index={props.pastTurns.length - index}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  );
}

interface PastTurnProps extends WithEventProps {
  turn: TurnAdmin;
  index: number;
}
function PastTurn({ event, turn, index }: PastTurnProps) {
  const isLarge = useIsLarge();
  const colAlign = isLarge ? "right" : "center"
  const duration = getTurnDuration(turn) || 0;
  let color: TypographyProps["color"] = "initial";
  if (event.turnMaxDuration > 0 && duration > event.turnMaxDuration) {
    color = "error";
  }
  return (
    <TableRow>
      <TableCell component="th" scope="row">
        <strong>{index}</strong>
      </TableCell>
      <TableCell>{turn.participant.name}</TableCell>
      <TableCell align={colAlign}>
        <Typography color={color}>{durationToText(duration)}</Typography>
      </TableCell>
      <TableCell align={colAlign}>
        <SocialCategoriesList participant={turn.participant} />
      </TableCell>
      <TableCell align={colAlign}>
        {getPreviousTurns(turn.participant).length}
      </TableCell>
    </TableRow>
  );
}
