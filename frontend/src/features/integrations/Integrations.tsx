import { Typography } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Footer } from "../footer/Footer";
import { Header } from "../header/Header";
import Page from "../page/Page";

export default function Integrations() {
  const intl = useIntl();
  return (
    <Page
      title={intl.formatMessage({
        defaultMessage: "Integrations",
      })}
    >
      <Container
        maxWidth="sm"
        sx={{
          "& h1": {
            mt: 4,
          },
          "& h3": {
            mt: 6,
          },
        }}
      >
        <Header />
        <h1>
          <FormattedMessage defaultMessage="Integrations" />
        </h1>
        <IntegrationsContent />
        <Footer />
      </Container>
    </Page>
  );
}

const formatCommand = (name: React.ReactNode[]) => (
  <Typography
    component="span"
    sx={{ fontFamily: "monospace", bgcolor: "#eee", fontSize: "120%" }}
  >
    {name}
  </Typography>
);

function IntegrationsContent() {
  return (
    <React.Fragment>
      <h3>
        <FormattedMessage defaultMessage="Discord" />
      </h3>
      <Box component="p" textAlign="justify" sx={{ "& ul li": { my: 1 } }}>
        <FormattedMessage
          defaultMessage="
          Speaking List has its Discord bot, if you want to run your events on Discord. The bot has the following features:
          "
        />
        <ul>
          <li>
            <FormattedMessage
              defaultMessage="
          Create a Speaking List event with the <tt>/create</tt> command
          "
              values={{
                tt: formatCommand,
              }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          Connect a channel to an existing Speaking List event with the <tt>/connect</tt> command
          "
              values={{
                tt: formatCommand,
              }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          Register as a participant with the <tt>/register</tt> command
          "
              values={{
                tt: formatCommand,
              }}
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          Ask for a speaking turn with the <tt>/turn</tt> command
          "
              values={{
                tt: formatCommand,
              }}
            />
          </li>
        </ul>
        <FormattedMessage
          defaultMessage="
          These commands should be run in the chat channel of a voice channel. It does not replace the administration interface of Speaking List, but it will let your participants stay in Discord to register or ask for a turn.
          "
        />
      </Box>
      <Box component="p" textAlign="center" mt={3}>
        <Button
          variant="contained"
          component="a"
          href="https://discord.com/api/oauth2/authorize?client_id=1118818566528057404&permissions=35186720901120&scope=bot"
          color="primary"
          size="large"
          target="_blank"
        >
          <FormattedMessage defaultMessage="Add to Discord" />
        </Button>
      </Box>
    </React.Fragment>
  );
}
