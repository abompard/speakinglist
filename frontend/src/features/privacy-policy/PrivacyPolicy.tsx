import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Footer } from "../footer/Footer";
import { Header } from "../header/Header";
import Page from "../page/Page";

export default function PrivacyPolicy() {
  const intl = useIntl();
  return (
    <Page
      title={intl.formatMessage({
        defaultMessage: "Privacy Policy and Terms of Service",
      })}
    >
      <Container
        maxWidth="sm"
        sx={{
          "& h1": {
            mt: 4,
          },
          "& h3": {
            mt: 6,
          },
        }}
      >
        <Header />
        <h1>
          <FormattedMessage defaultMessage="Privacy Policy and Terms of Service" />
        </h1>
        <PrivacyPolicyContent />
        <Footer />
      </Container>
    </Page>
  );
}

function PrivacyPolicyContent() {
  return (
    <React.Fragment>
      <h3>
        <FormattedMessage defaultMessage="Privacy Policy" />
      </h3>
      <Box component="p" textAlign="justify" sx={{ "& ul li": { my: 1 } }}>
        <FormattedMessage
          defaultMessage="
          These are the precautions we're taking to ensure that your data stays private:
          "
        />
        <ul>
          <li>
            <FormattedMessage
              defaultMessage="
          Events are automatically deleted 30 days after they ended. This includes the list of participants and their social categories.
          "
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          We have database backups for a week to recover in case of a crash, but after that the data is gone forever.
          "
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          We do not use analytics from large corporations (such as Google Analytics). There is some usage data collected by a private Matomo instance running on a private server, and it's mostly to see how much the service is being used and adjust server power accordingly.
          "
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          When users participate or create an event, we store an identifier on your computer (pretty much like a cookie) to track who you are during the event and make it work. This identifier is only used by SpeakingList, and is not communicated to other websites.
          "
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          Event organizers can download the event statistics, but they are anonymized. Instead of participant names, they get meaningless numbers.
          "
            />
          </li>
          <li>
            <FormattedMessage
              defaultMessage="
          The software is open-source, you are welcome to go look at how it works. The code actually running on the server is an exact mirror of the code visible in the repository (but you'll have to trust us about that, unfortunately there's no way for you to verify it).
          "
            />
          </li>
        </ul>
      </Box>
      <h3>
        <FormattedMessage defaultMessage="Terms of Service" />
      </h3>
      <Box component="p" textAlign="justify">
        <FormattedMessage
          defaultMessage="
          I'm not a lawyer and I didn't find a suitable terms of service template. I'm just a human who tries to do his best. Please try to do your best as well. I'm not responsible if this application made you loose money, loose temper or loose hope.
          "
        />
      </Box>
    </React.Fragment>
  );
}
