import Container from "@mui/material/Container";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Footer } from "../footer/Footer";
import { Header } from "../header/Header";
import Page from "../page/Page";
import { ZoomableImage } from "./ZoomableImage";
import CreateImageFr from "./create.fr.gif";
import RunImageFr from "./run.fr.gif";
import StatsImageFr from "./stats.fr.png";
import TellImageFr from "./tell.fr.gif";

export default function HowTo() {
  const intl = useIntl();
  return (
    <Page title={intl.formatMessage({ defaultMessage: "How does this work?" })}>
      <Container
        maxWidth="sm"
        sx={{
          "& h1": {
            mt: 4,
          },
          "& h3": {
            mt: 6,
          },
        }}
      >
        <Header />
        <h1>
          <FormattedMessage defaultMessage="How does this work?" />
        </h1>
        <HowToContent />
        <Footer />
      </Container>
    </Page>
  );
}

function HowToContent() {
  const intl = useIntl();
  let CreateImage: string | undefined;
  let TellImage: string | undefined;
  let RunImage: string | undefined;
  let StatsImage: string | undefined;
  if (intl.locale === "fr") {
    CreateImage = CreateImageFr;
    TellImage = TellImageFr;
    RunImage = RunImageFr;
    StatsImage = StatsImageFr;
  } else {
    // TODO: redo those in English
    CreateImage = CreateImageFr;
    TellImage = TellImageFr;
    RunImage = RunImageFr;
    StatsImage = StatsImageFr;
  }
  return (
    <React.Fragment>
      <h3>
        <FormattedMessage defaultMessage="Create an event" />
      </h3>
      <p>
        <FormattedMessage
          defaultMessage="
          When you have created your videoconference on the platform you use
          (Skype, Zoom, Google Chat, Jitsi, etc), create the event in SpeakingList.
          Set the title, choose the social categories you want to ask your participants,
          set the duration limit you want, etc.
          "
        />
      </p>
      <ZoomableImage
        src={CreateImage}
        alt={intl.formatMessage({ defaultMessage: "Create an event" })}
      />
      <h3>
        <FormattedMessage defaultMessage="Tell your participants" />
      </h3>
      <p>
        <FormattedMessage
          defaultMessage="
          Share the event link to your participants, either by copying and pasting the event link
          displayed on the event information page, or by clicking on the QR button and sharing
          your screen.
          You can also tell your participants to go to the website and enter the event code.
          "
        />
      </p>
      <ZoomableImage
        src={TellImage}
        alt={intl.formatMessage({ defaultMessage: "Tell your participants" })}
      />
      <p>
        <FormattedMessage
          defaultMessage="
          We recommend that participants use SpeakingList on a different device from the videoconferencing
          software, for example a phone or a tablet, to be able to request a turn at the touch of a
          button while still watching the videoconference. They can flash the QR code
          on the other device, it will take them to the right page.
          "
        />
      </p>
      <h3>
        <FormattedMessage defaultMessage="Run the event" />
      </h3>
      <p>
        <FormattedMessage
          defaultMessage="
          Participants won't be able to request a turn until an organizer clicks on the <b>Start</b> button.
          Then, they will see a button to request a turn, and it will show up in the organizer's waiting list.
          Organizers can start the turn by clicking on the corresponding button in the waiting list.
          The participant will see that it's their turn to speak, and a counter will show how long they
          have spoken. Organizers will also see this counter. The counter will change color when they reach
          the turn duration limit.
        "
          values={{
            b: (chunks: React.ReactNode[]) => <strong>{chunks}</strong>,
          }}
        />
      </p>
      <ZoomableImage
        src={RunImage}
        alt={intl.formatMessage({ defaultMessage: "Run the event" })}
      />
      <p>
        <FormattedMessage
          defaultMessage="
          The waiting list can be re-organized, and displays information to prioritize participants:
          how many times they have spoken before, how long they have spoken during the event,
          and their social categories.
          Organizers can start the turn of anyone in the waiting list, not only the next-in-line.
          "
        />
      </p>
      <h3>
        <FormattedMessage defaultMessage="End the event" />
      </h3>
      <p>
        <FormattedMessage
          defaultMessage="
          When the event is over, organisers can mark it as stopped on the event info page.
          Participants will not be able to request turns anymore.
          The statistics page will show (hopefully) interesting information about the distribution
          of speech during the event. They can download the statistics data with the corresponding button.
          Ended events will be deleted after 30 days.
          "
        />
      </p>
      <ZoomableImage
        src={StatsImage}
        alt={intl.formatMessage({ defaultMessage: "Statistics" })}
      />
    </React.Fragment>
  );
}
