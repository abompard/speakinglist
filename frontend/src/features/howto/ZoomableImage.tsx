import CloseIcon from "@mui/icons-material/Close";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import React from "react";

interface ZoomableImageProps {
  src: string;
  alt: string;
}

export function ZoomableImage({ src, alt }: ZoomableImageProps) {
  const [open, setOpen] = React.useState(false);

  const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Button
        onClick={handleClick}
        disableRipple
        sx={{
          width: "100%",
          "& img": {
            width: "100%",
          },
        }}
      >
        <img src={src} alt={alt} />
      </Button>
      {/*       <Modal open={open} onClose={handleClose}>
        <Box textAlign="center">
          <img src={src} alt={alt} />
        </Box>
      </Modal> */}
      <Dialog
        open={open}
        onClose={handleClose}
        fullScreen
        aria-labelledby="Full size image"
      >
        <Toolbar>
          <Typography variant="h6" sx={{ flex: 1 }}>
            {alt}
          </Typography>
          <IconButton
            edge="end"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
            size="large">
            <CloseIcon />
          </IconButton>
        </Toolbar>
        <Box component={DialogContent} textAlign="center">
          <img src={src} alt={alt} />
        </Box>
      </Dialog>
    </React.Fragment>
  );
}
