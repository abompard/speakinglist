import { useLazyQuery } from "@apollo/client";
import QuitIcon from "@mui/icons-material/ExitToApp";
import IdleIcon from "@mui/icons-material/MobileOff";
import Tooltip from "@mui/material/Tooltip";
import React, { useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { Event, Participant, ParticipantAdmin } from "../../app/types";
import { durationToText } from "../../app/utils";
import "./last-seen.css";
import { CHECK_IN } from "./queries";

const CHECK_IN_INTERVAL = 30;
const IDLE_THRESHOLD = 180;
const VERY_IDLE_THRESHOLD = 600;

export function useLastSeen(participant: ParticipantAdmin) {
  const idleTime = participant.lastSeen
    ? Math.trunc((Date.now() - Date.parse(participant.lastSeen)) / 1000)
    : 0;
  let idleClass: string | undefined = undefined;
  let isIdle = false;
  let IsIdleIcon = IdleIcon;
  let idleInfo: React.ReactNode = null;
  if (!participant.isActive) {
    idleClass = "veryIdle";
    isIdle = true;
    IsIdleIcon = QuitIcon;
  } else if (participant.addedByOrga) {
    // don't display anything
  } else if (idleTime > VERY_IDLE_THRESHOLD) {
    idleClass = "veryIdle";
    isIdle = true;
  } else if (idleTime > IDLE_THRESHOLD) {
    idleClass = "idle";
    isIdle = true;
  }
  if (isIdle) {
    idleInfo = (
      <Tooltip
        title={
          participant.isActive ? (
            <FormattedMessage
              defaultMessage="Last seen {duration} ago"
              values={{ duration: durationToText(idleTime) }}
            />
          ) : (
            <FormattedMessage defaultMessage="Participant has left the event" />
          )
        }
        placement="top"
        arrow
      >
        <IsIdleIcon fontSize="small" sx={{ verticalAlign: "middle", ml: 1 }} />
      </Tooltip>
    );
  }
  return { isIdle, idleTime, idleClass, idleInfo };
}

export function useCheckIn(event: Event, participants: Participant[]) {
  const [checkIn, { stopPolling }] = useLazyQuery(CHECK_IN, {
    variables: { eventCode: event.code },
    pollInterval: CHECK_IN_INTERVAL * 1000,
  });
  useEffect(() => {
    if (!participants.map((p) => p.isActive).includes(true)) {
      return;
    }
    checkIn();
    return () => stopPolling && stopPolling();
  }, [participants, checkIn, stopPolling]);
}
