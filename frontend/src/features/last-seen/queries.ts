import { gql } from "@apollo/client";

export const CHECK_IN = gql`
  query CheckIn($eventCode: ID!) {
    event(code: $eventCode) {
      code
      checkIn {
        id
      }
    }
  }
`;
