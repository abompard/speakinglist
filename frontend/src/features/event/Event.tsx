import {
  SubscribeToMoreOptions,
  useApolloClient,
  useQuery,
} from "@apollo/client";
import Alert from "@mui/material/Alert";
import AlertTitle from "@mui/material/AlertTitle";
import Link from "@mui/material/Link";
import { ComponentType, useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { Link as RouterLink, useParams } from "react-router-dom";
import { Event } from "../../app/types";
import { CheckOrgaNotOrga } from "../check-orga/CheckOrga";
import { ErrorAlert } from "../error/Error";
import { Loading } from "../loading/Loading";
import { GET_EVENT, GET_EVENT_ORGA, SUBSCRIBE_EVENT } from "./queries";

export interface WithEventProps {
  event: Event;
}

export const withEvent =
  (
    WrappedComponent: ComponentType<{ event: Event }>,
    isOrganizer: boolean = false
  ) =>
  (props: any) => {
    const { eventCode } = useParams();
    const query = isOrganizer ? GET_EVENT_ORGA : GET_EVENT;
    const { loading, error, data, subscribeToMore } = useQuery(query, {
      variables: { code: eventCode },
    });

    if (loading)
      return (
        <Loading>
          <FormattedMessage defaultMessage="Loading event..." />
        </Loading>
      );
    if (!eventCode) {
      return (
        <Alert severity="error">
          <FormattedMessage defaultMessage="Event code is missing" />
        </Alert>
      );
    }
    if (error) {
      if (error.message === "NOT ALLOWED" && isOrganizer) {
        return <PermissionDenied eventCode={eventCode} />;
      }
      return <ErrorAlert error={error} />;
    }
    if (data.event === null)
      return (
        <Alert severity="error">
          <AlertTitle>
            <FormattedMessage defaultMessage="No such event" />
          </AlertTitle>
          <FormattedMessage
            defaultMessage="Go back to <link>the home page</link>."
            values={{
              link: (chunks: React.ReactNode[]) => (
                <Link component={RouterLink} to="/" underline="hover">
                  {chunks}
                </Link>
              ),
            }}
          />
        </Alert>
      );

    return (
      <EventWithData
        component={WrappedComponent}
        event={data.event}
        downstreamProps={props}
        subscribeToMore={subscribeToMore}
      />
    );
  };

interface EventWithDataProps {
  component: ComponentType<{ event: Event }>;
  event: Event;
  downstreamProps: any;
  subscribeToMore: (options: SubscribeToMoreOptions) => void;
}
function EventWithData(props: EventWithDataProps) {
  const WrappedComponent = props.component;
  const eventCode = props.event.code;
  const subscribeToMore = props.subscribeToMore;
  const cache = useApolloClient().cache;
  useEffect(() => {
    subscribeToMore({
      document: SUBSCRIBE_EVENT,
      variables: { code: eventCode },
      updateQuery: (prev, { subscriptionData }) => {
        console.log("Got an event update:", subscriptionData);
        if (!subscriptionData.data) return prev;
        if (subscriptionData.data.event === null) {
          // event deleted
          cache.evict({ id: cache.identify(prev.event) });
          cache.gc();
          return null;
        }
        return {
          event: {
            ...prev.event,
            ...subscriptionData.data.event,
          },
        };
      },
    });
  }, [eventCode, subscribeToMore, cache]);
  return <WrappedComponent event={props.event} {...props.downstreamProps} />;
}

function PermissionDenied({ eventCode }: { eventCode: string }) {
  const handleSuccess = () => {
    window.location.reload();
  };
  return (
    <Alert severity="error">
      <AlertTitle>
        <FormattedMessage defaultMessage="Permission denied" />
      </AlertTitle>
      <p>
        <FormattedMessage defaultMessage="You are not allowed to view the organizer interface for this event, or your authentication has expired. Please enter the code again if you are an organizer." />
      </p>
      <CheckOrgaNotOrga
        eventCode={eventCode}
        buttonText={<FormattedMessage defaultMessage="Check code" />}
        onSuccess={handleSuccess}
      />
    </Alert>
  );
}
