import { SubscribeToMoreOptions, useApolloClient } from "@apollo/client";
import Alert from "@mui/material/Alert";
import React from "react";
import { FormattedMessage } from "react-intl";
import { Me } from "../../app/types";
import { ErrorAlert } from "../error/Error";
import { WithEventProps } from "../event/Event";
import { Loading } from "../loading/Loading";
import { useMe } from "./hook";
import { SUBSCRIBE_PARTICIPANT } from "./queries";

export interface WithMeAndEventProps extends WithEventProps {
  me: Me;
}

interface WithMeProps extends WithEventProps {
  subscribe: boolean;
  component: React.ElementType;
}

export function WithMe(props: WithMeProps) {
  const { data: me, loading, error, subscribeToMore } = useMe(props.event);

  if (error) return <ErrorAlert error={error} />;
  if (loading)
    return (
      <Loading>
        <FormattedMessage defaultMessage="Loading user data..." />
      </Loading>
    );
  if (me === null)
    return (
      <Alert severity="error">
        <FormattedMessage defaultMessage="Can't load user data!" />
      </Alert>
    );

  const Component = props.component;
  return (
    <>
      {props.subscribe
        ? me.participants.map((participant) => (
            <ParticipantSubscriber
              key={participant.id}
              event={props.event}
              participantId={participant.id}
              subscribeToMore={subscribeToMore}
            />
          ))
        : null}
      <Component event={props.event} me={me} />
    </>
  );
}

type SubscribeToMoreFn = (options: SubscribeToMoreOptions) => () => void;
interface ParticipantSubscriberProps extends WithEventProps {
  participantId: number;
  subscribeToMore: SubscribeToMoreFn;
}

function ParticipantSubscriber(props: ParticipantSubscriberProps) {
  const { participantId, subscribeToMore, event } = props;
  const client = useApolloClient();

  React.useEffect(() => {
    const eventId = client.cache.identify({
      __typename: "Event",
      code: event.code,
    });
    subscribeToMore({
      document: SUBSCRIBE_PARTICIPANT,
      variables: { id: participantId },
      updateQuery: (prev, { subscriptionData }) => {
        console.log("Got participant update", subscriptionData.data);
        if (!subscriptionData.data) return prev;
        const newParticipant = subscriptionData.data.participant;
        if (!newParticipant) {
          // It has been deleted by organizers
          client.cache.modify({
            id: eventId,
            fields: {
              participant: (cached) => {
                return null;
              },
            },
          });
          // Also remove the cached object itself so it can't be referenced by turns
          client.cache.evict({
            id: client.cache.identify({
              __typename: "Participant",
              id: participantId,
            }),
          });
        }
        return newParticipant;
      },
    });
  }, [client, subscribeToMore, participantId, event]);

  return null;
}
