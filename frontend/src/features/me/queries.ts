import { gql } from "@apollo/client";

export const GET_ME = gql`
  query GetMe($eventCode: ID!) {
    event(code: $eventCode) {
      code
      me {
        participants {
          id
          isActive
        }
        organizer
        token
      }
    }
  }
`;

export const GET_ME_FULL = gql`
  query GetMeFull($eventCode: ID!) {
    event(code: $eventCode) {
      code
      me {
        organizer
        token
        participants {
          id
          name
          isActive
          addedByOrga
          isOrga
          turns {
            id
            startedAt
            endedAt
            requestedAt
          }
          socialCategories {
            id
            name
            value
          }
        }
      }
    }
  }
`;

export const SUBSCRIBE_PARTICIPANT = gql`
  subscription OnParticipantChanged($id: ID!) {
    participant(id: $id) {
      id
      name
      isActive
      addedByOrga
      isOrga
      turns {
        id
        startedAt
        endedAt
        requestedAt
      }
      socialCategories {
        id
        name
        value
      }
    }
  }
`;
