import { ApolloError, useQuery } from "@apollo/client";
import { useIntl } from "react-intl";
import { Event, Me } from "../../app/types";
import { useCheckIn } from "../last-seen/LastSeen";
import { GET_ME_FULL } from "./queries";

export function useMe(event: Event) {
  let { loading, error, data, subscribeToMore, refetch } = useQuery(
    GET_ME_FULL,
    {
      variables: { eventCode: event.code },
    }
  );
  const me: Me | null = data ? data.event.me : null;

  useCheckIn(event, me ? me.participants : []);
  const intl = useIntl();

  if (!error && !loading && !data) {
    error = new ApolloError({
      errorMessage: intl.formatMessage({
        defaultMessage: "could not get the participant",
      }),
    });
  }
  return { loading, error, data: me, refetch, subscribeToMore };
}
