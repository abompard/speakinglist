import { useMutation, useQuery } from "@apollo/client";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Link from "@mui/material/Link";
import TextField from "@mui/material/TextField";
import { Field, Form, Formik, FormikHelpers } from "formik";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { FormErrors, Participant } from "../../app/types";
import { ErrorAlert } from "../error/Error";
import { WithEventProps } from "../event/Event";
import { CHECK_ORGA, GET_ORGA } from "./queries";

interface FormValues {
  adminCode: string;
}

export function CheckOrga(props: WithEventProps) {
  const navigate = useNavigate();
  const { data, loading } = useQuery(GET_ORGA, {
    variables: { eventCode: props.event.code },
  });
  if (loading) return null;
  const eventAdminUrl = `/e/${props.event.code}/admin`;
  const handleSuccess = () => {
    navigate(eventAdminUrl);
  };
  const isOrga =
    data &&
    (data.event.me.organizer ||
      data.event.me.participants
        .map((p: Participant) => p.isOrga)
        .includes(true));
  return (
    <Box mt={4} display="flex" justifyContent="flex-end">
      {isOrga ? (
        <Link component={RouterLink} to={eventAdminUrl} underline="hover">
          <FormattedMessage defaultMessage="Go to admin" />
        </Link>
      ) : (
        <CheckOrgaNotOrga
          eventCode={props.event.code}
          buttonSize="small"
          onSuccess={handleSuccess}
        />
      )}
    </Box>
  );
}

interface CheckOrgaNotOrgaProps {
  eventCode: string;
  buttonText?: string | React.ReactNode;
  buttonSize?: "small" | "medium" | "large";
  onSuccess: () => void;
}
export function CheckOrgaNotOrga(props: CheckOrgaNotOrgaProps) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <Button
        variant="text"
        onClick={handleClickOpen}
        size={props.buttonSize}
        color="inherit"
      >
        {props.buttonText || <FormattedMessage defaultMessage="Organizer?" />}
      </Button>
      <CheckOrgaNotOrgaDialog
        eventCode={props.eventCode}
        isOpen={open}
        onClose={handleClose}
        onSuccess={props.onSuccess}
      />
    </React.Fragment>
  );
}

interface CheckOrgaNotOrgaDialogProps {
  eventCode: string;
  isOpen: boolean;
  onClose: () => void;
  onSuccess: () => void;
}

function CheckOrgaNotOrgaDialog(props: CheckOrgaNotOrgaDialogProps) {
  const [checkOrga, { loading, error }] = useMutation(CHECK_ORGA);
  const intl = useIntl();
  return (
    <Dialog
      open={props.isOpen}
      onClose={props.onClose}
      aria-labelledby="form-dialog-title"
    >
      <Formik
        initialValues={{ adminCode: "" }}
        validate={(values) => {
          const errors: FormErrors<FormValues> = {};
          if (!values.adminCode) {
            errors.adminCode = intl.formatMessage({
              defaultMessage: "Required",
            });
          }
          return errors;
        }}
        onSubmit={(
          values: FormValues,
          { setSubmitting, setFieldError }: FormikHelpers<FormValues>
        ) => {
          checkOrga({
            variables: {
              eventCode: props.eventCode,
              adminCode: values.adminCode,
            },
          }).then(
            (result) => {
              setSubmitting(false);
              const newMe = result.data.event.checkOrga.me;
              if (!newMe || !newMe.organizer) {
                setFieldError(
                  "adminCode",
                  intl.formatMessage({ defaultMessage: "Wrong code" })
                );
                return;
              }
              props.onSuccess();
            },
            (error) => {
              setSubmitting(false);
            }
          );
        }}
      >
        {({ isSubmitting, errors }) => (
          <React.Fragment>
            <DialogTitle id="form-dialog-title">
              <FormattedMessage defaultMessage="Organizer?" />
            </DialogTitle>
            <Form>
              <DialogContent>
                <DialogContentText>
                  <FormattedMessage defaultMessage="Enter the secret code if you are an organiser of this event." />
                </DialogContentText>
                <Box sx={{ textAlign: "center", mt: 2 }}>
                  <Field
                    as={TextField}
                    name="adminCode"
                    label={<FormattedMessage defaultMessage="Secret code" />}
                    error={!!errors.adminCode}
                    autoFocus
                    helperText={errors.adminCode}
                  />
                  <ErrorAlert error={error} />
                </Box>
              </DialogContent>
              <DialogActions>
                <Button onClick={props.onClose} color="inherit">
                  <FormattedMessage defaultMessage="Cancel" />
                </Button>
                <Button
                  type="submit"
                  color="primary"
                  variant="contained"
                  disabled={isSubmitting || loading}
                >
                  <FormattedMessage defaultMessage="Check code" />
                </Button>
              </DialogActions>
            </Form>
          </React.Fragment>
        )}
      </Formik>
    </Dialog>
  );
}

// export default withEvent(CheckOrga);
