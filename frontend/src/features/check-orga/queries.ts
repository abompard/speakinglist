import { gql } from "@apollo/client";

export const CHECK_ORGA = gql`
  mutation CheckOrga($eventCode: ID!, $adminCode: String!) {
    event(code: $eventCode) {
      code
      checkOrga(adminCode: $adminCode) {
        me {
          organizer
          token
          participants {
            id
            isOrga
          }
        }
      }
    }
  }
`;

export const GET_ORGA = gql`
  query GetOrga($eventCode: ID!) {
    event(code: $eventCode) {
      code
      me {
        organizer
        participants {
          id
          isOrga
        }
      }
    }
  }
`;
