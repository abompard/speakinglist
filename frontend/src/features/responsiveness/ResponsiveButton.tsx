import Button, { ButtonProps } from "@mui/material/Button";
import IconButton, { IconButtonProps } from "@mui/material/IconButton";
import Tooltip from "@mui/material/Tooltip";
import useIsLarge from "./useIsLarge";


interface ResponsiveButtonProps extends ButtonProps {
  startIcon?: never;
  icon: ButtonProps["startIcon"];
  tooltipMessage?: string;
}

export default function ResponsiveButton(props: ResponsiveButtonProps) {
  const {tooltipMessage, ...otherProps} = props
  const isLarge = useIsLarge();
  const Class = isLarge ? LargeResponsiveButton : SmallResponsiveButton;
  return (
    <Tooltip title={tooltipMessage || (!isLarge && props.children)} placement="top" arrow={!tooltipMessage}>
      <span>
        <Class {...otherProps}>{props.children}</Class>
      </span>
    </Tooltip>
  );
}


function SmallResponsiveButton(props: ResponsiveButtonProps) {
  const {icon, color, ...otherProps} = props
  const iconColor = color as IconButtonProps["color"];
  return (
    <IconButton
      {...otherProps}
      color={iconColor}
      >
      {icon}
    </IconButton>
  );

}

function LargeResponsiveButton(props: ResponsiveButtonProps) {
  const {icon, ...otherProps} = props
  return (
    <Button
      {...otherProps}
      startIcon={icon}
      >
      {props.children}
    </Button>
  );
}
