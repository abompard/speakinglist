import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';


export default function useIsLarge() {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.up('sm'));
}
