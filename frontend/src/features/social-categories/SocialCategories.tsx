import Box from "@mui/material/Box";
import MenuItem from "@mui/material/MenuItem";
import TextField from "@mui/material/TextField";
import { Field, FormikErrors } from "formik";
import * as _ from "lodash";
import React from "react";
import {
  FormattedMessage,
  MessageDescriptor,
  defineMessage,
  useIntl,
} from "react-intl";
import {
  PRIVATE,
  SOCIAL_CATEGORIES,
  SOCIAL_CATEGORY_ICONS,
  SOCIAL_CATEGORY_LABELS,
  SocialCategoryName,
} from "../../app/socialcategories";
import { Event, Participant, ParticipantAdmin } from "../../app/types";
import { WithEventProps } from "../event/Event";
import useIsLarge from "../responsiveness/useIsLarge";

export function getInitialSCValues(
  event: Event,
  participant?: Participant | null
) {
  const socialCategoryNames = event.socialCategories.map((sc) => sc.name);
  return participant && participant.socialCategories.length > 0
    ? _.fromPairs(
        participant.socialCategories.map((sc) => [sc.name, sc.value || ""])
      )
    : _.fromPairs(socialCategoryNames.map((n) => [n, ""]));
}

interface FormValues {
  sc: { [key in SocialCategoryName]?: string };
}

export function valuesToVariables(values: FormValues) {
  return _.toPairs(values.sc).map(([name, value]) => ({ name, value }));
}

interface Props extends WithEventProps {
  participant?: Participant | null;
  errors?: FormikErrors<FormValues>;
}

export function SocialCategoriesFormFields(props: Props) {
  const intl = useIntl();
  const socialCategoryNames = props.event.socialCategories.map((sc) => sc.name);
  if (socialCategoryNames.length === 0) return null;
  return (
    <div>
      {socialCategoryNames.map((name: SocialCategoryName) => {
        const category = SOCIAL_CATEGORIES[name];
        if (!category) {
          return null;
        }
        const options = [
          ...category.options,
          {
            label: defineMessage({ defaultMessage: "I don't want to say" }),
            value: PRIVATE,
          },
        ];
        return (
          <Box mb={2} key={name}>
            <Field
              as={TextField}
              name={`sc.${name}`}
              label={intl.formatMessage(category.title)}
              error={
                !!(props.errors && props.errors.sc && props.errors.sc[name])
              }
              helperText={
                props.errors && props.errors.sc ? props.errors.sc[name] : ""
              }
              select
              fullWidth={true}
              children={options.map(
                (option: { label: MessageDescriptor; value: string }) => (
                  <MenuItem key={option.value} value={option.value}>
                    <FormattedMessage {...option.label} />
                  </MenuItem>
                )
              )}
            />
          </Box>
        );
      })}
    </div>
  );
}

export function SocialCategoriesList({
  participant,
}: {
  participant: ParticipantAdmin;
}) {
  const intl = useIntl();
  const isLarge = useIsLarge();
  const content = (participant.socialCategories || [])
  .filter(
    (sc) =>
      sc.value &&
      sc.value !== PRIVATE &&
      SOCIAL_CATEGORY_LABELS[sc.name][sc.value]
  )
  .map((sc) =>
    isLarge ? intl.formatMessage(SOCIAL_CATEGORY_LABELS[sc.name][sc.value!]) : React.createElement(SOCIAL_CATEGORY_ICONS[sc.name][sc.value!], {key: sc.name})
  );
  return (
    <React.Fragment>
      {isLarge ? content.join(", ") : content}
    </React.Fragment>
  );
}
