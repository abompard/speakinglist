import CheckIcon from "@mui/icons-material/Check";
import CopyIcon from "@mui/icons-material/FileCopy";
import { useState } from "react";
import CopyToClipboard from "react-copy-to-clipboard";
import { FormattedMessage } from "react-intl";
import ResponsiveButton from "../responsiveness/ResponsiveButton";

interface CopyButtonProps {
  value: string;
  size?: "small";
}

export function CopyButton({ value, size }: CopyButtonProps) {
  const [copied, setCopied] = useState(false);
  return (
    <CopyToClipboard text={value} onCopy={() => setCopied(true)}>
      <ResponsiveButton
        size={size}
        color="inherit"
        icon={copied ? <CheckIcon /> : <CopyIcon />}
      >
        {copied ? (
          <FormattedMessage defaultMessage="Copied!" />
        ) : (
          <FormattedMessage defaultMessage="Copy" />
        )}
      </ResponsiveButton>
    </CopyToClipboard>
  );
}
