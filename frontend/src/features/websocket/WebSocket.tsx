import Fade from "@mui/material/Fade";
import Alert from "@mui/material/Alert";
import React, { ReactNode, useRef } from "react";
import useWebSocket, { ReadyState } from "react-use-websocket";
import { settings } from "../../app/config";

interface Props {
  url: string;
  protocols?: string | string[];
  onMessage?: (data: any) => void;
  onOpen?: () => void;
  onError?: (e: Event) => void;
  onClose?: (e: Event) => void;
  pingInterval: number;
  reconnect?: boolean;
  children?: ReactNode;
}

export const WebSocketManager = (props: Props) => {
  const pingTimeoutId = useRef<null | NodeJS.Timeout>(null);

  const { readyState } = useWebSocket(props.url, {
    retryOnError: true,
    shouldReconnect: (event: WebSocketEventMap["close"]) => {
      return true;
    },
    onOpen: (event: WebSocketEventMap["open"]) => {
      console.log("Websocket connected");
      props.onOpen && props.onOpen();
      const socket = event.target as WebSocket;
      if (socket) {
        pingTimeoutId.current = setInterval(() => {
          socket.send("ping");
        }, props.pingInterval * 1000);
      }
    },
    onClose: (event: WebSocketEventMap["close"]) => {
      console.log(
        `Websocket disconnected. Reason: ${event.reason}. Code: ${event.code}`,
        event
      );
      pingTimeoutId.current && clearInterval(pingTimeoutId.current);
      props.onClose && props.onClose(event);
    },
    onError: (event: WebSocketEventMap["error"]) => {
      props.onError && props.onError(event);
    },
    onMessage: (event: WebSocketEventMap["message"]) => {
      if (event.data === "pong") {
        return;
      }
      if (!props.onMessage) {
        return;
      }
      console.log("Received:", event.data);
      let message;
      try {
        message = JSON.parse(event.data);
      } catch (e) {
        console.error(`Received message is not JSON: ${e}.`);
        console.error(`Message was: ${event.data}`);
        return;
      }
      props.onMessage(message);
    },
  });

  /*
  const connectionStatus = {
    [ReadyState.CONNECTING]: "Connecting",
    [ReadyState.OPEN]: "Open",
    [ReadyState.CLOSING]: "Closing",
    [ReadyState.CLOSED]: "Not connected",
    [ReadyState.UNINSTANTIATED]: "Uninstantiated",
  }[readyState];
  */

  return (
    <React.Fragment>
      {readyState === ReadyState.CONNECTING && (
        <Fade in={true}>
          <Alert severity="warning">Connecting...</Alert>
        </Fade>
      )}
      {(readyState === ReadyState.CLOSED ||
        readyState === ReadyState.CLOSING) && (
        <Fade in={true}>
          <Alert severity="error">Network error: can't reach the server</Alert>
        </Fade>
      )}
      {props.children}
    </React.Fragment>
  );
};
WebSocketManager.defaultProps = {
  reconnect: true,
  pingInterval: settings.WEBSOCKET_PING_INTERVAL,
};

/*
interface Props {
  url: string;
  protocols?: string | string[];
  onMessage?: (data: any) => void;
  onOpen?: () => void;
  onError?: (e: Event) => void;
  onClose?: (code: number, reason: string) => void;
  pingInterval?: number;
  reconnect?: boolean;
  children?: ReactNode;
}


export const WebSocketManager = (props: Props) => {
  const ws = useRef(null);
  const reconnect: {timeout: null | NodeJS.Timeout, attempt: number} = useRef({timeout: null, attempt: 1});
  const pingTimeout: null | NodeJS.Timeout = useRef(null);

  const generateInterval = () => {
    if (settings.WEBSOCKET_RECONNECT_INTERVAL) {
      return settings.WEBSOCKET_RECONNECT_INTERVAL * 1000;
    }
    return Math.min(30, Math.pow(2, reconnect.current.attempt) - 1) * 1000;
  };

  const setupWebSocket = () => {
    console.log("Setting up websocket");
    const socket = new window.WebSocket(props.url, props.protocols);
    socket.onopen = () => {
      console.log("Websocket connected");
      props.onOpen && props.onOpen();
      setPingTimeoutId(
        setInterval(() => {
          socket.send("ping");
        }, props.pingInterval! * 1000)
      );
    };

    socket.onerror = (e) => {
      props.onError && props.onError(e);
    };

    socket.onmessage = (evt) => {
      props.onMessage && props.onMessage(evt.data);
    };

    socket.onclose = (evt) => {
      console.log(
        `Websocket disconnected. Reason: ${evt.reason}. Code: ${evt.code}`
      );
      pingTimeoutId && clearInterval(pingTimeoutId);
      props.onClose && props.onClose(evt.code, evt.reason);
      console.log("should reconnect?", shouldReconnect);
      if (shouldReconnect) {
        const time = generateInterval();
        console.log("reconnectTime : ", time);
        setReconnectTimeoutId(
          setTimeout(() => {
            setAttempt(attempt + 1);
            setupWebSocket();
          }, time)
        );
      }
    };

    ws.current = socket;
  };

  useEffect(() => {
    console.log("Beginning useEffect", socket);
    !socket && setupWebSocket();
    return () => {
      console.log("Tearing down websocket", socket);
      if (!socket) {
        return;
      }
      setShouldReconnect(false);
      reconnectTimeoutId && clearTimeout(reconnectTimeoutId);
      console.log("closing socket in teardownWebSocket", socket);
      socket.close();
    };
  }, [props.url, socket]);

  return <div>{props.children}</div>;
};

WebSocketManager.defaultProps = {
  reconnect: true,
  pingInterval: settings.WEBSOCKET_PING_INTERVAL,
};
*/
