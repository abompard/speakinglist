import React, { useEffect, useState } from "react";
import { Turn } from "../../app/types";

export type ExceededStatus = "ok" | "warning" | "over";

interface BaseDurationProps {
  warnTime?: number;
  maxTime: number;
}

export interface DurationWidgetProps extends BaseDurationProps {
  duration: number;
  exceeded: ExceededStatus;
  warnTime: number;
}

interface DurationCounterProps extends BaseDurationProps {
  turn: Turn;
  widget: React.FC<DurationWidgetProps>;
}

export function DurationCounter(props: DurationCounterProps) {
  const startedAt = Date.parse(props.turn.startedAt);
  const computeDuration = React.useCallback(
    () => Math.round((Date.now() - startedAt) / 1000),
    [startedAt]
  );
  const [duration, setDuration] = useState(computeDuration());
  useEffect(() => {
    const intervalID = window.setInterval(() => {
      setDuration(computeDuration());
    }, 1000);
    return () => {
      clearInterval(intervalID);
    };
  }, [computeDuration]);
  const Widget = props.widget;
  const warnTime = props.warnTime || props.maxTime - props.maxTime / 5;
  let exceeded: ExceededStatus = "ok";
  if (props.maxTime) {
    if (duration > props.maxTime) {
      exceeded = "over";
    } else if (duration > warnTime) {
      exceeded = "warning";
    }
  }
  return (
    <Widget
      duration={duration}
      exceeded={exceeded}
      warnTime={warnTime}
      maxTime={props.maxTime}
    />
  );
}
