/*
 * Supported social categories
 */

import WhiteIcon from "@mui/icons-material/Face";
import PoCIcon from "@mui/icons-material/FaceOutlined";
import WomanIcon from "@mui/icons-material/Female";
import ManIcon from "@mui/icons-material/Male";
import NBIcon from "@mui/icons-material/Transgender";
import * as _ from "lodash";
import React from "react";
import { defineMessage, MessageDescriptor } from "react-intl";


export type SocialCategoryName = "gender" | "race";

type SocialCategoriesType = {
  [key in SocialCategoryName]: {
    title: MessageDescriptor;
    options: { label: MessageDescriptor; value: string, icon: React.ComponentType }[];
  };
};

export const SOCIAL_CATEGORIES: SocialCategoriesType = {
  gender: {
    title: defineMessage({ defaultMessage: "Gender" }),
    options: [
      { label: defineMessage({ defaultMessage: "Man" }), value: "man", icon: ManIcon },
      { label: defineMessage({ defaultMessage: "Woman" }), value: "woman", icon: WomanIcon },
      { label: defineMessage({ defaultMessage: "Non Binary" }), value: "nb", icon: NBIcon },
    ],
  },
  race: {
    title: defineMessage({ defaultMessage: "Racialization" }),
    options: [
      { label: defineMessage({ defaultMessage: "White" }), value: "white", icon: WhiteIcon },
      {
        label: defineMessage({ defaultMessage: "Person of Color" }),
        value: "poc",
        icon: PoCIcon
      },
    ],
  },
};

export const SOCIAL_CATEGORY_LABELS = _.fromPairs(
  _.toPairs(SOCIAL_CATEGORIES).map(([catname, sc]) => [
    catname,
    _.fromPairs(sc.options.map((option) => [option.value, option.label])),
  ])
);
export const SOCIAL_CATEGORY_ICONS = _.fromPairs(
  _.toPairs(SOCIAL_CATEGORIES).map(([catname, sc]) => [
    catname,
    _.fromPairs(sc.options.map((option) => [option.value, option.icon])),
  ])
);

export const PRIVATE = "_private_";
