import { SocialCategoryName } from "./socialcategories";

// Form errors for possible fields in V
export type FormErrors<V> = { [K in keyof V]?: string };

export interface ParamTypes {
  eventCode: string;
}

export interface Event {
  id: number;
  code: string;
  title: string;
  isPaused: boolean;
  startedAt: string | null;
  endedAt: string | null;
  expiresAt: string | null;
  duration: number;
  adminCode: string;
  turnMaxDuration: number;
  participants: Participant[];
  socialCategories: SocialCategory[];
}

export interface Participant {
  id: number;
  name: string;
  isActive: boolean;
  isOrga: boolean;
  lastSeen: string;
  turns: Turn[];
  socialCategories: SocialCategory[];
}

export interface ParticipantAdmin extends Participant {
  addedByOrga: boolean;
}

interface TurnBase {
  id: number;
  position: number;
  startedAt: string;
  endedAt: string;
  requestedAt: string;
  participantId: number;
}

export interface Turn extends TurnBase {
  participant: Participant | null;
}

export interface TurnAdmin extends TurnBase {
  participant: ParticipantAdmin;
}

export interface SocialCategory {
  id: number;
  name: SocialCategoryName;
  value?: string;
}

export interface Me {
  participants: Participant[];
  organizer: boolean;
  token: string;
}

type NotificationMsgId =
  | "PARTICIPANT_ADDED"
  | "WAITING_LIST_MOVED"
  | "EVENT_STATE_CHANGED"
  | "TURN_REQUESTED"
  | "TURN_CANCELLED"
  | "TURN_STARTED"
  | "TURN_STOPPED";

type NotificationValue = { name: string; value: string };
export interface Notification {
  msgid: NotificationMsgId;
  values: NotificationValue[];
  byId: number;
}
