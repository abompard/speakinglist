import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import React from "react";
import { Navigate, Route, Routes } from "react-router-dom";
import { WithEventProps, withEvent } from "../features/event/Event";
import { Footer } from "../features/footer/Footer";
import { Header } from "../features/header/Header";
import { WithMe, WithMeAndEventProps } from "../features/me/Me";
import Page from "../features/page/Page";
import { MemberHome } from "./features/member-home/MemberHome";
import {
  ParticipantPage,
  RegistrationStatus,
} from "./features/participant/Participant";

export default function Member() {
  return (
    <Container maxWidth="sm">
      <Header />
      <MemberWithEvent />
      <Footer />
    </Container>
  );
}

const MemberWithEvent = withEvent((props: WithEventProps) => {
  return <WithMe {...props} subscribe component={MemberWithMeAndEvent} />;
});

const MemberWithMeAndEvent = (props: WithMeAndEventProps) => {
  return (
    <Page title={props.event.title}>
      <React.Fragment>
        <EventSubtitle event={props.event} me={props.me} />
        <Box p={1}>
          <Routes>
            <Route
              path="/"
              element={
                <MemberHome
                  event={props.event}
                  currentParticipants={props.me.participants}
                />
              }
            />
            <Route
              path="account"
              element={
                <ParticipantPage
                  event={props.event}
                  currentParticipants={props.me.participants}
                />
              }
            />
            <Route path="*" element={<Navigate to="/" replace />} />
          </Routes>
        </Box>
      </React.Fragment>
    </Page>
  );
};

function EventSubtitle(props: WithMeAndEventProps) {
  if (!props.event) {
    return null;
  }
  return (
    <Box px={1} component="p">
      <Routes>
        <Route path="/" element={<RegistrationStatus {...props} />} />
      </Routes>
    </Box>
  );
}
