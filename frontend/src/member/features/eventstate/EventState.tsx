import React from "react";
import { FormattedMessage } from "react-intl";
import { WithEventProps } from "../../../features/event/Event";

interface EventStateProps extends WithEventProps {
  children?: React.ReactNode;
}

export function EventState({ event, children }: EventStateProps) {
  if (event.endedAt) {
    return (
      <p>
        <FormattedMessage defaultMessage="The event has ended." />
      </p>
    );
  }
  if (event.isPaused) {
    return (
      <p>
        <FormattedMessage defaultMessage="The event is paused." />
      </p>
    );
  }
  if (!event.startedAt) {
    return (
      <p>
        <FormattedMessage defaultMessage="The event has not started yet." />
      </p>
    );
  }
  return <>{children}</>;
}
