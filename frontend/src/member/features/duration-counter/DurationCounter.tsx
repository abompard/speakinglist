import Box from "@mui/material/Box";
import LinearProgress, {
  LinearProgressProps,
} from "@mui/material/LinearProgress";
import Typography, { TypographyProps } from "@mui/material/Typography";
import { durationToText } from "../../../app/utils";
import {
  DurationWidgetProps,
  ExceededStatus,
} from "../../../features/duration-counter/DurationCounter";

export function MemberDurationWidget(props: DurationWidgetProps) {
  return (
    <TimerWithProgressBar
      duration={props.duration}
      exceeded={props.exceeded}
      maxTime={props.maxTime}
    />
  );
}

interface TimerWithProgressBarProps {
  duration: number;
  maxTime: number;
  exceeded: ExceededStatus;
  size?: "normal" | "small";
}
export function TimerWithProgressBar(props: TimerWithProgressBarProps) {
  let color: TypographyProps["color"] = "initial";
  let barColor: LinearProgressProps["color"] = "primary";
  if (props.exceeded === "over") {
    color = "error";
    barColor = "error";
  } else if (props.exceeded === "warning") {
    color = "warning";
    barColor = "warning";
  }
  const showBar = !!props.maxTime;
  const completed = Math.min((props.duration / props.maxTime) * 100, 100);
  const size = props.size || "normal";
  return (
    <Box
      sx={{
        width: size === "small" ? 100 : 200,
        marginLeft: "auto",
        marginRight: "auto",
        pb: size === "small" ? 0 : 2,
      }}
    >
      <Typography
        variant={size === "small" ? "h6" : "h5"}
        align="center"
        color={color}
      >
        {durationToText(props.duration)}
      </Typography>
      {showBar ? (
        <LinearProgress
          color={barColor}
          variant="determinate"
          value={completed}
          sx={size === "small" ? {} : { mt: 1, mb: -1 }}
        />
      ) : null}
    </Box>
  );
}
