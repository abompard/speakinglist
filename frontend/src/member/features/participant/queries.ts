import { gql } from "@apollo/client";

export const REGISTER_PARTICIPANT = gql`
  mutation RegisterParticipant(
    $eventCode: ID!
    $name: String!
    $socialCategories: [SocialCategoryInput]
  ) {
    event(code: $eventCode) {
      code
      registerParticipant(name: $name, socialCategories: $socialCategories) {
        participant {
          id
          name
          addedByOrga
          socialCategories {
            id
            name
            value
          }
        }
        me {
          participants {
            id
          }
          token
        }
      }
    }
  }
`;

export const UPDATE_PARTICIPANT = gql`
  mutation UpdateParticipant(
    $eventCode: ID!
    $participantId: ID!
    $name: String!
    $socialCategories: [SocialCategoryInput]
    $me: Boolean
  ) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        update(name: $name, socialCategories: $socialCategories, me: $me) {
          id
          name
          socialCategories {
            id
            name
            value
          }
        }
      }
    }
  }
`;

export const PARTICIPANT_SET_ACTIVE = gql`
  mutation ParticipantSetActive(
    $eventCode: ID!
    $participantId: ID!
    $isActive: Boolean!
  ) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        setActive(isActive: $isActive) {
          id
          isActive
        }
      }
    }
  }
`;

export const UPDATE_SUBSCRIPTION_TOKEN = gql`
  subscription OnTokenChanged($token: String!) {
    updateSubscriptionToken(token: $token)
  }
`;
