import {
  DocumentNode,
  gql,
  useApolloClient,
  useMutation,
} from "@apollo/client";
import RegisterIcon from "@mui/icons-material/Create";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import LeaveIcon from "@mui/icons-material/MeetingRoom";
import JoinIcon from "@mui/icons-material/PersonAdd";
import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Link from "@mui/material/Link";
import TextField from "@mui/material/TextField";
import { Field, Form, Formik, FormikHelpers } from "formik";
import { FormattedMessage, useIntl } from "react-intl";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { SocialCategoryName } from "../../../app/socialcategories";
import { FormErrors, Participant } from "../../../app/types";
import { ErrorAlert } from "../../../features/error/Error";
import { WithEventProps } from "../../../features/event/Event";
import { WithMeAndEventProps } from "../../../features/me/Me";
import {
  SocialCategoriesFormFields,
  getInitialSCValues,
  valuesToVariables,
} from "../../../features/social-categories/SocialCategories";
import {
  PARTICIPANT_SET_ACTIVE,
  REGISTER_PARTICIPANT,
  UPDATE_PARTICIPANT,
  UPDATE_SUBSCRIPTION_TOKEN,
} from "./queries";

export interface EventAndParticipantsProps extends WithEventProps {
  currentParticipants: Participant[];
}

export interface EventAndParticipantProps extends WithEventProps {
  participant: Participant;
}

export interface EventAndParticipantIdProps extends WithEventProps {
  participantId: number;
}

export function ParticipantPage(props: EventAndParticipantsProps) {
  return (
    <>
      {props.currentParticipants.map((participant) => {
        const form = (
          <ParticipantForm
            key={participant.id}
            event={props.event}
            participant={participant}
            mutation={UPDATE_PARTICIPANT}
          />
        );
        return (
          <Box key={participant.id} mb={5}>
            {props.currentParticipants.length > 1 ? (
              <Card>
                <CardContent>{form}</CardContent>
              </Card>
            ) : (
              form
            )}
          </Box>
        );
      })}
      <Box mb={5}>
        <OneMoreWrapper collapsed={props.currentParticipants.length > 0}>
          <ParticipantForm
            event={props.event}
            participant={null}
            mutation={REGISTER_PARTICIPANT}
          />
        </OneMoreWrapper>
      </Box>
      <p>
        <EventLink event={props.event} />
      </p>
    </>
  );
}

interface OneMoreWrapperProps {
  collapsed: boolean;
  children: React.ReactNode;
}
function OneMoreWrapper(props: OneMoreWrapperProps) {
  if (props.collapsed) {
    return (
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} id="register-more">
          <FormattedMessage defaultMessage="Other participants with you?" />
          <Box component="strong" ml={1}>
            <FormattedMessage defaultMessage="Register someone else." />
          </Box>
        </AccordionSummary>
        <AccordionDetails>{props.children}</AccordionDetails>
      </Accordion>
    );
  } else {
    return <>{props.children}</>;
  }
}

function EventLink({ event }: WithEventProps) {
  const eventUrl = `/e/${event.code}`;
  return (
    <FormattedMessage
      defaultMessage="Go back to <a>{title}</a>."
      values={{
        a: (chunks: React.ReactNode[]) => (
          <Link component={RouterLink} to={eventUrl} underline="hover">
            {chunks}
          </Link>
        ),
        title: event.title,
      }}
    />
  );
}

interface ParticipantFormProps extends WithEventProps {
  participant: Participant | null;
  mutation: DocumentNode;
}

interface FormValues {
  name: string;
  sc: { [key in SocialCategoryName]?: string };
}

function ParticipantForm(props: ParticipantFormProps) {
  const [mutate, { loading, error }] = useMutation(props.mutation);

  const eventUrl = `/e/${props.event.code}`;
  const navigate = useNavigate();
  const intl = useIntl();
  const apollo = useApolloClient();
  const socialCategoryNames = props.event.socialCategories.map((sc) => sc.name);
  const initialValues = {
    name: props.participant ? props.participant.name : "",
    sc: getInitialSCValues(props.event, props.participant),
  };
  return (
    <Formik
      initialValues={initialValues}
      validate={(values) => {
        const errors: FormErrors<FormValues> = {};
        if (!values.name) {
          errors.name = intl.formatMessage({ defaultMessage: "Required" });
        }
        return errors;
      }}
      onSubmit={(
        values: FormValues,
        { setSubmitting }: FormikHelpers<FormValues>
      ) => {
        mutate({
          variables: {
            eventCode: props.event.code,
            participantId: props.participant ? props.participant.id : undefined,
            name: values.name,
            socialCategories: valuesToVariables(values),
          },
          update: (cache, { data }) => {
            if (!data.event.registerParticipant) {
              return;
            }
            const newParticipantRef = cache.writeFragment({
              data: data.event.registerParticipant.participant,
              fragment: gql`
                fragment NewParticipant on Participant {
                  id
                }
              `,
            });
            cache.modify({
              id: cache.identify({
                __typename: "Event",
                code: props.event.code,
              }),
              fields: {
                // Add the new participant to the list of participants (useful when admins register as participants)
                participants: (existingParticipants = []) => {
                  return [...existingParticipants, newParticipantRef];
                },
              },
            });
            cache.modify({
              id: cache.identify({
                __typename: "Me",
              }),
              fields: {
                // Add the new participant to the list of participants
                participants: (existingParticipants = []) => {
                  return [...existingParticipants, newParticipantRef];
                },
              },
            });
          },
        }).then(
          (result) => {
            // Don't set submitting to false as we're going to redirect away
            //setSubmitting(false);
            // Run a fake subscription to update the token in the connection parameters
            // https://github.com/apollographql/subscriptions-transport-ws/issues/171
            if (result.data.event.registerParticipant) {
              const token = result.data.event.registerParticipant.me.token;
              apollo.subscribe({
                query: UPDATE_SUBSCRIPTION_TOKEN,
                variables: { token },
              });
            }
            navigate(eventUrl);
          },
          (error) => {
            setSubmitting(false);
            return error;
          }
        );
      }}
    >
      {({ isSubmitting, errors }) => (
        <Form>
          <Box mb={4}>
            <Field
              as={TextField}
              name="name"
              label={<FormattedMessage defaultMessage="Name" />}
              error={!!errors.name}
              fullWidth={true}
              helperText={
                errors.name ||
                intl.formatMessage({
                  defaultMessage: "Use the same name as in the video call.",
                })
              }
            />
          </Box>
          {socialCategoryNames.length > 0 && (
            <p>
              <FormattedMessage
                defaultMessage="
                  Please tell us a few more things about yourself. We use this
                  information to make the discussion fairer.
                "
              />
            </p>
          )}
          <SocialCategoriesFormFields
            event={props.event}
            participant={props.participant}
            errors={errors}
          />
          <Box mt={3}>
            <Button
              variant="contained"
              color="primary"
              type="submit"
              disabled={isSubmitting || loading}
              startIcon={<RegisterIcon />}
            >
              {props.participant ? (
                <FormattedMessage defaultMessage="Update" />
              ) : (
                <FormattedMessage defaultMessage="Register" />
              )}
            </Button>
            <ErrorAlert error={error} />
          </Box>
        </Form>
      )}
    </Formik>
  );
}

export function LeaveEvent(props: EventAndParticipantProps) {
  const [mutate, { loading, error }] = useMutation(PARTICIPANT_SET_ACTIVE);
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
        isActive: false,
      },
    });
  return (
    <div>
      <Button
        variant="contained"
        onClick={onClick}
        disabled={loading}
        size="small"
        startIcon={<LeaveIcon />}
        color="primary"
      >
        <FormattedMessage defaultMessage="Leave event" />
      </Button>
      <ErrorAlert error={error} />
    </div>
  );
}

export function RejoinEvent(props: EventAndParticipantProps) {
  const [mutate, { loading, error }] = useMutation(PARTICIPANT_SET_ACTIVE);
  const onClick = () =>
    mutate({
      variables: {
        eventCode: props.event.code,
        participantId: props.participant.id,
        isActive: true,
      },
    });
  return (
    <div>
      <Button
        variant="contained"
        onClick={onClick}
        disabled={loading}
        size="small"
        startIcon={<JoinIcon />}
      >
        <FormattedMessage defaultMessage="Re-join event" />
      </Button>
      <ErrorAlert error={error} />
    </div>
  );
}

export function RegistrationStatus(props: WithMeAndEventProps) {
  if (props.me.participants.length === 0) return null; // server rejected the participant
  return (
    <Box component="span" display="block">
      <FormattedMessage
        defaultMessage="Logged in as {names}."
        values={{ names: props.me.participants.map((p) => p.name).join(" & ") }}
      />{" "}
      <Button
        component={RouterLink}
        to={`/e/${props.event.code}/account`}
        color="inherit"
        size="small"
      >
        <FormattedMessage defaultMessage="Edit" />
      </Button>
    </Box>
  );
}
