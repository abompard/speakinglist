import { CardContent } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Card from "@mui/material/Card";
import RegisterIcon from "@mui/icons-material/Create";
import React from "react";
import { FormattedMessage } from "react-intl";
import QRCode from "react-qr-code";
import { Link as RouterLink } from "react-router-dom";
import { settings } from "../../../app/config";
import { Participant } from "../../../app/types";
import { CheckOrga } from "../../../features/check-orga/CheckOrga";
import { WithEventProps } from "../../../features/event/Event";
import { EventState } from "../eventstate/EventState";
import {
  EventAndParticipantsProps,
  LeaveEvent,
  RejoinEvent,
} from "../participant/Participant";
import { TurnAction } from "../turn/Turn";

export const MemberHome = (props: EventAndParticipantsProps) => {
  return (
    <div className="Member">
      {props.currentParticipants.length > 0 ? (
        <MemberWithParticipant {...props} />
      ) : (
        <JoinLink event={props.event} />
      )}
      <CheckOrga event={props.event} />
    </div>
  );
};

function MemberWithParticipant(props: EventAndParticipantsProps) {
  return (
    <div>
      {props.currentParticipants.map((participant) => (
        <MemberNameCard
          {...props}
          participant={participant}
          key={participant.id}
        >
          {participant.isActive ? (
            <>
              <EventState event={props.event}>
                <TurnAction
                  event={props.event}
                  participant={participant}
                  multiple={props.currentParticipants.length > 1}
                />
              </EventState>
              <LeaveEvent event={props.event} participant={participant} />
            </>
          ) : (
            <>
              <p>
                {props.currentParticipants.length > 1 ? (
                  <FormattedMessage
                    defaultMessage="{name} has left the event."
                    values={{ name: participant.name }}
                  />
                ) : (
                  <FormattedMessage defaultMessage="You have left the event." />
                )}
              </p>
              <RejoinEvent event={props.event} participant={participant} />
            </>
          )}
        </MemberNameCard>
      ))}
    </div>
  );
}

interface MemberNameCardProps extends EventAndParticipantsProps {
  participant: Participant;
  children: React.ReactNode;
}
function MemberNameCard(props: MemberNameCardProps) {
  if (props.currentParticipants.length > 1) {
    return (
      <Box mb={5}>
        <Card variant="outlined">
          <CardContent>
            {/*
            <Box color="textSecondary" mb={2} fontWeight="fontWeightBold">
              {props.participant.name}
            </Box>
            */}
            {props.children}
          </CardContent>
        </Card>
      </Box>
    );
  } else {
    return <>{props.children}</>;
  }
}

function JoinLink({ event }: WithEventProps) {
  const eventUrl = `${settings.PUBLIC_URL}/e/${event.code}`;
  return (
    <React.Fragment>
      <p>
        <FormattedMessage defaultMessage="To participate, you need to tell us who you are:" />
      </p>
      <Box component="p" textAlign="center">
        <Button
          variant="contained"
          component={RouterLink}
          to={`/e/${event.code}/account`}
          color="primary"
          size="large"
          startIcon={<RegisterIcon />}
        >
          <FormattedMessage defaultMessage="Register" />
        </Button>
      </Box>
      <Box component="p" mt={5} pt={3} borderTop="1px dotted #555">
        <FormattedMessage defaultMessage="To use this application on a different device, flash this code:" />
      </Box>
      <Box textAlign="center">
        <QRCode value={eventUrl} size={128} />
      </Box>
    </React.Fragment>
  );
}
