import { gql, MutationUpdaterFn } from "@apollo/client";

export const REQUEST_TURN = gql`
  mutation RequestTurn($eventCode: ID!, $participantId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        requestTurn {
          turn {
            id
            startedAt
            endedAt
            requestedAt
          }
        }
      }
    }
  }
`;

export const CANCEL_TURN = gql`
  mutation CancelTurn($eventCode: ID!, $participantId: ID!, $turnId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        turn(id: $turnId) {
          cancel {
            turn {
              id
              startedAt
              endedAt
            }
          }
        }
      }
    }
  }
`;

export const END_TURN = gql`
  mutation EndTurn($eventCode: ID!, $participantId: ID!, $turnId: ID!) {
    event(code: $eventCode) {
      code
      participant(id: $participantId) {
        id
        turn(id: $turnId) {
          end {
            turn {
              id
              endedAt
            }
          }
        }
      }
    }
  }
`;

export const mutationTurnUpdate: MutationUpdaterFn = (cache, { data }) => {
  if (!data!.event.participant.requestTurn) {
    // There's an error
    console.log("There has been an error, aborting mutationTurnUpdate");
    return;
  }
  console.log("Writing the turn to the cache");
  const newTurnRef = cache.writeFragment({
    data: data!.event.participant.requestTurn.turn,
    fragment: gql`
      fragment NewTurn on Turn {
        id
        startedAt
        endedAt
      }
    `,
  });
  console.log("Adding the turn to the participant's turns");
  cache.modify({
    id: cache.identify({
      __typename: "Participant",
      id: data!.event.participant.id,
    }),
    fields: {
      turns(existingTurns = []) {
        return [...existingTurns, newTurnRef];
      },
    },
  });
  console.log("Adding the turn to the event's turns");
  cache.modify({
    id: cache.identify({
      __typename: "Event",
      code: data!.event.code,
    }),
    fields: {
      turns(existingTurns = []) {
        return [...existingTurns, newTurnRef];
      },
    },
  });
};
