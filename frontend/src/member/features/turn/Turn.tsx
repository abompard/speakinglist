import { DocumentNode, MutationUpdaterFn, useMutation } from "@apollo/client";
import CancelIcon from "@mui/icons-material/Cancel";
import DoneIcon from "@mui/icons-material/Done";
import AskTurnIcon from "@mui/icons-material/PanTool";
import Box from "@mui/material/Box";
import Button, { ButtonTypeMap } from "@mui/material/Button";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { grey } from "@mui/material/colors";
import { styled } from "@mui/system";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";
import { Turn } from "../../../app/types";
import { getCurrentTurn, getPendingTurn } from "../../../app/utils";
import { DurationCounter } from "../../../features/duration-counter/DurationCounter";
import { ErrorAlert } from "../../../features/error/Error";
import { MemberDurationWidget } from "../duration-counter/DurationCounter";
import { Notification } from "../notification/Notification";
import NotifSound from "../notification/notification_simple-02.mp3";
import { EventAndParticipantProps } from "../participant/Participant";
import {
  CANCEL_TURN,
  END_TURN,
  REQUEST_TURN,
  mutationTurnUpdate,
} from "./queries";

interface TurnActionProps extends EventAndParticipantProps {
  multiple: boolean;
}

const FlashingCardContent = styled(CardContent)(({ theme }) => ({
  "@keyframes flash": {
    "0%, 40%, 80%": {
      backgroundColor: theme.palette.secondary.main,
      color: theme.palette.secondary.contrastText,
    },
    "20%, 60%, 100%": {
      backgroundColor: grey[50],
    },
  },
  animation: "flash 1s",
}));

export function TurnAction(props: TurnActionProps) {
  const commonProps = {
    event: props.event,
    participant: props.participant,
    multiple: props.multiple,
  };
  const currentTurn = getCurrentTurn(props.participant);
  const pendingTurn = getPendingTurn(props.participant);
  let Component: typeof PendingTurn = NoPendingTurn;
  let turn: Turn | null = null;
  let CardContentComponent: typeof CardContent | typeof FlashingCardContent =
    CardContent;
  if (currentTurn !== null) {
    Component = CurrentTurn;
    turn = currentTurn;
    CardContentComponent = FlashingCardContent;
  } else if (pendingTurn !== null) {
    Component = PendingTurn;
    turn = pendingTurn;
  }
  return (
    <Card sx={{ mb: 4 }}>
      <CardContentComponent
        sx={{
          backgroundColor: grey[50],
          textAlign: "center",
          "&:last-child": {
            pb: 5,
          },
        }}
      >
        <Component {...commonProps} turn={turn} />
      </CardContentComponent>
    </Card>
  );
}

interface TurnProps extends EventAndParticipantProps {
  multiple: boolean;
  turn: Turn | null;
}

function NoPendingTurn(props: TurnProps) {
  const intl = useIntl();
  return (
    <>
      <StatusMessage
        multiple={props.multiple}
        messageMultiple={
          <FormattedMessage
            defaultMessage="{name} does not have a pending turn"
            values={{ name: props.participant.name }}
          />
        }
        messageSingle={
          <FormattedMessage defaultMessage="You don't have a pending turn" />
        }
      />
      <TurnButton
        label={intl.formatMessage({ defaultMessage: "Ask for a turn" })}
        icon={<AskTurnIcon />}
        mutation={REQUEST_TURN}
        mutationVars={{
          eventCode: props.event.code,
          participantId: props.participant.id,
        }}
        mutationUpdate={mutationTurnUpdate}
        {...props}
      />
    </>
  );
}

function PendingTurn(props: TurnProps) {
  const intl = useIntl();
  if (!props.turn) return null;
  return (
    <>
      <StatusMessage
        multiple={props.multiple}
        messageMultiple={
          <FormattedMessage
            defaultMessage="{name} has a pending turn"
            values={{ name: props.participant.name }}
          />
        }
        messageSingle={
          <FormattedMessage defaultMessage="You have a pending turn" />
        }
      />
      <TurnButton
        label={intl.formatMessage({ defaultMessage: "Cancel" })}
        icon={<CancelIcon />}
        mutation={CANCEL_TURN}
        color="error"
        mutationVars={{
          eventCode: props.event.code,
          participantId: props.participant.id,
          turnId: props.turn.id,
        }}
        {...props}
      />
    </>
  );
}

function CurrentTurn(props: TurnProps) {
  const intl = useIntl();
  if (!props.turn) return null;
  return (
    <>
      <StatusMessage
        multiple={props.multiple}
        messageMultiple={
          <FormattedMessage
            defaultMessage="It's {name}'s turn to talk!"
            values={{ name: props.participant.name }}
          />
        }
        messageSingle={
          <FormattedMessage defaultMessage="It's your turn to talk!" />
        }
      />
      <Box my={3}>
        <DurationCounter
          turn={props.turn}
          maxTime={props.event.turnMaxDuration}
          widget={MemberDurationWidget}
        />
      </Box>
      <TurnButton
        label={intl.formatMessage({ defaultMessage: "I'm done" })}
        icon={<DoneIcon />}
        mutation={END_TURN}
        mutationVars={{
          eventCode: props.event.code,
          participantId: props.participant.id,
          turnId: props.turn.id,
        }}
        {...props}
      />
      <Notification
        message={intl.formatMessage(
          {
            defaultMessage: "You can start talking now, {name}.",
          },
          { name: props.participant.name }
        )}
        sound={NotifSound}
      />
    </>
  );
}

interface TurnButtonProps extends TurnProps {
  label: string;
  icon?: React.ReactNode;
  mutation: DocumentNode;
  mutationVars: { [key: string]: string | number };
  mutationUpdate?: MutationUpdaterFn;
  color?: ButtonTypeMap["props"]["color"];
}

function TurnButton(props: TurnButtonProps) {
  const [mutate, { loading, error }] = useMutation(props.mutation);
  const onClick = () =>
    mutate({
      variables: props.mutationVars,
      update: props.mutationUpdate,
    });
  return (
    <>
      <Button
        variant="contained"
        color={props.color || "primary"}
        onClick={onClick}
        disabled={loading}
        size="large"
        startIcon={props.icon}
      >
        {props.label}
      </Button>
      <ErrorAlert error={error} />
    </>
  );
}

interface StatusMessageProps {
  multiple: boolean;
  messageSingle: React.ReactNode;
  messageMultiple: React.ReactNode;
}
function StatusMessage(props: StatusMessageProps) {
  return (
    <Box component="p" sx={{ fontSize: "120%" }}>
      {props.multiple ? props.messageMultiple : props.messageSingle}
    </Box>
  );
}
