import { useApolloClient } from "@apollo/client/react";
import React, { ReactNode } from "react";
import { settings } from "../../../app/config";
import { WebSocketManager } from "../../../features/websocket/WebSocket";
import { EventAndParticipantProps } from "../participant/Participant";

interface Props extends EventAndParticipantProps {
  children?: ReactNode;
}

interface Message {
  typename: string;
  obj: {
    id: number;
    __typename: string;
    [key: string]: any;
  };
}

export function Updater(props: Props) {
  const client = useApolloClient();
  const onMessage = (message: Message) => {
    console.log("Received message", message);
    client.cache.modify({
      id: client.cache.identify(message.obj),
      fields(fieldValue, details) {
        const newValue = message.obj[details.fieldName];
        if (typeof newValue === "undefined") {
          return fieldValue;
        }
        return message.obj[details.fieldName];
      },
    });
  };
  return (
    <WebSocketManager
      url={`${settings.WEBSOCKET_URL}/p/${props.participant.id}`}
      onMessage={onMessage}
    >
      {props.children}
    </WebSocketManager>
  );
}
