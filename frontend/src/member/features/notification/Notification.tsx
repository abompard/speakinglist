import CloseIcon from "@mui/icons-material/Close";
import IconButton from "@mui/material/IconButton";
import Slide from "@mui/material/Slide";
import Snackbar from "@mui/material/Snackbar";
import React from "react";
import Logo from "../../../features/header/logo.png";

function notify(message: string) {
  let notification: Notification | null = null;
  const options = {
    icon: Logo,
    badge: Logo,
    vibrate: [200, 100, 200],
    tag: "Speaking List",
  };
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }
  // Let's check whether notification permissions have already been granted
  else if (window.Notification.permission === "granted") {
    // If it's okay let's create a notification
    notification = new window.Notification(message, options);
  }
  // Otherwise, we need to ask the user for permission
  else if (window.Notification.permission !== "denied") {
    window.Notification.requestPermission().then(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        notification = new window.Notification(message, options);
      }
    });
  }
  // At last, if the user has denied notifications, and you
  // want to be respectful there is no need to bother them any more.
  return notification;
}

interface NotificationProps {
  message: string;
  sound?: string;
}
export function Notification(props: NotificationProps) {
  const [open, setOpen] = React.useState(true);

  const handleClose = (
    event: Event | React.SyntheticEvent,
    reason?: string
  ) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  // HTML5 Notification
  React.useEffect(() => {
    const notification = notify(props.message);
    return () => {
      if (notification) notification.close();
    };
  }, [props.message]);
  // Play a sound
  React.useEffect(() => {
    if (!props.sound) {
      return;
    }
    const notifAudio = new Audio(props.sound);
    notifAudio.play();
    return () => {
      notifAudio.pause();
    };
  }, [props.sound]);
  /*
  // Change the title
  React.useEffect(() => {
    const titleOrig = document.title;
    const interval = setInterval(() => {
      document.title =
        document.title === titleOrig ? `(*) ${titleOrig} (*)` : titleOrig;
    }, 1000);
    return () => {
      clearInterval(interval);
      document.title = titleOrig;
    };
  }, []);
  */

  return (
    <>
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={5000}
        onClose={handleClose}
        message={props.message}
        TransitionComponent={Slide}
        action={
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleClose}
          >
            <CloseIcon fontSize="small" />
          </IconButton>
        }
      />
    </>
  );
}
