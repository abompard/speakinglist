const { addBabelPlugins, addWebpackModuleRule, override } = require('customize-cra');

module.exports = override(
  ...addBabelPlugins([
    'react-intl',
    {
      idInterpolationPattern: '[sha512:contenthash:base64:6]',
      extractFromFormatMessageCall: true,
      ast: true,
    },
    "graphql-tag"
  ]),
  addWebpackModuleRule({
    test: /\.mp3$/,
    use: {
      loader: "file-loader",
      options: {
        name: 'static/media/[name].[hash:8].[ext]'
      }
    }
  })
);
