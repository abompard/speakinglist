import csv
from urllib.parse import quote

from starlette.requests import Request
from starlette.responses import Response, StreamingResponse

from .auth import user_from_jwt
from .dataloaders import create_loaders
from .models import Event


class Echo:
    """An object that implements just the write method of the file-like
    interface.
    """

    def write(self, value):
        """Write the value by returning it, instead of storing in a buffer."""
        return value


COLUMNS = (
    "participant",
    "timestamp",
    "duration",
    "requested-at",
    "speeches",
    "is-organizer",
    "exceeded",
)


async def _get_csv(event):
    columns = list(COLUMNS)
    social_categories = [sc.name for sc in await event.get_social_categories()]

    async def _get_social_categories(participant):
        participant_categories = {
            sc.name: sc.value for sc in await participant.get_social_categories()
        }
        return [
            participant_categories.get(category, "") for category in social_categories
        ]

    columns.extend(social_categories)
    yield columns
    loaders = create_loaders()
    # Export turns
    for turn in await event.get_past_turns():
        participant = await loaders["participant"].load(turn.participant_id)
        line = [
            participant.id,
            int(turn.started_at.timestamp()),
            turn.duration,
            int(turn.requested_at.timestamp()),
            len(await participant.get_previous_turns()),
            str(await participant.is_orga()).upper(),
            str(turn.duration > event.turn_max_duration).upper(),
        ]
        line.extend(await _get_social_categories(participant))
        yield line
    # Export participants who did not speak
    participants = await loaders["participants_in_event"].load(event.id)
    for participant in participants:
        if len(await participant.get_previous_turns()) != 0:
            continue  # covered above
        line = [
            participant.id,
            None,
            None,
            None,
            0,
            str(await participant.is_orga()).upper(),
            "FALSE",
        ]
        line.extend(await _get_social_categories(participant))
        yield line


async def get_csv_stats(event):
    pseudo_buffer = Echo()
    writer = csv.writer(pseudo_buffer)
    async for row in _get_csv(event):
        yield writer.writerow(row)


async def stats_view(request: Request):
    event_code = request.path_params["event_code"]
    filename = request.path_params["filename"] + ".csv"
    event = await Event.get_by_code(event_code)
    if event is None:
        return Response(
            f"No event with code {event_code}\n",
            status_code=404,
            media_type="text/plain",
        )

    # authn & authz
    user = await user_from_jwt(request, "DL")
    if not user.is_authenticated or not await user.is_orga_for(event.id):
        return Response(
            "You are now allowed to see this.\n",
            status_code=403,
            media_type="text/plain",
        )
    data = get_csv_stats(event)

    content_disposition_filename = quote(filename)
    if content_disposition_filename != filename:
        content_disposition = "attachment; filename*=utf-8''{}".format(
            content_disposition_filename
        )
    else:
        content_disposition = 'attachment; filename="{}"'.format(filename)

    return StreamingResponse(
        data,
        media_type="text/csv",
        headers={"content-disposition": content_disposition},
    )
