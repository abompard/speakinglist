"""Organizers ondelete

Revision ID: fba789cf8ccb
Revises: 327d98eb3b12
Create Date: 2021-03-05 13:52:10.397750

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "fba789cf8ccb"
down_revision = "327d98eb3b12"
branch_labels = None
depends_on = None


def upgrade():
    op.drop_constraint(
        "fk_organizers_event_id_events", "organizers", type_="foreignkey"
    )
    op.drop_constraint("fk_organizers_user_id_users", "organizers", type_="foreignkey")
    op.create_foreign_key(
        op.f("fk_organizers_event_id_events"),
        "organizers",
        "events",
        ["event_id"],
        ["id"],
        ondelete="CASCADE",
    )
    op.create_foreign_key(
        op.f("fk_organizers_user_id_users"),
        "organizers",
        "users",
        ["user_id"],
        ["id"],
        ondelete="CASCADE",
    )


def downgrade():
    op.drop_constraint(
        op.f("fk_organizers_user_id_users"), "organizers", type_="foreignkey"
    )
    op.drop_constraint(
        op.f("fk_organizers_event_id_events"), "organizers", type_="foreignkey"
    )
    op.create_foreign_key(
        "fk_organizers_user_id_users", "organizers", "users", ["user_id"], ["id"]
    )
    op.create_foreign_key(
        "fk_organizers_event_id_events", "organizers", "events", ["event_id"], ["id"]
    )
