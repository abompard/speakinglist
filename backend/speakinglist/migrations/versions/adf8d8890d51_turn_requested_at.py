"""Turn.requested_at

Revision ID: adf8d8890d51
Revises: fba789cf8ccb
Create Date: 2021-03-12 14:04:44.742544

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "adf8d8890d51"
down_revision = "fba789cf8ccb"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("turns", sa.Column("requested_at", sa.DateTime(), nullable=False))


def downgrade():
    op.drop_column("turns", "requested_at")
