"""Make some foreign keys not nullable.

Revision ID: 49ba231da2b6
Revises: adf8d8890d51
Create Date: 2021-04-27 08:33:53.112735

"""
from alembic import op
from sqlalchemy.dialects import mysql


revision = "49ba231da2b6"
down_revision = "adf8d8890d51"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        "events_social_categories",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "events_social_categories",
        "social_category_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "organizers",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "organizers",
        "user_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "participants",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "turns",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )
    op.alter_column(
        "turns",
        "participant_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=False,
    )


def downgrade():
    op.alter_column(
        "turns",
        "participant_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "turns",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "participants",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "organizers",
        "user_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "organizers",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "events_social_categories",
        "social_category_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
    op.alter_column(
        "events_social_categories",
        "event_id",
        existing_type=mysql.INTEGER(display_width=11),
        nullable=True,
    )
