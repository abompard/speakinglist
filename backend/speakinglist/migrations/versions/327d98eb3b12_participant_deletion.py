"""Participant deletion

Revision ID: 327d98eb3b12
Revises: 23b61f77ebf4
Create Date: 2021-02-05 21:20:59.074566

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "327d98eb3b12"
down_revision = "23b61f77ebf4"
branch_labels = None
depends_on = None


def upgrade():
    with op.batch_alter_table("events") as batch_op:
        batch_op.alter_column("created_at", existing_type=sa.DATETIME(), nullable=False)
    with op.batch_alter_table("events_social_categories") as batch_op:
        batch_op.drop_constraint("fk_social_categories_id", type_="foreignkey")
        batch_op.drop_constraint("fk_events_id", type_="foreignkey")
        batch_op.create_foreign_key(
            None,
            "social_categories",
            ["social_category_id"],
            ["id"],
            ondelete="CASCADE",
        )
        batch_op.create_foreign_key(
            None, "events", ["event_id"], ["id"], ondelete="CASCADE",
        )
    with op.batch_alter_table("participants_social_categories") as batch_op:
        batch_op.drop_constraint("fk_participants_id", type_="foreignkey")
        batch_op.drop_constraint("fk_social_categories_id", type_="foreignkey")
        batch_op.create_foreign_key(
            None, "participants", ["participant_id"], ["id"], ondelete="CASCADE",
        )
        batch_op.create_foreign_key(
            None,
            "social_categories",
            ["social_category_id"],
            ["id"],
            ondelete="CASCADE",
        )


def downgrade():
    with op.batch_alter_table("participants_social_categories") as batch_op:
        batch_op.drop_constraint("fk_social_categories_id", type_="foreignkey")
        batch_op.drop_constraint("fk_participants_id", type_="foreignkey")
        batch_op.create_foreign_key(
            "fk_social_categories_id",
            "social_categories",
            ["social_category_id"],
            ["id"],
        )
        batch_op.create_foreign_key(
            "fk_participants_id", "participants", ["participant_id"], ["id"],
        )
    with op.batch_alter_table("events_social_categories") as batch_op:
        batch_op.drop_constraint("fk_events_id", type_="foreignkey")
        batch_op.drop_constraint("fk_social_categories_id", type_="foreignkey")
        batch_op.create_foreign_key("fk_events_id", "events", ["event_id"], ["id"])
        batch_op.create_foreign_key(
            "fk_social_categories_id",
            "social_categories",
            ["social_category_id"],
            ["id"],
        )
    with op.batch_alter_table("events") as batch_op:
        batch_op.alter_column("created_at", existing_type=sa.DATETIME(), nullable=True)
