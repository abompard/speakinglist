"""Add Event.created_at

Revision ID: 525e93caa396
Revises: f51d369d1305
Create Date: 2021-01-06 12:49:54.543035

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "525e93caa396"
down_revision = "f51d369d1305"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column("events", sa.Column("created_at", sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column("events", "created_at")
