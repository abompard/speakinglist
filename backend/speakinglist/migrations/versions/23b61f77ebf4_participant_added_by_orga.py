"""Participant.added_by_orga

Revision ID: 23b61f77ebf4
Revises: 112e01e14d36
Create Date: 2021-01-29 22:34:30.046913

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "23b61f77ebf4"
down_revision = "112e01e14d36"
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        "participants",
        sa.Column(
            "added_by_orga",
            sa.Boolean(),
            server_default=sa.text("(FALSE)"),
            nullable=False,
        ),
    )


def downgrade():
    op.drop_column("participants", "added_by_orga")
