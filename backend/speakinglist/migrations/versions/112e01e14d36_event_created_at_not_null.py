"""Event.created_at not null

Revision ID: 112e01e14d36
Revises: 525e93caa396
Create Date: 2021-01-08 00:14:35.431561

"""
import sqlalchemy as sa
from alembic import op


# revision identifiers, used by Alembic.
revision = "112e01e14d36"
down_revision = "525e93caa396"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column(
        "events",
        "created_at",
        existing_type=sa.DATETIME(),
        server_default=sa.text("(FALSE)"),
        nullable=False,
    )


def downgrade():
    op.alter_column("events", "created_at", existing_type=sa.DATETIME(), nullable=True)
