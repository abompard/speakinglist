from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import Response

from speakinglist.database import Session


class DBSessionMiddleware(BaseHTTPMiddleware):
    """Creates a database session and attaches it to
    `request.state.db` and auto commits and closes it at
    the end of the response.
    If there is an exception in processing the request,
    session will be rolled back unless explicitly committed
    inside the view.
    """

    async def dispatch(self, request, call_next):
        response = Response("Internal server error", status_code=500)
        async with Session() as db:
            request.state.db = db
            response = await call_next(request)
        return response
