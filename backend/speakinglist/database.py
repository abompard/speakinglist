from contextlib import asynccontextmanager
from functools import wraps

import databases
from alembic import command
from alembic.config import Config as AlembicConfig
from sqlalchemy import insert, MetaData
from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.future import select
from sqlalchemy.orm.exc import NoResultFound

from .settings import DATABASE_MAX_OVERFLOW, DATABASE_URL, DEFAULT_SOCIAL_CATEGORIES


convention = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}


def get_engine():
    db_url = str(DATABASE_URL)
    engine_args = {
        "pool_recycle": 3600 * 2,
    }

    if db_url.startswith("sqlite://") or db_url.startswith("sqlite+"):
        engine_args["connect_args"] = {"check_same_thread": False}
    else:
        engine_args["max_overflow"] = DATABASE_MAX_OVERFLOW

    # engine_args["echo"] = True
    engine = create_async_engine(db_url, **engine_args)
    return engine


metadata = MetaData(naming_convention=convention)
# Base = declarative_base(metadata=metadata)
# engine = get_engine()
# Session = async_scoped_session(sessionmaker(
#     engine, expire_on_commit=False, class_=AsyncSession
# ), scopefunc=current_task)
db = databases.Database(str(DATABASE_URL))  # , echo=True)


def run_alembic_command(alembic_command, *args, **kwargs):
    def _run(connection):
        alembic_cfg = AlembicConfig()
        alembic_cfg.set_main_option("script_location", "speakinglist:migrations")
        alembic_cfg.attributes["connection"] = connection
        return alembic_command(alembic_cfg, *args, **kwargs)

    return _run


async def init_db():
    from . import models  # noqa

    engine = get_engine()
    async with engine.begin() as conn:
        await conn.run_sync(metadata.create_all)
        await conn.run_sync(run_alembic_command(command.stamp, "head"))
    for name in DEFAULT_SOCIAL_CATEGORIES:
        query = insert(models.tables.social_categories_table).values(name=name)
        await db.execute(query)


# @asynccontextmanager
# async def db_transaction():
#     # async with AsyncSession(engine, expire_on_commit=False) as db:
#     async with Session() as db:
#         try:
#             async with db.transaction():
#                 yield db
#             await db.commit()
#         except Exception:
#             await db.rollback()
#             raise
#         # finally:
#         #     await db.close()

# def with_db_session(func):
#     @wraps(func)
#     async def wrapper(*args, **kwargs):
#         async_session = sessionmaker(
#             engine, expire_on_commit=False, class_=AsyncSession
#         )
#         async with async_session() as db:
#             async with db.transaction():
#                 try:
#                     try:
#                         info = kwargs["info"]
#                     except KeyError:
#                         info = args[1]
#                     info.context["request"].state.db = db
#                     async for result in func(*args, **kwargs):
#                         yield result
#                 except Exception:
#                     await db.rollback()
#                     raise
#                 finally:
#                     await db.close()
#
#     return wrapper


@asynccontextmanager
async def db_object(db, model, **filters):
    catch = filters.pop("catch", False)
    try:
        result = await db.execute(select(model).filter_by(**filters))
        yield result.scalar_one()
    except NoResultFound:
        print(f"Could not find {model} with {filters}")
        if catch:
            yield None
        else:
            raise


def db_connection(f):
    @wraps(f)
    async def wrapper(*args, **kwargs):
        await db.connect()
        result = await f(*args, **kwargs)
        await db.disconnect()
        return result

    return wrapper
