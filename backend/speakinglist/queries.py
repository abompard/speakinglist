from graphene import ID, Field, List, Mutation, ObjectType, String
from graphql import GraphQLError

from speakinglist.schemas.notification import Notification

from . import models
from .auth import get_user_from_context, require_orga, require_participant
from .database import db
from .push import (
    EventBroadcast,
    NotificationBroadcast,
    ParticipantBroadcast,
    ParticipantsBroadcast,
    TurnBroadcast,
    TurnsBroadcast,
)
from .schemas.event import Event
from .schemas.me import Me
from .schemas.participant import Participant
from .schemas.turn import Turn


class Query(ObjectType):
    event = Field(Event, code=ID(required=True))
    # me = Field(Me, event_code=String(required=True))
    download_token = String()

    async def resolve_event(self, info, code):
        return await info.context["loaders"]["event_by_code"].load(code)
        # return await models.Event.get_by_code(code)

    async def resolve_download_token(self, info):
        user = await get_user_from_context(info.context)
        if not user.is_authenticated:
            return None
        return user.get_download_token()


class CreateEvent(Mutation):
    class Arguments:
        title = String()

    event = Field(Event)
    me = Field(Me)

    async def mutate(root, info, title):
        current_user = await get_user_from_context(info.context)
        async with db.transaction():
            event = await models.Event.create(title=title)
            if not current_user.is_authenticated:
                current_user = await models.User.create()
            await current_user.add_organized_event(event.id)
        # Clear loaders cache
        info.context["loaders"]["organizers_of_event"].clear(event.id)
        me = await Me.from_user(info, current_user, event)
        return {"me": me, "event": event}


class Mutations(ObjectType):
    create_event = CreateEvent.Field()
    event = Field(Event, code=ID(required=True))

    async def resolve_event(self, info, code):
        return await info.context["loaders"]["event_by_code"].load(code)


class Subscription(ObjectType):
    participant = Field(Participant, id=ID(required=True))
    turns = List(Turn, event_code=ID(required=True))
    turn = Field(Turn, id=ID(required=True))
    event = Field(Event, code=ID(required=True))
    participants = List(Participant, event_code=ID(required=True))
    participants_last_seen = Field(Participant, event_code=ID(required=True))
    update_subscription_token = Field(String, token=String(required=True))
    notifications = Field(Notification, event_code=ID(required=True))

    async def subscribe_participant(root, info, id):
        async with db.transaction():
            participant = await info.context["loaders"]["participant"].load(id)
            if participant is None:
                return  # raise error?
            await require_participant(info, participant)
            broadcaster = ParticipantBroadcast(info.context, participant)
        print("Subscription to participant", id)
        async for participant, _msg in broadcaster.subscribe():
            print("Broadcasting Participant:", participant)
            yield participant

    async def subscribe_turn(root, info, id):
        async with db.transaction():
            turn = await info.context["loaders"]["turn"].load(id)
            if turn is None:
                return  # raise error?
            await require_participant(info, turn.participant)
            broadcaster = TurnBroadcast(info.context, turn)
        print("Subscription to turn", id)
        async for turn, _msg in broadcaster.subscribe():
            print("Broadcasting Turn:", turn)
            yield turn

    async def subscribe_event(root, info, code):
        event = await info.context["loaders"]["event_by_code"].load(code)
        if event is None:
            raise GraphQLError(f"Event {code} does not exist")
        broadcaster = EventBroadcast(info.context, event)
        print("Subscription to event", code)
        async for event, _msg in broadcaster.subscribe():
            print("Broadcasting Event:", event)
            yield event

    async def subscribe_participants(root, info, event_code):
        event = await info.context["loaders"]["event_by_code"].load(event_code)
        if event is None:
            raise GraphQLError(f"Event {event_code} does not exist")
        await require_orga(info, event)
        broadcaster = ParticipantsBroadcast(info.context, event)
        print("Subscription to participants of event:", event_code)
        async for event, _msg in broadcaster.subscribe():
            print("Broadcasting participants of event:", event, repr(_msg))
            yield await event.get_participants()

    async def subscribe_turns(root, info, event_code):
        event = await info.context["loaders"]["event_by_code"].load(event_code)
        if event is None:
            raise GraphQLError(f"Event {event_code} does not exist")
        await require_orga(info, event)
        broadcaster = TurnsBroadcast(info.context, event)
        print("Subscription to event turns", event_code)
        async for event, _msg in broadcaster.subscribe():
            print("Broadcasting turns of event:", event, repr(_msg))
            turns = await event.get_turns()
            print([t.id for t in turns])
            yield turns

    async def subscribe_update_subscription_token(root, info, token):
        print("Received update token request", token)
        info.context["request"].scope["connection_params"]["authToken"] = token
        yield token

    async def subscribe_notifications(root, info, event_code):
        event = await info.context["loaders"]["event_by_code"].load(event_code)
        if event is None:
            raise GraphQLError(f"Event {event_code} does not exist")
        await require_orga(info, event)
        broadcaster = NotificationBroadcast(info.context, event)
        print("Subscription to notifications of event", event_code)
        async for event, msg in broadcaster.subscribe():
            current_user = await get_user_from_context(info.context)
            print("Broadcasting notification:", repr(msg), "to", current_user)
            try:
                by_id = msg["by_id"]
                by_user = await models.User.get(by_id)
            except KeyError:
                print("Can't find the author:", msg)
                continue
            if by_user.id == current_user.id:
                # Only yield if the subscriber didn't do it.
                continue
            # Convert values from a dict to a list of dicts
            msg["values"] = [
                {"name": key, "value": value}
                for key, value in msg.get("values", {}).items()
            ]
            yield msg
