from speakinglist.database import db
from sqlalchemy.future import select
from sqlalchemy.orm.exc import NoResultFound
from starlette.authentication import (
    AuthCredentials,
    AuthenticationBackend,
    AuthenticationError,
    UnauthenticatedUser,
)

from .models import User
from .models.tables import users_table


def get_token_from_header(request):
    if "Authorization" not in request.headers:
        return
    auth = request.headers["Authorization"]
    if not auth:
        return
    return get_token_from_auth(auth)


def get_token_from_auth(authorization: str) -> str:
    """Parses the Authorization header and returns only the token"""
    try:
        scheme, token = authorization.split()
    except ValueError:
        raise AuthenticationError("Could not separate Authorization scheme and token")
    if scheme.lower() != "bearer":
        raise AuthenticationError("Authorization scheme Bearer is not supported")
    return token


def get_token_from_websocket(request):
    connection_params = request.scope.get("connection_params", {})
    if "authToken" not in connection_params:
        return
    return connection_params["authToken"]


async def get_auth_user_from_token(token):
    if token is None:
        return
    try:
        result = await db.fetch_one(
            select(users_table).where(users_table.c.token == token)
        )
    except NoResultFound:
        return
    if result is None:
        return
    return (AuthCredentials(["authenticated"]), User(**result))


class TokenBackend(AuthenticationBackend):
    async def authenticate(self, request):
        if request.scope["type"] == "websocket":
            # In websocket we can only get the auth token after connection_init,
            # which is later in the connection setup process.
            return
        token = get_token_from_header(request)
        return await get_auth_user_from_token(token)


async def get_user_from_context(context):
    request = context["request"]
    token = get_token_from_websocket(request)
    if token is not None:
        loader = context["loaders"]["user_by_token"]
        auth_user = await loader.load(token)
        if auth_user is not None:
            request.scope["user"] = auth_user
            return auth_user
    # this will always be the anonymous user because it is set by the starlette auth middleware
    return request.scope["user"]


async def is_orga_of(context, user_id, event_id):
    organizer_ids = [
        user.id
        for user in await context["loaders"]["organizers_of_event"].load(event_id)
    ]
    return user_id in organizer_ids


async def require_participant(info, participant):
    user = await get_user_from_context(info.context)
    if participant is None or not user.is_authenticated:
        raise AuthenticationError("NOT ALLOWED")
    is_orga = await is_orga_of(info.context, user.id, participant.event_id)
    if not (participant.user_id == user.id or is_orga):
        raise AuthenticationError("NOT ALLOWED")


async def require_orga(info, event):
    user = await get_user_from_context(info.context)
    if not user.is_authenticated:
        raise AuthenticationError("NOT ALLOWED")
    is_orga = await is_orga_of(info.context, user.id, event.id)
    if not is_orga:
        raise AuthenticationError("NOT ALLOWED")


async def user_from_jwt(request, type):
    token = request.query_params["token"]
    try:
        user = await User.from_jwt(token, type)
    except ValueError:
        user = None
    if user is None:
        user = UnauthenticatedUser()
    return user
