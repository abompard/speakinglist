from dataclasses import make_dataclass
from datetime import datetime

from sqlalchemy import Boolean, DateTime, delete, Integer, select, String, update

from speakinglist.database import db


def now():
    return datetime.now().replace(microsecond=0)


@classmethod
async def _get(cls, obj_id=None, **kwargs):
    if obj_id is None and not kwargs or obj_id is not None and kwargs:
        raise ValueError("get(): either use obj_id or keywords")
    if not kwargs:
        kwargs["id"] = obj_id
    where_clause = []
    for key, value in kwargs.items():
        where_clause.append(getattr(cls._table.c, key) == value)
    query = select(cls._table).where(*where_clause)
    result = await db.fetch_one(query=query)
    if result is None:
        return None
    return cls(**result)


async def _update(obj, values):
    query_values = {}
    for col, value in values.items():
        if getattr(obj, col) == value:
            # Already set
            continue
        setattr(obj, col, value)
        query_values[col] = value
    if not query_values:
        # Nothing to update
        return
    async with db.transaction():
        return await db.execute(
            update(obj._table).where(obj._table.c.id == obj.id).values(**query_values)
        )


async def _delete(obj):
    async with db.transaction():
        await db.execute(delete(obj._table).where(obj._table.c.id == obj.id))


def model_from_table(name, table):
    fields = []
    for column in table.columns:
        if isinstance(column.type, String):
            field_type = str
        elif isinstance(column.type, Integer):
            field_type = int
        elif isinstance(column.type, Boolean):
            field_type = bool
        elif isinstance(column.type, DateTime):
            field_type = datetime
        else:
            raise ValueError(f"Unknown column type: {column.type!r}")
        fields.append((column.name, field_type))
    return make_dataclass(
        name,
        fields,
        namespace={
            "_table": table,
            "_update": _update,
            "_delete": _delete,
            "get": _get,
        },
    )
