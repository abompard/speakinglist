from sqlalchemy import func, insert, select

from speakinglist.database import db

from .tables import turns_table
from .utils import model_from_table, now


TurnBase = model_from_table("Turn", turns_table)


class Turn(TurnBase):
    @classmethod
    async def request_for(cls, participant):
        last_position = await db.fetch_val(
            select(func.max(cls._table.c.position)).where(
                cls._table.c.event_id == participant.event_id
            )
        )
        if not last_position:
            last_position = 0
        query = insert(cls._table).values(
            requested_at=now(),
            event_id=participant.event_id,
            participant_id=participant.id,
            position=last_position + 1,
        )
        async with db.transaction():
            result = await db.execute(query)
        return await cls.get(result)

    @classmethod
    async def of_participant(cls, participant_id):
        query = select(cls._table).where(cls._table.c.participant_id == participant_id)
        return [Turn(**result) for result in await db.fetch_all(query)]

    async def start(self):
        await self._update({"started_at": now()})

    async def stop(self):
        await self._update({"ended_at": now()})

    @property
    def is_pending(self):
        return self.started_at is None and self.ended_at is None

    @property
    def is_current(self):
        return self.started_at is not None and self.ended_at is None

    @property
    def is_past(self):
        return self.started_at is not None and self.ended_at is not None

    @property
    def duration(self):
        if self.ended_at is None or self.started_at is None:
            return None
        return int((self.ended_at - self.started_at).total_seconds())
