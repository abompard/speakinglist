from sqlalchemy import (
    Boolean,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    String,
    Table,
    text,
)

from speakinglist.database import metadata


events_table = Table(
    "events",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("title", String(254)),
    Column("code", String(16), unique=True, index=True, nullable=False),
    Column("admin_code", String(16), nullable=False),
    Column("created_at", DateTime, nullable=False),
    Column("started_at", DateTime),
    Column("ended_at", DateTime),
    Column("unpaused_at", DateTime),
    Column(
        "is_paused",
        Boolean,
        nullable=False,
        default=False,
        server_default=text("FALSE"),
    ),
    Column("duration", Integer, nullable=False, default=0),  # in minutes
    Column("turn_max_duration", Integer, nullable=False, default=150),  # in seconds
)

organizers_table = Table(
    "organizers",
    metadata,
    Column(
        "user_id", Integer, ForeignKey("users.id", ondelete="CASCADE"), nullable=False
    ),
    Column(
        "event_id", Integer, ForeignKey("events.id", ondelete="CASCADE"), nullable=False
    ),
)

participants_table = Table(
    "participants",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("name", String(254)),
    Column("is_active", Boolean, default=True, server_default=text("TRUE"), index=True),
    Column("created", DateTime, nullable=False),
    Column("last_seen", DateTime, nullable=False),
    Column(
        "added_by_orga",
        Boolean,
        nullable=False,
        default=False,
        server_default=text("FALSE"),
        index=True,
    ),
    Column("user_id", Integer, ForeignKey("users.id", ondelete="CASCADE")),
    Column(
        "event_id", Integer, ForeignKey("events.id", ondelete="CASCADE"), nullable=False
    ),
)

social_categories_table = Table(
    "social_categories",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("name", String(254), nullable=False, index=True),
)

participants_social_categories_table = Table(
    "participants_social_categories",
    metadata,
    Column(
        "participant_id",
        Integer,
        ForeignKey("participants.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
    Column(
        "social_category_id",
        Integer,
        ForeignKey("social_categories.id", ondelete="CASCADE"),
        primary_key=True,
        nullable=False,
    ),
    Column("value", String(254)),
)

events_social_categories_table = Table(
    "events_social_categories",
    metadata,
    Column(
        "event_id", Integer, ForeignKey("events.id", ondelete="CASCADE"), nullable=False
    ),
    Column(
        "social_category_id",
        Integer,
        ForeignKey("social_categories.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

turns_table = Table(
    "turns",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("position", Integer, nullable=False, index=True),
    Column("requested_at", DateTime, nullable=False),
    Column("started_at", DateTime, index=True),
    Column("ended_at", DateTime, index=True),
    Column(
        "event_id", Integer, ForeignKey("events.id", ondelete="CASCADE"), nullable=False
    ),
    Column(
        "participant_id",
        Integer,
        ForeignKey("participants.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

users_table = Table(
    "users",
    metadata,
    Column("id", Integer, primary_key=True, index=True),
    Column("created", DateTime, nullable=False),
    Column("token", String(254), unique=True, index=True),
)
