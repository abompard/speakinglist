from datetime import datetime

from sqlalchemy import delete, func, insert, select
from sqlalchemy.exc import IntegrityError

from speakinglist.database import db
from speakinglist.settings import DEFAULT_SOCIAL_CATEGORIES
from speakinglist.utils import get_random_string

from .participant import Participant
from .social_category import SocialCategory
from .tables import (
    events_social_categories_table,
    events_table,
    participants_table,
    social_categories_table,
    turns_table,
)
from .turn import Turn
from .utils import model_from_table, now


EventBase = model_from_table("Event", events_table)


class Event(EventBase):
    @classmethod
    async def get_by_code(cls, event_code):
        query = select(cls._table).where(cls._table.c.code == event_code)
        event = await db.fetch_one(query)
        return cls(**event)

    @classmethod
    async def create(cls, title=None):
        admin_code = get_random_string(length=6)
        while True:
            event_code = get_random_string()
            query = insert(events_table).values(
                code=event_code,
                admin_code=admin_code,
                title=title,
                created_at=datetime.now(),
                # Column defaults are currently ignored
                # https://github.com/encode/databases/issues/72
                is_paused=False,
                duration=0,
                turn_max_duration=150,
            )
            transaction = await db.transaction()
            try:
                await db.execute(query)
                await transaction.commit()
            except IntegrityError:
                # Duplicate event code
                await transaction.rollback()
                continue
            else:
                break
        event = await cls.get_by_code(event_code)
        print("Created event", event_code)
        await event.set_social_categories(DEFAULT_SOCIAL_CATEGORIES)
        return event

    @property
    def _get_turns_query(self):
        return (
            select(turns_table)
            .where(turns_table.c.event_id == self.id)
            .order_by(turns_table.c.started_at, turns_table.c.position)
        )

    async def get_waiting_list(self):
        return [
            Turn(**result)
            for result in await db.fetch_all(
                self._get_turns_query.where(turns_table.c.ended_at.is_(None))
            )
        ]

    async def get_past_turns(self):
        return [
            Turn(**result)
            for result in await db.fetch_all(
                self._get_turns_query.where(
                    turns_table.c.started_at.isnot(None),
                    turns_table.c.ended_at.isnot(None),
                )
            )
        ]

    async def get_turns(self):
        return [Turn(**result) for result in await db.fetch_all(self._get_turns_query)]

    async def get_participants(self):
        return [
            Participant(**result)
            for result in await db.fetch_all(
                select(participants_table)
                .where(participants_table.c.event_id == self.id)
                .order_by(participants_table.c.name)
            )
        ]

    async def get_participants_count(self):
        return await db.fetch_val(
            select(func.count())
            .select_from(participants_table)
            .where(participants_table.c.event_id == self.id)
        )

    async def get_current_turn(self):
        result = await db.fetch_one(
            self._get_turns_query.where(
                turns_table.c.started_at.isnot(None),
                turns_table.c.ended_at.is_(None),
            )
        )
        return Turn(**result) if result is not None else None

    async def start(self):
        await self._update({"started_at": now()})

    async def stop(self):
        await self._update({"ended_at": now()})
        await self._update_duration()

    async def pause(self):
        await self._update({"is_paused": True})
        await self._update_duration()

    async def unpause(self):
        await self._update({"is_paused": False, "unpaused_at": now()})

    async def restart(self):
        await self._update({"ended_at": None, "unpaused_at": now()})

    async def _update_duration(self):
        if self.unpaused_at is not None:
            last_start = self.unpaused_at
        else:
            last_start = self.started_at
        this_run_duration = now() - last_start
        await self._update(
            {"duration": self.duration + this_run_duration.total_seconds()}
        )

    @property
    def accepts_turn_requests(self):
        return (
            self.started_at is not None and self.ended_at is None and not self.is_paused
        )

    async def get_social_categories(self):
        query = (
            select(social_categories_table)
            .join(events_social_categories_table)
            .where(events_social_categories_table.c.event_id == self.id)
            .order_by(social_categories_table.c.name)
        )
        return [SocialCategory(**result) for result in await db.fetch_all(query)]

    async def set_social_categories(self, social_categories):
        # Clean existing
        await db.execute(
            delete(events_social_categories_table).where(
                events_social_categories_table.c.event_id == self.id
            )
        )
        # Set the new ones
        query = select(social_categories_table.c.id).where(
            social_categories_table.c.name.in_(social_categories)
        )
        for result in await db.fetch_all(query):
            await db.execute(
                insert(events_social_categories_table).values(
                    event_id=self.id, social_category_id=result.id
                )
            )

    async def move_turn(self, from_position, to_position):
        waiting_list = [
            turn
            for turn in await self.get_waiting_list()
            if turn.is_pending  # can't reorder the current turn
        ]
        # print("before move:", [(t.participant.name, t.position) for t in waiting_list])
        turn = waiting_list.pop(from_position)
        waiting_list.insert(to_position, turn)
        for index, turn in enumerate(waiting_list):
            await turn._update({"position": index})
        return waiting_list
        # print("after move:", [(t.participant.name, t.position) for t in waiting_list])
