from datetime import timedelta
from secrets import token_urlsafe

from jose import JWTError, jwt
from speakinglist.database import db
from speakinglist.settings import SECRET_KEY, TOKEN_ALGORITHM
from sqlalchemy import delete, insert, select
from starlette.authentication import BaseUser as StarletteBaseUser

from .tables import organizers_table, participants_table, users_table
from .utils import model_from_table, now

UserBase = model_from_table("User", users_table)


class User(UserBase, StarletteBaseUser):
    @classmethod
    async def create(cls):
        token = token_urlsafe()
        # TODO: check that the token does not exist already
        query = insert(cls._table).values(
            created=now(),
            token=token,
        )
        async with db.transaction():
            result = await db.execute(query)
        return await cls.get(result)

    @property
    def is_authenticated(self) -> bool:
        return True

    async def is_orga_for(self, event_id):
        return event_id in [e.id for e in await self.get_organized_events()]

    async def get_participants_in(self, event_id):
        from .participant import Participant

        query = select(participants_table).where(
            participants_table.c.user_id == self.id,
            participants_table.c.event_id == event_id,
        )
        return [Participant(**result) for result in await db.fetch_all(query)]

    async def get_organized_events(self):
        query = select(organizers_table).where(
            organizers_table.c.user_id == self.id,
        )
        from .event import Event

        events = []
        for result in await db.fetch_all(query):
            events.append(await Event.get(result.event_id))
        return events

    async def add_organized_event(self, event_id):
        query = insert(organizers_table).values(user_id=self.id, event_id=event_id)
        async with db.transaction():
            await db.execute(query)

    async def remove_organized_event(self, event_id):
        query = delete(organizers_table).where(
            organizers_table.c.user_id == self.id,
            organizers_table.c.event_id == event_id,
        )
        async with db.transaction():
            await db.execute(query)

    def get_download_token(self):
        to_encode = {"sub": str(self.id), "aud": "DL"}
        expire = now() + timedelta(minutes=2)
        to_encode.update({"exp": int(expire.timestamp())})
        return jwt.encode(to_encode, str(SECRET_KEY), algorithm=TOKEN_ALGORITHM)

    @classmethod
    async def from_jwt(cls, token, type):
        try:
            payload = jwt.decode(
                token, key=str(SECRET_KEY), algorithms=TOKEN_ALGORITHM, audience=type
            )
        except JWTError as e:
            print("Invalid token:", e)
            raise ValueError(e)
            # raise AuthenticationError(str(e))
        return await cls.get(int(payload["sub"]))
