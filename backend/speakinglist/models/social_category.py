from dataclasses import dataclass

from sqlalchemy import insert, select, update

from speakinglist.database import db

from .tables import (
    events_social_categories_table,
    participants_social_categories_table,
    social_categories_table,
)
from .utils import model_from_table


SocialCategoryBase = model_from_table("SocialCategory", social_categories_table)


class SocialCategory(SocialCategoryBase):
    @classmethod
    async def available_in_event(cls, event_id):
        query = (
            select(cls._table)
            .join(events_social_categories_table)
            .where(events_social_categories_table.c.event_id == event_id)
        )
        return [cls(**result) for result in await db.fetch_all(query)]


ParticipantSocialCategoryBase = model_from_table(
    "ParticipantSocialCategory", participants_social_categories_table
)


@dataclass
class ParticipantSocialCategory(ParticipantSocialCategoryBase):

    name: str

    @classmethod
    async def get_all(cls, participant_id):
        query = (
            select(cls._table, social_categories_table.c.name)
            .join(social_categories_table)
            .where(
                cls._table.c.participant_id == participant_id,
            )
        )
        return [cls(**result) for result in await db.fetch_all(query)]

    @classmethod
    async def get_one(cls, participant_id, name):
        query = (
            select(cls._table)
            .join(social_categories_table)
            .where(
                cls._table.c.participant_id == participant_id,
                social_categories_table.c.name == name,
            )
        )
        result = await db.fetch_one(query)
        if result is None:
            return None
        result = dict(result)
        result["name"] = name
        return cls(**result)

    @classmethod
    async def create(cls, participant_id, social_category_id, value):
        sc = await SocialCategory.get(social_category_id)
        query = insert(cls._table).values(
            participant_id=participant_id,
            social_category_id=social_category_id,
            value=value,
        )
        async with db.transaction():
            await db.execute(query)
        return cls(
            participant_id=participant_id,
            social_category_id=social_category_id,
            name=sc.name,
            value=value,
        )

    async def _update(self, values):
        query_values = {}
        for col, value in values.items():
            if getattr(self, col) == value:
                # Already set
                continue
            setattr(self, col, value)
            query_values[col] = value
        if not query_values:
            # Nothing to update
            return
        return await db.execute(
            update(self._table)
            .where(
                self._table.c.participant_id == self.participant_id,
                self._table.c.social_category_id == self.social_category_id,
            )
            .values(**query_values)
        )
