from sqlalchemy import insert, select

from speakinglist.database import db

from .social_category import ParticipantSocialCategory, SocialCategory
from .tables import participants_table, turns_table
from .turn import Turn
from .user import User
from .utils import model_from_table, now


ParticipantBase = model_from_table("Participant", participants_table)


class Participant(ParticipantBase):
    @classmethod
    async def create(
        cls, name, event, user, social_categories=None, added_by_orga=False
    ):
        now_ = now()
        query = insert(cls._table).values(
            name=name,
            added_by_orga=added_by_orga,
            created=now_,
            last_seen=now_,
            event_id=event.id,
            user_id=user.id if user else None,
            # Column defaults are currently ignored
            # https://github.com/encode/databases/issues/72
            is_active=True,
        )
        async with db.transaction():
            result = await db.execute(query)
        participant = await cls.get(result)
        social_categories = social_categories or []
        if social_categories:
            await participant.set_social_categories(social_categories)
        return participant

    @property
    def _turns_query(self):
        return select(turns_table).where(
            turns_table.c.participant_id == self.id,
        )

    async def get_turn(self, turn_id):
        result = await db.fetch_one(
            self._turns_query.where(turns_table.c.id == turn_id)
        )
        return Turn(**result) if result is not None else None

    async def get_pending_turn(self):
        result = await db.fetch_one(
            self._turns_query.where(
                turns_table.c.started_at.is_(None),
                turns_table.c.ended_at.is_(None),
            )
        )
        return Turn(**result) if result is not None else None

    async def get_current_turn(self):
        result = await db.fetch_one(
            self._turns_query.where(
                turns_table.c.started_at.isnot(None),
                turns_table.c.ended_at.is_(None),
            )
        )
        return Turn(**result) if result is not None else None

    async def get_previous_turns(self):
        result = await db.fetch_all(
            select(turns_table).where(
                turns_table.c.participant_id == self.id,
                turns_table.c.started_at.isnot(None),
                turns_table.c.ended_at.isnot(None),
            )
        )
        return [Turn(**t) for t in result]

    async def get_social_categories(self):
        return await ParticipantSocialCategory.get_all(self.id)
        # query = (
        #     select(
        #         social_categories_table, participants_social_categories_table.c.value
        #     )
        #     .join(participants_social_categories_table)
        #     .where(
        #         participants_social_categories_table.c.participant_id == self.id,
        #     )
        # )
        # result = [SocialCategory(**result) for result in await db.fetch_all(query)]
        # print("result of get_social_categories:", result)
        # return result

    async def set_social_categories(self, social_categories):
        available_sc = {
            sc.name: sc for sc in await SocialCategory.available_in_event(self.event_id)
        }
        for social_category in social_categories:
            name = social_category.name
            value = social_category.value
            existing = await ParticipantSocialCategory.get_one(self.id, name)
            if existing:
                await existing._update({"value": value})
                continue
            category = available_sc[name]
            await ParticipantSocialCategory.create(
                participant_id=self.id, social_category_id=category.id, value=value
            )

    async def is_orga(self):
        if self.user_id is None:
            return False
        user = await User.get(self.user_id)
        return await user.is_orga_for(self.event_id)

    async def set_orga(self, make_orga: bool):
        if self.user_id is None:
            user = await User.create()
            await self._update({"user_id": user.id})
        else:
            user = await User.get(self.user_id)
        is_orga = await self.is_orga()
        if make_orga and not is_orga:
            await user.add_organized_event(self.event_id)
        elif not make_orga and is_orga:
            await user.remove_organized_event(self.event_id)
