from dotenv import load_dotenv
from starlette.config import Config
from starlette.datastructures import URL, Secret

TOKEN_ALGORITHM = "HS256"


def to_list(value):
    return [elem.strip() for elem in value.split(",") if elem.strip()]


load_dotenv()

config = Config(".env.backend")

DEBUG = config("DEBUG", cast=bool, default=False)
TESTING = config("TESTING", cast=bool, default=False)

DATABASE_URL = config("DATABASE_URL", cast=URL)

if TESTING:
    DATABASE_URL = DATABASE_URL.replace(database="test_" + DATABASE_URL.database)

DATABASE_MAX_OVERFLOW = config("DATABASE_MAX_OVERFLOW", cast=int, default=-1)

BROADCAST_BACKEND = config("BROADCAST_BACKEND", cast=URL)

# to get a string suitable for the secret key, run: openssl rand -hex 32
SECRET_KEY = config("SECRET_KEY", cast=Secret)

ACCESS_TOKEN_EXPIRE_MINUTES = config(
    "ACCESS_TOKEN_EXPIRE_MINUTES", cast=int, default=60 * 6
)


ALLOW_HOSTS = config(
    "ALLOW_HOSTS",
    cast=to_list,
    default="http://localhost,http://localhost:3000",
)

DEFAULT_SOCIAL_CATEGORIES = config(
    "DEFAULT_SOCIAL_CATEGORIES", cast=to_list, default="gender,race"
)

EVENT_STOP_AFTER_DAYS = config("EVENT_STOP_AFTER_DAYS", cast=int, default=7)
EVENT_EXPIRATION_DAYS = config("EVENT_EXPIRATION_DAYS", cast=int, default=30)
