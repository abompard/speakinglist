from graphene import Int, List, ObjectType, String


class NotificationValue(ObjectType):
    name = String()
    value = String()


class Notification(ObjectType):
    msgid = String()
    values = List(NotificationValue)
    by_id = Int()
