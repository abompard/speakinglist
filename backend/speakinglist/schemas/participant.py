from graphene import ID, Boolean, DateTime, Field, List, Mutation, ObjectType, String
from graphql import GraphQLError

from .. import models
from ..auth import get_user_from_context, is_orga_of, require_orga
from ..database import db
from .base import ModelMixin

# from .event import Event
from .social_category import SocialCategory, SocialCategoryInput
from .turn import Turn


def _clear_turn_cache(context, turn):
    context["loaders"]["turn"].clear(turn.id).prime(turn.id, turn)
    context["loaders"]["turns_of_participant"].clear(turn.participant_id)
    context["loaders"]["turns_in_event"].clear(turn.event_id)


def _clear_participant_cache(context, participant, prime=True):
    # Clear/prime cache
    context["loaders"]["participant"].clear(participant.id)
    if prime:
        context["loaders"]["participant"].prime(participant.id, participant)
    context["loaders"]["participants_of_user"].clear(
        (participant.user_id, participant.event_id)
    )
    context["loaders"]["participants_in_event"].clear(participant.event_id)


class ParticipantArguments:
    name = String(required=True)
    social_categories = List(SocialCategoryInput)
    me = Boolean()


class UpdateParticipant(Mutation):
    Arguments = ParticipantArguments

    @staticmethod
    def Output():
        return Participant

    async def mutate(participant, info, name, social_categories=None, me=None):
        social_categories = social_categories or []
        async with db.transaction():
            await participant._update({"name": name})
            await participant.set_social_categories(social_categories)
            user = await get_user_from_context(info.context)
            is_orga = await is_orga_of(info.context, user.id, participant.event_id)
            if user.is_authenticated and is_orga:
                if me:
                    await participant._update({"user_id": user.id})
                elif me is False:
                    await participant._update({"user_id": None})
                # If me is None, it's not been specified, don't change the user.
            # Clear/prime cache
            _clear_participant_cache(info.context, participant)
            info.context["loaders"]["social_categories_of_participant"].clear(
                participant.id
            )
        from ..push import ParticipantBroadcast

        await ParticipantBroadcast(info.context, participant).publish()
        print(f"Participant {participant.id} was updated")
        return participant


class RequestTurn(Mutation):
    turn = Field(Turn)

    async def mutate(participant, info):
        if await participant.get_pending_turn() is not None:
            raise GraphQLError("EXISTING_PENDING_TURN")

        event = await info.context["loaders"]["event"].load(participant.event_id)
        if not event.accepts_turn_requests:
            raise GraphQLError("EVENT_NOT_RUNNING")
        async with db.transaction():
            turn = await models.Turn.request_for(participant)

        # Clear/prime cache
        _clear_turn_cache(info.context, turn)

        # Boardcasts
        from ..push import NotificationBroadcast, TurnBroadcast

        await TurnBroadcast(info.context, turn).publish()
        await NotificationBroadcast(info.context, event).publish(
            {
                "msgid": "TURN_REQUESTED",
                "values": {
                    "participant_name": participant.name,
                    "participant_id": participant.id,
                    "id": turn.id,
                },
            }
        )
        print(f"Participant {participant.id} requested a turn: {turn.id}")
        return {"turn": turn}


class SetActive(Mutation):
    class Arguments:
        is_active = Boolean(required=True)

    @staticmethod
    def Output():
        return Participant

    async def mutate(participant, info, is_active):
        from ..push import ParticipantBroadcast, TurnBroadcast

        async with db.transaction():
            await participant._update({"is_active": is_active})
        # Clear/prime cache
        _clear_participant_cache(info.context, participant)

        if not is_active:
            # Remove current turn and pending turn
            turns_to_stop = [
                await participant.get_current_turn(),
                await participant.get_pending_turn(),
            ]
            for turn in turns_to_stop:
                if turn is None:
                    continue
                async with db.transaction():
                    await turn.stop()

                _clear_turn_cache(info.context, turn)
                await TurnBroadcast(info.context, turn).publish()
        await ParticipantBroadcast(info.context, participant).publish()
        return participant


class SetOrga(Mutation):
    class Arguments:
        is_orga = Boolean(required=True)

    @staticmethod
    def Output():
        return Participant

    async def mutate(participant, info, is_orga):
        event = await info.context["loaders"]["event"].load(participant.event_id)
        await require_orga(info, event)
        current_user = await get_user_from_context(info.context)
        if participant.user_id == current_user.id:
            raise GraphQLError("You can't change your own status")
        async with db.transaction():
            await participant.set_orga(is_orga)

        # Clear loaders caches
        info.context["loaders"]["organizers_of_event"].clear(participant.event_id)

        # Broadcasts
        from ..push import ParticipantBroadcast

        await ParticipantBroadcast(info.context, participant).publish()
        return participant


class Delete(Mutation):
    Output = Boolean()

    async def mutate(participant, info):
        event = await info.context["loaders"]["event"].load(participant.event_id)
        await require_orga(info, event)
        participant_id = participant.id
        participant_name = participant.name
        if await participant.get_pending_turn() is not None:
            raise GraphQLError("This participant has a pending turn, delete it first")
        async with db.transaction():
            await participant._delete()

        # Clear loaders caches
        _clear_participant_cache(info.context, participant, prime=False)

        from ..push import (
            NotificationBroadcast,
            ParticipantBroadcast,
            ParticipantsBroadcast,
        )

        await ParticipantBroadcast(info.context, None, participant_id).publish()
        await ParticipantsBroadcast(info.context, event).publish()
        await NotificationBroadcast(info.context, event).publish(
            {
                "msgid": "PARTICIPANT_DELETED",
                "values": {"name": participant_name, "id": participant_id},
            }
        )
        print(f"Participant {participant_id} in event {event.id} was deleted")
        return True


class Participant(ObjectType, ModelMixin):
    id = ID()
    name = String(required=True)
    last_seen = DateTime()
    is_active = Boolean()
    added_by_orga = Boolean()

    turns = List(Turn)
    social_categories = List(SocialCategory)
    is_orga = Boolean()
    turn = Field(Turn, id=ID())
    # Mutations
    request_turn = RequestTurn.Field()
    update = UpdateParticipant.Field()
    set_active = SetActive.Field()
    set_orga = SetOrga.Field()
    delete = Delete.Field()

    async def resolve_turns(participant, info):
        loader = info.context["loaders"]["turns_of_participant"]
        return await loader.load(participant.id)

    async def resolve_social_categories(participant, info):
        loader = info.context["loaders"]["social_categories_of_participant"]
        return await loader.load(participant.id)

    async def resolve_is_orga(participant, info):
        return await is_orga_of(info.context, participant.user_id, participant.event_id)

    async def resolve_turn(participant, info, id):
        turn = await info.context["loaders"]["turn"].load(int(id))
        if turn is None:
            raise GraphQLError(f"Turn {id} not found")
        return turn
        turn = await info.context["loaders"]["turn"].load(int(id))
        if turn is None:
            raise GraphQLError(f"Turn {id} not found")
        return turn
