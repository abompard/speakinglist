import datetime

from graphene import (
    ID,
    Boolean,
    DateTime,
    Field,
    InputObjectType,
    Int,
    List,
    Mutation,
    ObjectType,
    String,
)
from graphql import GraphQLError

from .. import models
from ..auth import get_user_from_context, require_orga, require_participant
from ..database import db
from ..settings import EVENT_EXPIRATION_DAYS
from .base import ModelMixin
from .me import Me
from .participant import Participant, ParticipantArguments
from .social_category import SocialCategory
from .turn import Turn


async def _create_participant(
    info, name, event, user, social_categories, added_by_orga
):
    participant = await models.Participant.create(
        name=name,
        event=event,
        user=user,
        social_categories=social_categories,
        added_by_orga=added_by_orga,
    )
    # Publish the change to the participants list
    from ..push import NotificationBroadcast, ParticipantBroadcast

    p_broadcaster = ParticipantBroadcast(info.context, participant)
    await p_broadcaster.publish()

    n_broadcaster = NotificationBroadcast(info.context, event)
    await n_broadcaster.publish(
        {"msgid": "PARTICIPANT_ADDED", "values": {"name": name, "id": participant.id}}
    )

    return participant


def _clear_event_cache(context, event, prime=True):
    context["loaders"]["event"].clear(event.id)
    context["loaders"]["event_by_code"].clear(event.code)
    if prime:
        context["loaders"]["event"].prime(event.id, event)
        context["loaders"]["event_by_code"].prime(event.code, event)


class RegisterParticipant(Mutation):
    Arguments = ParticipantArguments

    participant = Field(Participant)
    me = Field(Me)

    async def mutate(event, info, name, social_categories):
        current_user = await get_user_from_context(info.context)
        if not current_user.is_authenticated:
            current_user = await models.User.create()
            info.context["request"].user = current_user

        participant = await _create_participant(
            info,
            name=name,
            event=event,
            user=current_user,
            social_categories=social_categories,
            added_by_orga=False,
        )
        print(
            f"Registered participant {participant.id} ({name}) by user {current_user.id}"
        )
        me = await Me.from_user(info, current_user, event)
        return {
            "me": me,
            "participant": participant,
        }


class MoveTurn(Mutation):
    class Arguments:
        from_position = Int(required=True)
        to_position = Int(required=True)

    Output = List(Turn)

    async def mutate(event, info, from_position, to_position):
        await require_orga(info, event)
        async with db.transaction():
            waiting_list = await event.move_turn(from_position, to_position)

        # Clear loaders cache
        info.context["loaders"]["turns_in_event"].clear(event.id)
        for turn in waiting_list:
            info.context["loaders"]["turn"].clear(turn.id).prime(turn.id, turn)
            info.context["loaders"]["turns_of_participant"].clear(turn.participant_id)

        # Broadcasts
        from ..push import NotificationBroadcast, TurnsBroadcast

        turns_broadcaster = TurnsBroadcast(info.context, event)
        await turns_broadcaster.publish()

        n_broadcaster = NotificationBroadcast(info.context, event)
        await n_broadcaster.publish(
            {
                "msgid": "WAITING_LIST_MOVED",
                "values": {"from_position": from_position, "to_position": to_position},
            }
        )

        waiting_list = await event.get_waiting_list()
        return waiting_list


class SetState(Mutation):
    class Arguments:
        action = String(required=True)

    @staticmethod
    def Output():
        return Event

    async def mutate(event, info, action):
        await require_orga(info, event)
        if action not in ("start", "stop", "pause", "unpause", "restart"):
            raise GraphQLError(f"Invalid action: {action}")
        action_method = getattr(event, action)  # Hmmm seems a bit too magic
        async with db.transaction():
            await action_method()

        # Clear loaders cache
        _clear_event_cache(info.context, event)

        # Broadcasts
        from ..push import EventBroadcast, NotificationBroadcast

        e_broadcaster = EventBroadcast(info.context, event)
        await e_broadcaster.publish()

        n_broadcaster = NotificationBroadcast(info.context, event)
        await n_broadcaster.publish(
            {"msgid": "EVENT_STATE_CHANGED", "values": {"action": action}}
        )
        print(f"Event {event.id} changed state: {action}")
        return event


class CheckOrga(Mutation):
    class Arguments:
        admin_code = String(required=True)

    me = Field(Me)

    async def mutate(event, info, admin_code):
        current_user = await get_user_from_context(info.context)
        if admin_code == event.admin_code:
            async with db.transaction():
                if not current_user.is_authenticated:
                    current_user = await models.User.create()
                await current_user.add_organized_event(event.id)
            # Clear loaders cache
            info.context["loaders"]["organizers_of_event"].clear(event.id)
        me = await Me.from_user(info, current_user, event)
        return {"me": me}


class EventInput(InputObjectType):
    title = String()
    social_categories = List(String)
    turn_max_duration = Int()


class UpdateEvent(Mutation):
    class Arguments:
        properties = EventInput(required=True)

    @staticmethod
    def Output():
        return Event

    async def mutate(event, info, properties):
        await require_orga(info, event)
        async with db.transaction():
            try:
                social_categories = properties.pop("social_categories")
            except KeyError:
                pass
            else:
                await event.set_social_categories(social_categories)
                # Clear loaders cache
                info.context["loaders"]["social_categories_in_event"].clear(event.id)
            await event._update(properties)
            # Clear loaders cache
            _clear_event_cache(info.context, event)

        from ..push import EventBroadcast, NotificationBroadcast

        broadcaster = EventBroadcast(info.context, event)
        await broadcaster.publish()

        n_broadcaster = NotificationBroadcast(info.context, event)
        await n_broadcaster.publish({"msgid": "EVENT_PROPS_CHANGED"})

        return event


class AddParticipant(Mutation):
    Arguments = ParticipantArguments

    Output = Participant

    async def mutate(event, info, name, social_categories, me):
        await require_orga(info, event)
        current_user = await get_user_from_context(info.context)
        participant = await _create_participant(
            info,
            name=name,
            event=event,
            user=current_user if me else None,
            social_categories=social_categories,
            added_by_orga=not me,
        )
        print(
            f"Added participant {participant.id} by orga {current_user.id} (for self: {me})"
        )
        return participant


class DeleteEvent(Mutation):
    Output = Boolean

    async def mutate(event, info):
        await require_orga(info, event)
        if event.started_at and not event.ended_at:
            raise GraphQLError("EVENT_STILL_RUNNING")
        event_id = event.id
        await event._delete()
        # Clear loaders cache
        _clear_event_cache(info.context, event, prime=False)

        from ..push import EventBroadcast, NotificationBroadcast

        broadcaster = EventBroadcast(info.context, None, obj_id=event_id)
        await broadcaster.publish()

        n_broadcaster = NotificationBroadcast(info.context, event)
        await n_broadcaster.publish({"msgid": "EVENT_DELETED"})

        return True


class Event(ObjectType, ModelMixin):
    title = String()
    code = ID(required=True)
    started_at = DateTime()
    ended_at = DateTime()
    expires_at = DateTime()
    duration = Int()
    is_paused = Boolean()
    accepts_turn_requests = Boolean()
    turn_max_duration = Int()
    admin_code = String()

    current_participants = List(Participant)
    participant = Field(Participant, id=ID(required=True))
    participants = List(Participant)
    social_categories = List(SocialCategory)
    turns = List(Turn)
    me = Field(Me)
    check_in = List(Participant)

    # Mutations
    register_participant = RegisterParticipant.Field()
    move_turn = MoveTurn.Field()
    set_state = SetState.Field()
    check_orga = CheckOrga.Field()
    update = UpdateEvent.Field()
    add_participant = AddParticipant.Field()
    delete = DeleteEvent.Field()

    def resolve_expires_at(event, info):
        if event.ended_at is None:
            return None
        return event.ended_at + datetime.timedelta(days=EVENT_EXPIRATION_DAYS)

    async def resolve_current_participants(event, info):
        user = await get_user_from_context(info.context)
        if not user.is_authenticated:
            return []
        participants = await info.context["loaders"]["participants_of_user"].load(
            (user.id, event.id)
        )
        for participant in participants:
            await require_participant(info, participant)
        return participants

    async def resolve_participant(event, info, id):
        participant = await info.context["loaders"]["participant"].load(int(id))
        if participant is None:
            return None
        assert participant.event_id == event.id
        await require_participant(info, participant)
        return participant

    async def resolve_participants(event, info):
        await require_orga(info, event)
        return await info.context["loaders"]["participants_in_event"].load(event.id)

    async def resolve_admin_code(event, info):
        await require_orga(info, event)
        return event.admin_code

    async def resolve_social_categories(event, info):
        return await info.context["loaders"]["social_categories_in_event"].load(
            event.id
        )

    async def resolve_turns(event, info):
        await require_orga(info, event)
        return await info.context["loaders"]["turns_in_event"].load(event.id)

    async def resolve_me(event, info):
        user = await get_user_from_context(info.context)
        return await Me.from_user(info, user, event)

    async def resolve_check_in(event, info):
        user = await get_user_from_context(info.context)
        async with db.transaction():
            participants = (
                await info.context["loaders"]["participants_of_user"].load(
                    (user.id, event.id)
                )
                if user.is_authenticated
                else []
            )
            for participant in participants:
                await participant._update({"last_seen": datetime.datetime.now()})
                # Clear loaders cache
                info.context["loaders"]["participant"].clear(participant.id).prime(
                    participant.id, participant
                )
        info.context["loaders"]["participants_of_user"].clear((user.id, event.id))
        info.context["loaders"]["participants_in_event"].clear(event.id)
        return participants
