import json
from asyncio import iscoroutine

from ..utils import DateTimeAwareEncoder


class ModelMixin:
    @classmethod
    async def model_to_dict(cls, obj):
        # Don't use dataclasses.asdict() because it won't resolve model methods
        result = {}
        for field_name, schema_field in cls._meta.fields.items():
            # Only if the field exists on the model
            try:
                field_value = getattr(obj, field_name)
            except AttributeError:
                continue
            resolver = getattr(schema_field, "resolver", None)
            if resolver is not None and resolver.__name__ == "mutate":
                # This is a mutation, skip it
                continue
            # Call model methods
            if callable(field_value):
                if iscoroutine(field_value):
                    field_value = await field_value()
                else:
                    field_value = await field_value()
            result[field_name] = field_value
        return result

    @classmethod
    async def from_model(cls, obj):
        values = await cls.model_to_dict(obj)
        return cls.from_dict(values)

    @classmethod
    def from_dict(cls, props):
        for propname in list(props):
            if propname not in cls._meta.fields:
                del props[propname]
        return cls(**props)

    @classmethod
    def from_json(cls, data):
        props = json.loads(data)
        return cls.from_dict(props)

    @classmethod
    async def to_json(cls, obj):
        return json.dumps(await cls.model_to_dict(obj), cls=DateTimeAwareEncoder)
