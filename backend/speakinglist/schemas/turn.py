from graphene import ID, DateTime, Field, Int, Mutation, ObjectType
from graphql import GraphQLError

from ..auth import get_user_from_context, require_orga
from ..database import db
from .base import ModelMixin


def _clear_turn_caches(context, turn):
    context["loaders"]["turn"].clear(turn.id).prime(turn.id, turn)
    context["loaders"]["turns_of_participant"].clear(turn.participant_id)
    context["loaders"]["turns_in_event"].clear(turn.event_id)


class CancelTurn(Mutation):
    turn = Field(lambda: Turn)

    async def mutate(turn, info):
        if not turn.is_pending:
            # This can happen if a participant does not receive the notif that their
            # turn has ended, and they try to cancel it.
            current_user = await get_user_from_context(info.context)

            print(
                f"Turn {turn.id} for participant {turn.participant_id} was cancelled "
                f"by {current_user.id}, but it was not pending."
            )
            return {"turn": turn}
        async with db.transaction():
            await turn.stop()

        # Clear caches
        _clear_turn_caches(info.context, turn)

        # Broadcasts
        from ..push import NotificationBroadcast, TurnBroadcast

        await TurnBroadcast(info.context, turn).publish()
        event = await info.context["loaders"]["event"].load(turn.event_id)
        participant = await info.context["loaders"]["participant"].load(
            turn.participant_id
        )
        await NotificationBroadcast(info.context, event).publish(
            {
                "msgid": "TURN_CANCELLED",
                "values": {
                    "participant_name": participant.name,
                    "participant_id": participant.id,
                    "id": turn.id,
                },
            }
        )
        print(f"Turn {turn.id} for participant {turn.participant_id} was cancelled")
        return {"turn": turn}


class StartTurn(Mutation):
    turn = Field(lambda: Turn)

    async def mutate(turn, info):
        event = await info.context["loaders"]["event"].load(turn.event_id)
        await require_orga(info, event)
        if not turn.is_pending:
            raise GraphQLError(f"Turn {turn.id} is not pending")

        participant = await info.context["loaders"]["participant"].load(
            turn.participant_id
        )

        from ..push import NotificationBroadcast, TurnBroadcast

        # First, stop current turn if there was one.
        current_turn = await event.get_current_turn()
        if current_turn is not None:
            async with db.transaction():
                await current_turn.stop()
            _clear_turn_caches(info.context, current_turn)
            await TurnBroadcast(info.context, current_turn).publish()
            await NotificationBroadcast(info.context, event).publish(
                {
                    "msgid": "TURN_STOPPED",
                    "values": {
                        "participant_name": participant.name,
                        "participant_id": participant.id,
                        "id": current_turn.id,
                    },
                }
            )
        # Now start the new turn
        async with db.transaction():
            await turn.start()
        # Clear/Prime caches
        _clear_turn_caches(info.context, turn)
        # Broadcasts
        await TurnBroadcast(info.context, turn).publish()
        await NotificationBroadcast(info.context, event).publish(
            {
                "msgid": "TURN_STARTED",
                "values": {
                    "participant_name": participant.name,
                    "participant_id": participant.id,
                    "id": turn.id,
                },
            }
        )
        # Query return
        print(f"Turn {turn.id} for participant {turn.participant_id} has started")
        return {"turn": turn}


class EndTurn(Mutation):
    turn = Field(lambda: Turn)

    async def mutate(turn, info):
        if not turn.is_current:
            print(f"Turn {turn.id} was ended but it's not the current turn, ignoring.")
            return {"turn": turn}
        async with db.transaction():
            await turn.stop()

        # Clear/Prime caches
        _clear_turn_caches(info.context, turn)

        # Broadcasts
        from ..push import NotificationBroadcast, TurnBroadcast

        await TurnBroadcast(info.context, turn).publish()
        # event = await models.Event.get(turn.event_id)
        event = await info.context["loaders"]["event"].load(turn.event_id)
        # participant = await models.Participant.get(turn.participant_id)
        participant = await info.context["loaders"]["participant"].load(
            turn.participant_id
        )
        await NotificationBroadcast(info.context, event).publish(
            {
                "msgid": "TURN_STOPPED",
                "values": {
                    "participant_name": participant.name,
                    "participant_id": participant.id,
                    "id": turn.id,
                },
            }
        )
        print(f"Turn {turn.id} for participant {turn.participant_id} has ended")
        return {"turn": turn}


class Turn(ObjectType, ModelMixin):
    id = ID()
    position = Int()
    started_at = DateTime()
    ended_at = DateTime()
    requested_at = DateTime()
    duration = Int()
    participant_id = Int()

    # Mutations
    cancel = CancelTurn.Field()
    start = StartTurn.Field()
    end = EndTurn.Field()
