from graphene import Boolean, List, ObjectType, String

from .. import models
from ..auth import is_orga_of
from .participant import Participant


class Me(ObjectType):
    participants = List(Participant)
    organizer = Boolean()
    token = String()

    @classmethod
    async def from_user(cls, info, user, event):
        if not user.is_authenticated:
            # Create user
            user = await models.User.create()
            return cls(participants=[], organizer=False, token=user.token)

        participants_loader = info.context["loaders"]["participants_of_user"]
        participants = await participants_loader.load((user.id, event.id))

        return cls(
            participants=participants,
            organizer=await is_orga_of(info.context, user.id, event.id),
            token=user.token,
        )
