from graphene import ID, InputObjectType, ObjectType, String

from .base import ModelMixin


class SocialCategoryInput(InputObjectType):
    name = String()
    value = String()


class SocialCategory(ObjectType, ModelMixin):
    id = ID()
    name = String(required=True)
    value = String()

    # @classmethod
    # async def from_model(cls, obj):
    #     sc = await models.SocialCategory.get(obj.social_category_id)
    #     return cls(
    #         id=f"P:{obj.participant_id}:{obj.social_category_id}",
    #         name=sc.name,
    #         value=obj.value,
    #     )

    def resolve_id(sc, info):
        if hasattr(sc, "participant_id"):
            return f"P:{sc.participant_id}:{sc.social_category_id}"
        else:
            return sc.id

    # @classmethod
    # async def from_psc_model(cls, obj):
    #     return cls(
    #         id=f"P:{obj.participant_id}:{obj.social_category_id}",
    #         name=obj.name,
    #         value=obj.value,
    #     )
