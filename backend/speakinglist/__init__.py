# from datetime import datetime, timedelta
# from typing import Optional
#
# import graphene
# from fastapi import Depends, FastAPI, HTTPException, status
# from jose import jwt, JWTError
# from pydantic import BaseModel
# from starlette.graphql import GraphQLApp
#
#
# # to get a string like this run:
# # openssl rand -hex 32
# SECRET_KEY = "2db0b1b2606bfb2da92c38acb1bab406298e064c97e3fa06076292cc0316004c"
# ALGORITHM = "HS256"
# ACCESS_TOKEN_EXPIRE_MINUTES = 60 * 24
#
#
# app = FastAPI()
# # oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
#
#
# def create_access_token(data: dict, expires_minutes: Optional[int] = 60):
#     to_encode = data.copy()
#     expire = datetime.utcnow() + timedelta(minutes=expires_minutes)
#     to_encode.update({"exp": expire})
#     encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
#     return encoded_jwt
#
#
# @app.get("/")
# def read_root():
#     return {"Hello": "World"}


# @app.post("/token", response_model=Token)
# async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
#     user = authenticate_user(fake_users_db, form_data.username, form_data.password)
#     if not user:
#         raise HTTPException(
#             status_code=status.HTTP_401_UNAUTHORIZED,
#             detail="Incorrect username or password",
#             headers={"WWW-Authenticate": "Bearer"},
#         )
#     access_token = create_access_token(
#         data={"sub": user.username}, expires_minutes=ACCESS_TOKEN_EXPIRE_MINUTES
#     )
#     return {"access_token": access_token, "token_type": "bearer"}


# @app.get("/users/me")
# async def read_users_me(current_user: User = Depends(get_current_active_user)):
#     return current_user
#
#
# @app.get("/users/me/items/")
# async def read_own_items(current_user: User = Depends(get_current_active_user)):
#     return [{"item_id": "Foo", "owner": current_user.username}]
