from aiodataloader import DataLoader
from sqlalchemy import select

from speakinglist.database import db
from speakinglist.models.user import User

from ..models.tables import (
    organizers_table,
    users_table,
)
from .base import ModelLoader, ModelListLoader


class UserLoader(ModelLoader):
    model = User


class UserByTokenLoader(DataLoader):
    async def batch_load_fn(self, keys):
        query = select(users_table).where(users_table.c.token.in_(keys))
        results = {result.token: User(**result) for result in await db.fetch_all(query)}
        return [results.get(k) for k in keys]


class OrganizersOfEventLoader(ModelListLoader):
    model = User
    column_name = "event_id"

    def get_query(self, keys):
        return (
            select(users_table, organizers_table.c.event_id)
            .join(organizers_table)
            .where(
                organizers_table.c.event_id.in_(keys),
            )
        )
