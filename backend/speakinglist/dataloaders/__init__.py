from .event import EventByCodeLoader, EventLoader
from .participant import (
    ParticipantLoader,
    ParticipantsInEventLoader,
    ParticipantsOfUserLoader,
)
from .social_category import (
    SocialCategoriesInEventLoader,
    SocialCategoriesOfParticipantLoader,
    SocialCategoryLoader,
)
from .turn import TurnLoader, TurnsInEventLoader, TurnsOfParticipantLoader
from .user import OrganizersOfEventLoader, UserByTokenLoader, UserLoader


def create_loaders(request=None):
    cache = request is None or request.scope["type"] != "websocket"
    return {
        "event": EventLoader(cache=cache),
        "event_by_code": EventByCodeLoader(cache=cache),
        "participant": ParticipantLoader(cache=cache),
        "participants_of_user": ParticipantsOfUserLoader(cache=cache),
        "participants_in_event": ParticipantsInEventLoader(cache=cache),
        "turn": TurnLoader(cache=cache),
        "turns_in_event": TurnsInEventLoader(cache=cache),
        "turns_of_participant": TurnsOfParticipantLoader(cache=cache),
        "social_category": SocialCategoryLoader(cache=cache),
        "social_categories_in_event": SocialCategoriesInEventLoader(cache=cache),
        "social_categories_of_participant": SocialCategoriesOfParticipantLoader(
            cache=cache
        ),
        "user": UserLoader(cache=cache),
        "user_by_token": UserByTokenLoader(cache=cache),
        "organizers_of_event": OrganizersOfEventLoader(cache=cache),
    }
