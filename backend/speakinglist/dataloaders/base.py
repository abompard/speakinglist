import dataclasses
from collections import defaultdict

from aiodataloader import DataLoader
from speakinglist.database import db
from sqlalchemy import select


class ModelLoader(DataLoader):
    model = None

    async def batch_load_fn(self, keys):
        assert self.model is not None
        table = self.model._table
        query = select(table).where(table.c.id.in_(keys))
        objects = {
            result.id: self.model(**result) for result in await db.fetch_all(query)
        }
        return [objects.get(obj_id) for obj_id in keys]


class ModelListLoader(ModelLoader):
    column_name = None

    def get_query(self, keys):
        return None

    def get_result_key(self, row):
        assert self.column_name is not None
        return getattr(row, self.column_name)

    async def batch_load_fn(self, keys):
        query = self.get_query(keys)
        assert query is not None
        results = defaultdict(list)
        for row in await db.fetch_all(query):
            result_key = self.get_result_key(row)
            model_data = dict(row)
            model_field_names = [f.name for f in dataclasses.fields(self.model)]
            if (
                self.column_name is not None
                and self.column_name not in model_field_names
            ):
                del model_data[self.column_name]
            results[result_key].append(self.model(**model_data))
        return [results.get(k, []) for k in keys]
