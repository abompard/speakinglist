# from promise import Promise
# from promise.dataloader import DataLoader
from aiodataloader import DataLoader
from sqlalchemy import select

from speakinglist.database import db
from speakinglist.models.event import Event

from .base import ModelLoader


class EventLoader(ModelLoader):
    model = Event


class EventByCodeLoader(DataLoader):
    async def batch_load_fn(self, keys):
        query = select(Event._table).where(Event._table.c.code.in_(keys))
        events = {result.code: Event(**result) for result in await db.fetch_all(query)}
        return [events.get(event_code) for event_code in keys]
