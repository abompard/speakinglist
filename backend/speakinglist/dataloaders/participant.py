from sqlalchemy import select

from speakinglist.models.participant import Participant

from .base import ModelLoader, ModelListLoader


class ParticipantLoader(ModelLoader):
    model = Participant


class ParticipantsOfUserLoader(ModelListLoader):
    model = Participant

    def get_result_key(self, row):
        return (row.user_id, row.event_id)

    def get_query(self, keys):
        return select(Participant._table).where(
            Participant._table.c.user_id.in_(k[0] for k in keys),
            Participant._table.c.event_id.in_(k[1] for k in keys),
        )


class ParticipantsInEventLoader(ModelListLoader):
    model = Participant
    column_name = "event_id"

    def get_query(self, keys):
        return (
            select(Participant._table)
            .where(Participant._table.c.event_id.in_(keys))
            .order_by(Participant._table.c.name)
        )
