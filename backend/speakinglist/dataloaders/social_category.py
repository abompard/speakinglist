from sqlalchemy import select

from speakinglist.models.social_category import (
    ParticipantSocialCategory,
    SocialCategory,
)

from ..models.tables import (
    events_social_categories_table,
    participants_social_categories_table,
)
from .base import ModelListLoader, ModelLoader


class SocialCategoryLoader(ModelLoader):
    model = SocialCategory


class SocialCategoriesInEventLoader(ModelListLoader):
    model = SocialCategory
    column_name = "event_id"

    def get_query(self, keys):
        return (
            select(SocialCategory._table, events_social_categories_table.c.event_id)
            .join(events_social_categories_table)
            .where(events_social_categories_table.c.event_id.in_(keys))
            .order_by(SocialCategory._table.c.name)
        )


class SocialCategoriesOfParticipantLoader(ModelListLoader):
    model = ParticipantSocialCategory
    column_name = "participant_id"

    def get_query(self, keys):
        return (
            select(participants_social_categories_table, SocialCategory._table.c.name)
            .join(SocialCategory._table)
            .where(participants_social_categories_table.c.participant_id.in_(keys))
            .order_by(SocialCategory._table.c.name)
        )
