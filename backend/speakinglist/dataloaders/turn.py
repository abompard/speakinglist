from sqlalchemy import select

from speakinglist.models.turn import Turn

from .base import ModelListLoader, ModelLoader


class TurnLoader(ModelLoader):
    model = Turn


class TurnsInEventLoader(ModelListLoader):
    model = Turn
    column_name = "event_id"

    def get_query(self, keys):
        return (
            select(Turn._table)
            .where(Turn._table.c.event_id.in_(keys))
            .order_by(Turn._table.c.started_at, Turn._table.c.position)
        )


class TurnsOfParticipantLoader(ModelListLoader):
    model = Turn
    column_name = "participant_id"

    def get_query(self, keys):
        return (
            select(Turn._table)
            .where(Turn._table.c.participant_id.in_(keys))
            .order_by(Turn._table.c.started_at, Turn._table.c.position)
        )
