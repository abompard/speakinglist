import graphene
from starlette.applications import Starlette
from starlette.background import BackgroundTasks
from starlette.middleware import Middleware
from starlette.middleware.authentication import AuthenticationMiddleware
from starlette.middleware.cors import CORSMiddleware

# from starlette.routing import Route
from starlette_graphene3 import GraphQLApp

from . import settings
from .auth import TokenBackend
from .csvstats import stats_view

# from .middleware.database import DBSessionMiddleware
from .database import db
from .push import broadcast
from .dataloaders import create_loaders
from .queries import Mutations, Query, Subscription


# from .middleware.graphql_auth import GraphQLAuthMiddleware


middleware = [
    Middleware(
        CORSMiddleware,
        allow_origins=settings.ALLOW_HOSTS,
        allow_methods=["*"],
        allow_headers=["*"],
        allow_credentials=True,
    ),
    # Middleware(DBSessionMiddleware),
    Middleware(AuthenticationMiddleware, backend=TokenBackend()),
]

app = Starlette(
    debug=settings.DEBUG,
    middleware=middleware,
    on_startup=[db.connect, broadcast.connect],
    on_shutdown=[db.disconnect, broadcast.disconnect],
)


def context_value(request):
    # Fix compatibility issue between starlette and starlette_graphene3
    context = {"request": request, "background": BackgroundTasks()}
    context["loaders"] = create_loaders(request)
    return context


schema = graphene.Schema(query=Query, mutation=Mutations, subscription=Subscription)
graphql_app = GraphQLApp(schema, context_value=context_value)
app.mount("/graphql", graphql_app)
app.mount("/ws", graphql_app)

app.add_route("/event/{event_code}/stats/{filename}.csv", stats_view)
