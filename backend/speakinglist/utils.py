import datetime
import functools
import json
import random
import string


class DateTimeAwareEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.replace(microsecond=0).isoformat()
        return super().default(obj)


def get_random_string(length: int = 4):
    pool = list(string.ascii_uppercase + string.digits)
    # Remove confusing elements
    for element in ["0", "O", "1", "I"]:
        pool.remove(element)
    return "".join(random.choice(pool) for i in range(length))
