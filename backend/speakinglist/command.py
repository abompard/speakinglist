import asyncio
import csv
from dataclasses import dataclass
from datetime import datetime, timedelta
from functools import wraps

import click
from sqlalchemy import insert, select
from tabulate import tabulate

from .database import db, db_connection
from .database import init_db as do_init_db
from .models import Event, Participant, Turn
from .settings import EVENT_EXPIRATION_DAYS, EVENT_STOP_AFTER_DAYS


def coroutine(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        return asyncio.run(f(*args, **kwargs))

    return wrapper


@click.group()
def cli():
    pass


@cli.command()
@coroutine
async def init_db():
    await do_init_db()
    click.echo("Database initialized.")


@cli.group()
def events():
    pass


@events.command("list")
@coroutine
@db_connection
async def event_list():
    events = await db.fetch_all(
        select(Event._table).order_by(Event._table.c.created_at)
    )
    if len(events) == 0:
        click.echo("No events.")
        return
    headers = ["Code", "Title", "Admin", "Start", "End", "Participants"]
    table = []
    for event in events:
        event = Event(**event)
        table.append(
            [
                event.code,
                event.title,
                event.admin_code,
                event.started_at or "not started",
                event.ended_at or "not ended",
                await event.get_participants_count(),
            ]
        )
    click.echo(tabulate(table, headers=headers))


@events.command("import")
@click.argument("event-code")
@click.argument("exported-file", type=click.File("r"))
@coroutine
@db_connection
async def event_import(event_code, exported_file):
    event_code = event_code.upper()
    event = await Event.get_by_code(event_code)
    if event is None:
        click.echo(f"The event {event_code} does not exist, please create it first.")
        return

    @dataclass
    class _SocialCategory:
        name: str
        value: str

    reader = csv.reader(exported_file)
    # ignore the first line
    next(reader)
    for turn_position, row in enumerate(reader):
        participant_name = f"Participant {row[0]}"
        isorga = True if row[5] == "TRUE" else False
        categories = row[7:]
        # Get or insert the participant
        participant = await db.fetch_one(
            select(Participant._table).filter_by(
                event_id=event.id, name=participant_name
            )
        )
        event_social_categories = await event.get_social_categories()
        if participant is None:
            print("Creating", participant_name)
            async with db.transaction():
                result = await db.execute(
                    insert(Participant._table).values(
                        event_id=event.id,
                        name=participant_name,
                        added_by_orga=True,
                        created=datetime.now(),
                        last_seen=datetime.now(),
                    )
                )
                participant = await Participant.get(result)
                await participant.set_orga(isorga)
                social_categories = []
                for category_index, category_value in enumerate(categories):
                    try:
                        social_category = event_social_categories[category_index]
                    except IndexError:
                        continue
                    social_categories.append(
                        _SocialCategory(name=social_category.name, value=category_value)
                    )
                await participant.set_social_categories(social_categories)
        else:
            participant = Participant(**participant)

        if row[1]:
            # There's a turn to add, it's not just a participant
            timestamp = datetime.fromtimestamp(int(row[1]))
            duration = int(row[2])
            requested_at = datetime.fromtimestamp(int(row[3]))
            async with db.transaction():
                await db.execute(
                    insert(Turn._table).values(
                        event_id=event.id,
                        participant_id=participant.id,
                        position=turn_position,
                        started_at=timestamp,
                        ended_at=timestamp + timedelta(seconds=duration),
                        requested_at=requested_at,
                    )
                )


@events.command("expire")
@click.option("-n", "--dry-run", is_flag=True, help="Only show what would be deleted.")
@coroutine
@db_connection
async def event_expire(dry_run):
    # Stop events that have been running for too long
    overtime_threshold = datetime.now() - timedelta(days=EVENT_STOP_AFTER_DAYS)
    overtime_events = await db.fetch_all(
        select(Event._table)
        .filter(
            Event._table.c.ended_at.is_(None),
            Event._table.c.started_at < overtime_threshold,
        )
        .order_by(Event._table.c.created_at)
    )
    for event in overtime_events:
        event = Event(**event)
        if dry_run:
            print(
                f'Would stop event "{event.title}" ({event.code}) that has been '
                f"running since {event.started_at}"
            )
        else:
            print(
                f'Stopping event "{event.title}" ({event.code}) that has been '
                f"running since {event.started_at}"
            )
            async with db.transaction():
                await event.stop()
    # Delete expired events
    expired_threshold = datetime.now() - timedelta(days=EVENT_EXPIRATION_DAYS)
    expired_events = await db.fetch_all(
        select(Event._table)
        .filter(
            Event._table.c.ended_at.isnot(None),
            Event._table.c.ended_at < expired_threshold,
        )
        .order_by(Event._table.c.created_at)
    )
    for event in expired_events:
        event = Event(**event)
        if dry_run:
            print(
                f'Would delete expired event "{event.title}" ({event.code}) '
                f"that ended on {event.ended_at}"
            )
        else:
            print(
                f'Deleting expired event "{event.title}" ({event.code}) that ended '
                f"on {event.ended_at}"
            )
            async with db.transaction():
                await event._delete()
    # Delete that never started
    stillborn_events = await db.fetch_all(
        select(Event._table)
        .filter(
            Event._table.c.started_at.is_(None),
            Event._table.c.created_at < expired_threshold,
        )
        .order_by(Event._table.c.created_at)
    )
    for event in stillborn_events:
        event = Event(**event)
        if dry_run:
            print(
                f'Would delete event "{event.title}" ({event.code}) '
                f"created on {event.created_at} that never started"
            )
        else:
            print(
                f'Deleting event "{event.title}" ({event.code}) created '
                f"on {event.created_at} that never started"
            )
            async with db.transaction():
                await event._delete()
