import json

from broadcaster import Broadcast
from starlette.background import BackgroundTasks

from . import models
from .auth import get_user_from_context
from .schemas.event import Event
from .schemas.participant import Participant
from .schemas.turn import Turn
from .settings import BROADCAST_BACKEND

broadcast = Broadcast(str(BROADCAST_BACKEND))


class Broadcaster:
    # def __init__(self, request):
    def __init__(self, tasks=None):
        self.broadcast = broadcast
        # if "background" not in request.context:
        #     request.context["background"] = BackgroundTasks()
        # self.background = request.context["background"]
        self.background = tasks

    def _get_object_broadcaster(self, obj):
        if isinstance(obj, models.Event):
            return EventBroadcast(self, obj)
        elif isinstance(obj, models.Participant):
            return ParticipantBroadcast(self, obj)
        elif isinstance(obj, models.Turn):
            return TurnBroadcast(self, obj)
        else:
            raise RuntimeError(f"Unsupported object: {obj!r}")

    def register_task(self, channel, message=""):
        if self.background is None:
            raise RuntimeError("No background tasks object, can't register")
        self.background.add_task(self.broadcast.publish, channel, message)

    async def publish(self, model_obj):
        broadcaster = self._get_object_broadcaster(model_obj)
        await broadcaster.publish()

    async def subscribe(self, model_obj):
        broadcaster = self._get_object_broadcaster(model_obj)
        async for event in broadcaster.subscribe():
            yield event


#     def register_tasks(self, broadcasts):
#         for channel, message in broadcasts:
#             self.register_task(channel, message)

# def publish_event_change(self, event):
#     self._register_tasks([self._event_msg(event)])
#
# def publish_participant_change(self, participant):
#     broadcasts = [
#         self._participant_msg(participant),
#         self._participants_msg(participant.event),
#     ]
#     self._register_tasks(broadcasts)
#
# def publish_turn_change(self, turn):
#     broadcasts = [
#         self._turn_msg(turn),
#         self._turns_msg(turn.event),
#         self._waitinglist_msg(turn.event),
#     ]
#     self._register_tasks(broadcasts)
#     self.publish_participant_change(turn.participant)
#
# def publish_waitinglist_change(self, event):
#     broadcasts = [
#         self._waitinglist_msg(event),
#     ]
#     self._register_tasks(broadcasts)
#
# def publish_last_seen(self, participant):
#     self._register_tasks([self._last_seen_msg(participant)])

#     def _event_msg(self, event):
#         from .schemas.event import Event
#
#         return (f"Event:{event.code}", Event.to_json(event))
#
#     def _turn_msg(self, turn):
#         from .schemas.turn import Turn
#
#         return (f"Turn:{turn.id}", Turn.to_json(turn))
#
#     def _turns_msg(self, event):
#         return (f"Event:{event.code}:Turns", "")
#
#     def _waitinglist_msg(self, event):
#         return (f"Event:{event.code}:WaitingList", "")
#
#     def _participants_msg(self, participant):
#         from .schemas.participant import Participant
#
#         return (
#             f"Event:{participant.event.code}:Participants",
#             Participant.to_json(participant),
#         )
#
#     def _participant_msg(self, participant):
#         from .schemas.participant import Participant
#
#         return (f"Participant:{participant.id}", Participant.to_json(participant))
#
#     def _last_seen_msg(self, participant):
#         message = json.dumps(
#             dict(id=participant.id, last_seen=participant.last_seen),
#             cls=DateTimeAwareEncoder,
#         )
#         return (f"Event:{participant.event.code}:ParticipantLastSeen", message)
#
#     def _object_msg(self, obj, schema):
#         typename = schema._meta.name
#         obj_id = getattr(obj, "id")
#         msg = schema.to_json(obj)
#         channel = f"{typename}:{obj_id}"
#         print("publishing update", typename, obj, channel, msg)
#         return (channel, msg)


class ObjectBroadcast:
    serializer = None
    model = None

    def __init__(self, context, obj, obj_id=None):
        self.broadcast = broadcast
        self.context = context
        self.obj = obj
        self.obj_id = obj_id or obj.id

    @property
    def type_name(self):
        if self.serializer is None:
            return None
        return self.serializer._meta.name

    @property
    def background(self):
        if "background" not in self.context:
            self.context["background"] = BackgroundTasks()
        return self.context["background"]

    def _register_task(self, channel, message=""):
        self.background.add_task(self.broadcast.publish, channel, message)

    def _get_channel(self):
        # print("Channel for", self.__class__.__name__, f"{self.type_name}:{self.obj_id}")
        return f"{self.type_name}:{self.obj_id}"

    @property
    def channel(self):
        return self._get_channel()

    async def get_msg(self, content):
        if self.serializer is None:
            return json.dumps(content)
        if not self.obj:
            return ""  # deletion
        return await self.serializer.to_json(self.obj)

    async def publish(self, content=""):
        self.background.add_task(
            self.broadcast.publish, self.channel, await self.get_msg(content)
        )

    async def subscribe(self):
        async with self.broadcast.subscribe(channel=self.channel) as subscriber:
            async for event in subscriber:
                data = json.loads(event.message) if event.message else None

                obj = await self.model.get(self.obj_id)
                if not obj:
                    print(f"Could not find {self.model} {self.obj_id}, can't subscribe")
                yield obj, data


class EventBroadcast(ObjectBroadcast):
    serializer = Event
    model = models.Event


class ParticipantBroadcast(ObjectBroadcast):
    serializer = Participant
    model = models.Participant

    async def publish(self):
        await super().publish()
        if self.obj:
            # event = await models.Event.get(self.obj.event_id)
            event = await self.context["loaders"]["event"].load(self.obj.event_id)
            await ParticipantsBroadcast(self.context, event).publish()


class TurnBroadcast(ObjectBroadcast):
    serializer = Turn
    model = models.Turn

    async def publish(self):
        await super().publish()
        # event = await models.Event.get(self.obj.event_id)
        event = await self.context["loaders"]["event"].load(self.obj.event_id)
        participant = await models.Participant.get(self.obj.participant_id)
        print("Publishing to Turns")
        await TurnsBroadcast(self.context, event).publish()
        print("Publishing to Participant")
        await ParticipantBroadcast(self.context, participant).publish()


class ParticipantsBroadcast(ObjectBroadcast):
    model = models.Event

    def _get_channel(self):
        return f"Event:{self.obj_id}:Participants"

    # async def get_msg(self, content=None):
    #     return await super().get_msg(
    #         [await Participant.to_json(p) for p in await self.obj.get_participants()]
    #     )


class TurnsBroadcast(ObjectBroadcast):
    """This works as a Waiting List broadcaster as well"""
    model = models.Event

    def _get_channel(self):
        return f"Event:{self.obj_id}:Turns"

    # async def get_msg(self, content=None):
    #     return await super().get_msg(
    #         [await Turn.to_json(t) for t in await self.obj.get_turns()]
    #     )


class NotificationBroadcast(ObjectBroadcast):
    model = models.Event

    def _get_channel(self):
        return f"Event:{self.obj_id}:Notifications"

    async def publish(self, content=None):
        content = content or {}
        current_user = await get_user_from_context(self.context)
        content["by_id"] = current_user.id
        print("Publishing to Notification")
        return await super().publish(content)


# async def publish_object_changes(obj, schema):
#     typename = schema._meta.name
#     obj_id = getattr(obj, "id")
#     msg = schema.to_json(obj)
#     channel = f"{typename}:{obj_id}"
#     print("publishing update", typename, obj, channel, msg)
#     await broadcast.publish(channel, msg)
#     if typename == "Turn":
#         await publish_turns_change(obj.event)
#         await publish_waitinglist_change(obj.event)
#         # The participants list uses `has_pending_turn` which may need to be updated
#         await publish_participants_change(obj.participant)
#         # Publish a change on the corresponding participant
#         await publish_participant_change(obj.participant)
#     elif typename == "Participant":
#         await publish_participants_change(obj)
#
#
# async def publish_turns_change(event):
#     await broadcast.publish(f"Event:{event.code}:Turns", "")
#
#
# async def publish_waitinglist_change(event):
#     await broadcast.publish(f"WaitingList:{event.code}", "")
#
#
# async def publish_participants_change(participant):
#     from .schemas.participant import Participant
#
#     message = Participant.to_json(participant)
#     await broadcast.publish(f"Participants:{participant.event.code}", message)
#
#
# async def publish_participant_change(participant):
#     from .schemas.participant import Participant
#
#     message = Participant.to_json(participant)
#     await broadcast.publish(f"Participant:{participant.id}", message)
#
#
# async def push_last_seen(participant):
#     # message = Participant.to_json(id=participant.id, last_seen=participant.last_seen)
#     message = json.dumps(
#         dict(id=participant.id, last_seen=participant.last_seen),
#         cls=DateTimeAwareEncoder,
#     )
#     await broadcast.publish(
#         f"Event:{participant.event.code}:ParticipantLastSeen", message
#     )
#
