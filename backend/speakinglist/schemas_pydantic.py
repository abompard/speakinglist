from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel


class ParticipantBase(BaseModel):
    full_name: str


class ParticipantCreate(ParticipantBase):
    pass


class Participant(ParticipantBase):
    id: int
    is_active: Optional[bool] = True

    class Config:
        orm_mode = True


class EventBase(BaseModel):
    title: str
    code: str
    start_at: datetime


class EventCreate(EventBase):
    pass


class Event(EventBase):
    id: int
    is_active: Optional[bool] = False
    participants: List[Participant] = []

    class Config:
        orm_mode = True
