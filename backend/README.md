# Speaking List (backend)

This is the backend component of Speaking List.

Install with `poetry install`
If you want to use Redis as the message queue, install with `poetry install -E redis`

Run with `poetry run uvicorn speakinglist.main:app`
