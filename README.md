# Speaking List

Manage speaking lists for your online events!

When online video calls reach a certain number of participants, it becomes harder and harder to listen to one another and distribute speaking turns fairly.

### Organize

Speaking List helps you organize speaking turns in your online events by letting participants ask for a turn, and be notified in real time when their turn to speak has arrived. As an event organizer, you can manage the waiting list, reorder it, start and end turns, etc.

### Be fairer

People in marginalized groups often get less opportunities to speak. The application will allow participants to declare which social categories they belong to, and organizers will be able to take that information into account when ordering the waiting list. They will also get statistics of speaking opportunities and durations by category.

## Installation

On the production server you need:

- Uvicorn or another ASGI-compatible webserver
- A regular webserver capable of serving static files (such as NginX or Apache HTTPd)
- Redis

### Frontend

To build the software, you need NodeJS and NPM. First, configure the build by editing the values in
`frontend/.env.production`. Then build the production code with:

```
$ cd frontend
$ npm run build
```

And put the files generated in `frontend/build` in you webserver's directory.

### Backend

Where the Python code is deployed, create a `.env` file with the parameters specific to your installation:

```
DATABASE_URL=mysql://username:password@localhost/database
SECRET_KEY=something-random
BROADCAST_BACKEND=redis://localhost:6379
ALLOW_HOSTS=https://your-public-url.tld
```

You can generate a secret key with `openssl rand -hex 32`.

Install the software (and pull the dependencies) with `poetry install -E deploy`

Then run it with `uvicorn speakinglist.main:app`

You should probably configure an event-based reverse proxy such as nginx in front of uvicorn.

### Database

Build a fresh database with `poetry run speakinglist init-db`.

Upgrade an existing database to the newest schema with `poetry run alembic -c alembic.ini upgrade head`.

## License

The code has been written by Aurélien Bompard (hopefully there will be other contributors in the future).

Speaking List is Free Software licensed under the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) or later.

The icon is "important document" by corpus delicti from the Noun Project (slightly modified to fit smaller sizes).

## TODO

Everyone:

- Prettify UI
- Logo
- Test on iPhone
- use ignoreResults on useMutation()? We never use the data result.

Participant:

- In-app notifications on events (SnackBar)
- Specific notification when turn has been ended by organizers
- store participant properties such as name and social categories in the user table and use them as default on the register form to avoid setting them on each event
- On WS reconnect, refresh the main queries

Organizer:

- A switch to automatically start next turn in line after a 5 seconds delay to allow organizers to announce it
- Add external apps such as onlinequestions.org
- On WS reconnect, refresh the main queries
- Show standard deviation in histograms (not sure Nivo supports it)
- A quick add button on the waiting list, that auto-completes participants

Sysadmins:

- installation doc
- cleanup events that never ended
