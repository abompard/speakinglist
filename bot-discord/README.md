# Speaking List bot for Discord

This package contains a Discord bot to act as the Speaking List interface for participants.
The available commands are:

- `/create [title]`: create an event with the provided title. It will return
  the event code and the admin code. This command is only available to server
  administrators. It should be run in the chat channel of a Discord voice
  channel.
- `/connect [code] [admin_code]`: connect the channel to an existing event.
  This command is only available to server administrators. It should be run in
  the chat channel of a Discord voice channel.
- `/register`: register as a participant for the current event. This command
  should be run by each participants once. It can only be run if an event is
  linked to the channel it is run in (with `/create` or `/connect`). Run the
  command again to edit your registration.
- `/turn`: request a speaking turn. This command can be run by participants
  once they have registered.

## Notes

Discord bots cannot stream video:
https://stackoverflow.com/questions/64724828/is-there-any-way-to-stream-videos-in-discord-py

OAuth2 permissions:

- Change Nickname
- Manage Nicknames
- Send Messages
- Use Slash Commands
- Use Soundboard
- Use External Sounds

→ https://discord.com/api/oauth2/authorize?client_id=1118818566528057404&permissions=35186720901120&scope=bot

## License

All the code is copyright Aurélien Bompard <aurelien(a)bompard.org>.
The license is AGPL-3.0-or-later.

Sounds by "Free Sounds Library" <http://www.freesoundslibrary.com>
Sounds Licence: Attribution 4.0 International (CC BY 4.0). You are allowed to
use sound effects free of charge and royalty free in your multimedia projects
for commercial or non-commercial purposes.

## TODO

- handle adding a second participant with /register
- generate a link with a JWT that discord users can click to get logged in in speakinglist as themselves.
