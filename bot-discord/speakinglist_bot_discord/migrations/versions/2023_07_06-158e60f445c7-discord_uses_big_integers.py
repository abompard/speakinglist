"""Discord uses big integers

Revision ID: 158e60f445c7
Revises: 5b2d7b9c5bd6
Create Date: 2023-07-06 23:50:30.656532

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "158e60f445c7"
down_revision = "5b2d7b9c5bd6"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("participants") as batch_op:
        batch_op.alter_column(
            "discord_id", type_=sa.BigInteger(), existing_type=sa.Integer()
        )
    with op.batch_alter_table("events") as batch_op:
        batch_op.alter_column(
            "channel_id", type_=sa.BigInteger(), existing_type=sa.Integer()
        )


def downgrade() -> None:
    with op.batch_alter_table("events") as batch_op:
        batch_op.alter_column(
            "channel_id", type_=sa.Integer(), existing_type=sa.BigInteger()
        )
    with op.batch_alter_table("participants") as batch_op:
        batch_op.alter_column(
            "discord_id", type_=sa.Integer(), existing_type=sa.BigInteger()
        )
