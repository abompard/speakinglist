"""participant

Revision ID: 9eeed4565617
Revises: 988f07f90efe
Create Date: 2023-06-27 12:52:09.760152

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "9eeed4565617"
down_revision = "988f07f90efe"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "participants",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("discord_id", sa.Unicode(254), nullable=False),
        sa.Column("speakinglist_id", sa.Integer(), nullable=False),
        sa.Column("event_id", sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(
            ["event_id"],
            ["events.id"],
            name=op.f("fk_participants_event_id_events"),
            ondelete="CASCADE",
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_participants")),
    )


def downgrade() -> None:
    op.drop_table("participants")
