"""Initial revision.

Revision ID: c25a7320ba9c
Create Date: 2023-06-26 21:44:57.068973

"""


# revision identifiers, used by Alembic.
revision = "c25a7320ba9c"
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    pass


def downgrade() -> None:
    pass
