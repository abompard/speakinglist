"""discord_id is int

Revision ID: 5b2d7b9c5bd6
Revises: 9eeed4565617
Create Date: 2023-07-04 00:35:20.589626

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5b2d7b9c5bd6"
down_revision = "9eeed4565617"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("participants") as batch_op:
        batch_op.alter_column(
            "discord_id", type_=sa.Integer(), existing_type=sa.Unicode(254)
        )


def downgrade() -> None:
    with op.batch_alter_table("participants") as batch_op:
        batch_op.alter_column(
            "discord_id", type_=sa.Unicode(254), existing_type=sa.Integer()
        )
