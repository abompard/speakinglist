"""no admin_code

Revision ID: 988f07f90efe
Revises: d5cc755a2fbe
Create Date: 2023-06-27 00:57:52.715379

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "988f07f90efe"
down_revision = "d5cc755a2fbe"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("events") as batch_op:
        batch_op.drop_column("admin_code")


def downgrade() -> None:
    with op.batch_alter_table("events") as batch_op:
        batch_op.add_column(sa.Column("admin_code", sa.TEXT(), nullable=False))
