"""Event locale

Revision ID: badc3e352e5f
Revises: 158e60f445c7
Create Date: 2023-07-09 19:58:57.593503

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "badc3e352e5f"
down_revision = "158e60f445c7"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column(
        "events", sa.Column("locale", sa.Unicode(length=16), nullable=False, default="fr")
    )


def downgrade() -> None:
    op.drop_column("events", "locale")
