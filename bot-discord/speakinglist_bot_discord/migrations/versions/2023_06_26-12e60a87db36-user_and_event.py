"""user and event

Revision ID: 12e60a87db36
Revises: c25a7320ba9c
Create Date: 2023-06-26 21:48:55.086038

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "12e60a87db36"
down_revision = "c25a7320ba9c"
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.create_table(
        "events",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("code", sa.Unicode(16), nullable=False),
        sa.Column("admin_code", sa.Unicode(64), nullable=False),
        sa.Column("channel_id", sa.Integer(), nullable=False),
        sa.Column("created", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_events")),
    )
    op.create_index(
        op.f("ix_events_channel_id"), "events", ["channel_id"], unique=False
    )
    op.create_index(op.f("ix_events_code"), "events", ["code"], unique=False)
    op.create_table(
        "users",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("token", sa.Unicode(254), nullable=False),
        sa.Column(
            "created",
            sa.DateTime(),
            server_default=sa.text("(CURRENT_TIMESTAMP)"),
            nullable=False,
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_users")),
    )
    op.create_index(op.f("ix_users_created"), "users", ["created"], unique=False)


def downgrade() -> None:
    op.drop_index(op.f("ix_users_created"), table_name="users")
    op.drop_table("users")
    op.drop_index(op.f("ix_events_code"), table_name="events")
    op.drop_index(op.f("ix_events_channel_id"), table_name="events")
    op.drop_table("events")
