"""event.user_id

Revision ID: d5cc755a2fbe
Revises: 12e60a87db36
Create Date: 2023-06-26 22:29:12.005844

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "d5cc755a2fbe"
down_revision = "12e60a87db36"
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table("events") as batch_op:
        batch_op.add_column(sa.Column("user_id", sa.Integer(), nullable=False))
        batch_op.create_foreign_key(
            op.f("fk_events_user_id_users"),
            "users",
            ["user_id"],
            ["id"],
            ondelete="CASCADE",
        )


def downgrade() -> None:
    with op.batch_alter_table("events") as batch_op:
        batch_op.drop_constraint(op.f("fk_events_user_id_users"), type_="foreignkey")
        batch_op.drop_column("user_id")
