import gettext
import pathlib
from contextlib import contextmanager, nullcontext
from contextvars import ContextVar
from functools import wraps

import discord
from babel.support import LazyProxy
from discord import app_commands

TRANSLATORS = {}
_lang: ContextVar[discord.Locale] = ContextVar(
    "lang", default=discord.Locale.american_english
)


def build_translators(domain, localedir):
    supported_languages = [
        mofile.parent.parent.name
        for mofile in pathlib.Path(localedir).glob(f"*/LC_MESSAGES/{domain}.mo")
    ]
    translators = {
        lang: gettext.translation(domain, localedir, languages=[lang])
        for lang in supported_languages
    }
    for discord_lang in discord.Locale:
        if "-" not in discord_lang.value or discord_lang.value in supported_languages:
            continue
        main, _region = discord_lang.value.split("-", 1)
        if main not in supported_languages:
            continue
        translators[discord_lang.value] = translators[main]
    return translators


@contextmanager
def _use_locale(locale: discord.Locale):
    prev_lang = _lang.get()
    _lang.set(locale)
    try:
        yield
    finally:
        _lang.set(prev_lang)


def use_locale(locale: discord.Locale | None):
    if locale is None:
        return nullcontext()
    else:
        return _use_locale(locale)


_SAME = object()


def translate(
    string: str, locale: discord.Locale, default: str | None | type[_SAME] = _SAME
):
    try:
        translator = TRANSLATORS[locale.value]
    except KeyError:
        # We don't handle this locale
        if default == _SAME:
            return string
        else:
            return default
    return translator.gettext(string)


class GettextTranslator(app_commands.Translator):
    def __init__(self, domain="messages", localedir="/usr/share/locale"):
        self._domain = domain
        self._localedir = localedir

    async def load(self):
        TRANSLATORS.update(build_translators(self._domain, self._localedir))

    async def translate(
        self,
        string: app_commands.locale_str,
        locale: discord.Locale,
        context: app_commands.TranslationContext,
    ) -> str | None:
        return translate(string.message, locale, None)


class gettext_str(app_commands.locale_str):
    def __str__(self):
        return translate(self.message, _lang.get())

    def format(self, *args, **kwargs):
        return str(self).format(*args, **kwargs)

    @property
    def lazy(self):
        return LazyProxy(self.__str__, enable_cache=False)
