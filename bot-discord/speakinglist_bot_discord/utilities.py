import json
from dataclasses import dataclass

import discord

from . import models
from .i18n import gettext_str as _


class InteractionError(Exception):
    def __init__(self, message):
        self.message = message


async def get_participant(cog, interaction):
    async with cog.bot.db_manager.Session() as session:
        return await models.Participant.get_from_discord_id_in_channel(
            session, interaction.user.id, interaction.channel_id
        )


@dataclass
class Notification:
    msg_id: str
    changes: dict[str, str]


def parse_notification(event):
    message = json.loads(event.message)
    changes = {v["name"]: v["value"] for v in message["values"]}
    return Notification(msg_id=message["msgid"], changes=changes)


def get_member_in_channel(channel, *, name=None, discord_id=None):
    for member in channel.members:
        if name is not None and member.display_name == name:
            return member
        if discord_id is not None and member.id == discord_id:
            return member


def connect_permitted(bot, channel):
    if channel.type != discord.ChannelType.voice:
        return False
    bot_member = channel.guild.get_member(bot.user.id)
    return bot_member is not None and channel.permissions_for(bot_member).connect


def check_connect_permitted(bot, channel):
    if not connect_permitted(bot, channel):
        raise InteractionError(
            _(
                "I am not allowed to connect to this channel, please check my permissions."
            )
        )


async def connect_if_permitted(bot, channel):
    if connect_permitted(bot, channel):
        await channel.connect(reconnect=True, self_deaf=True)


def duration_to_text(duration):
    result = []
    if duration >= 60:
        # NOTE: abbreviation for "minute"
        result.append(
            _("{m}m").format(
                m=duration // 60,
            )
        )
    # NOTE: abbreviation for "second"
    result.append(
        _("{s:02.0f}s").format(
            s=duration % 60,
        )
    )
    return "".join(result)
