import asyncio
import logging

log = logging.getLogger(__name__)


def print_err(task):
    exc = task.exception()
    if exc is None:
        return
    logging.exception(f"Task {task.get_name()} failed!")
    task.print_stack()


class BackgroundTasksManager:
    def __init__(self):
        self.tasks = set()

    def run(self, coro, name=None):
        task = asyncio.create_task(coro, name=name)
        self.tasks.add(task)
        task.add_done_callback(self.tasks.discard)
        task.add_done_callback(print_err)
        return task

    def stop(self):
        for task in self.tasks:
            task.cancel()

    def cancel_named(self, name: str):
        for task in self.tasks:
            if task.get_name() == name:
                task.cancel()
