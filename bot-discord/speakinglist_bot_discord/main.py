import asyncio
import logging
import logging.config
import signal
from functools import partial

import click
import yaml

from . import settings
from .bot import SpeakingListBot

EXTENSIONS = (
    "speakinglist_bot_discord.cogs.event",
    "speakinglist_bot_discord.cogs.register",
    "speakinglist_bot_discord.cogs.turn",
    "speakinglist_bot_discord.cogs.api_client",
    "speakinglist_bot_discord.cogs.presence",
)


@click.group(name="speakinglist-bot-discord")
@click.option(
    "--log-config", type=click.File("r"), help="Logging configuration in a YAML file"
)
def cli(log_config):
    """Discord bot for Speaking List"""
    if log_config:
        logging.config.dictConfig(yaml.safe_load(log_config))
    else:
        logging.basicConfig(level=logging.INFO)


@cli.command()
def start():
    """Start the bot"""

    async def _doit():
        loop = asyncio.get_running_loop()
        bot = SpeakingListBot()
        loop.add_signal_handler(
            signal.SIGHUP, partial(_reload_signal_handler, loop, bot)
        )
        async with bot:
            for ext in EXTENSIONS:
                await bot.load_extension(ext)
            try:
                await bot.start(str(settings.DISCORD_TOKEN))
            except KeyboardInterrupt:
                await bot.stop()
            except asyncio.CancelledError:
                await bot.stop()

    asyncio.run(_doit())


def _reload_signal_handler(loop, bot):
    loop.create_task(bot.reload_extensions())
