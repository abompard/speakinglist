import asyncio
import logging
from typing import Union

import discord
from broadcaster import Broadcast
from discord.ext import commands

from . import database, models, settings
from .background import BackgroundTasksManager
from .event_manager import EventManager, EventNotFound
from .i18n import GettextTranslator

log = logging.getLogger(__name__)


class SpeakingListBot(commands.Bot):
    def __init__(self, *args, **kwargs):
        print("Speaking List Discord Bot starting")
        intents = discord.Intents.default()
        intents.message_content = True
        intents.members = True
        intents.presences = True
        intents.voice_states = True
        super().__init__(
            command_prefix="!",
            description="Speaking List bot",
            intents=intents,
            allowed_mentions=discord.AllowedMentions.all(),
        )
        self.db_manager = database.get_manager()
        self.broadcaster = Broadcast(str(settings.BROADCAST_BACKEND))
        self._extensions = []
        self._event_managers = {}
        self._bg = BackgroundTasksManager()

    async def get_sl_user(self) -> models.SpeakingListUser | None:
        async with self.db_manager.Session() as session:
            return await models.SpeakingListUser.get_latest(session)

    async def get_event_in_channel(self, channel_id: int) -> models.Event:
        async with self.db_manager.Session() as session:
            return await models.Event.get_in_channel(session, channel_id)

    async def get_channel_of_event(self, event_code: str) -> models.Event:
        async with self.db_manager.Session() as session:
            return await models.Event.get_channel_of(session, event_code)

    def get_event_manager_in_channel(
        self, channel_id: int
    ) -> Union[EventManager, None]:
        return self._event_managers.get(channel_id)

    def get_voice_client(self, channel_id: int) -> Union[discord.VoiceClient, None]:
        for vc in self.voice_clients:
            if vc.channel.id == channel_id and vc.is_connected():
                return vc

    def is_only_event_in_guild(self, channel: discord.abc.GuildChannel):
        """Returns whether a channel is the only channel hosting a Speaking List event in the server/guild."""
        for channel_id in self._event_managers.keys():
            if channel.id == channel_id:
                continue
            channel_with_event = self.get_channel(channel_id)
            if channel_with_event is None:
                continue
            if channel_with_event.guild.id == channel.guild.id:
                return False
        return True

    async def register_event_manager(
        self, event_manager: EventManager, channel_id: int
    ):
        log.debug(f"Registering event manager for: {event_manager.event_code}")
        self._event_managers[channel_id] = event_manager
        self._bg.run(
            event_manager.subscribe_to_notifications(self.broadcaster),
            name=f"sub:{event_manager.event_code}",
        )

    async def unregister_event_manager(self, event_manager: EventManager):
        log.debug(f"Unregistering event manager for {event_manager.event_code}")
        for channel_id, em in self._event_managers.items():  # noqa: B007
            if em == event_manager:
                break
        del self._event_managers[channel_id]
        # close background task
        for task in self._bg.tasks:
            if task.get_name() == f"sub:{event_manager.event_code}":
                task.cancel()

    async def setup_hook(self) -> None:
        await self.tree.set_translator(
            GettextTranslator(localedir=settings.ROOT_DIR.joinpath("locale"))
        )
        await self.tree.sync()
        await self.broadcaster.connect()

    async def on_ready(self):
        log.info(f"Logged in as {self.user} (ID: {self.user.id})")
        await self._api_client_connected
        await self.load_event_managers()
        await self.resume_extensions()

    async def load_event_managers(self):
        log.info("Reloading event managers")
        user = await self.get_sl_user()
        async with self.db_manager.Session() as db:
            for event in await models.Event.all(db):
                try:
                    event_manager = await EventManager.load(
                        event.code, self.db_manager, self.api_client, user
                    )
                except EventNotFound:
                    await db.delete(event)
                    continue
                # if await event_manager.is_closed():
                # Don't do that! Events can start again.
                #     await db.delete(event)
                #     continue
                if event.channel_id in self._event_managers:
                    log.info(
                        f"Event manager for event {event.code} already exists, skipping"
                    )
                else:
                    await self.register_event_manager(event_manager, event.channel_id)
            await db.commit()

    async def reload_extensions(self):
        log.info("Reloading extensions")
        current_extensions = self.extensions.copy()
        for extname in current_extensions.keys():
            await self.reload_extension(extname)
        await self.resume_extensions()

    async def resume_extensions(self):
        resumable_exts = [
            module for module in self.extensions.values() if hasattr(module, "resume")
        ]
        log.debug("Resuming extensions")
        async with self.db_manager.Session() as db:
            current_events = list(await models.Event.all(db))
        await asyncio.gather(
            *[module.resume(self, current_events) for module in resumable_exts]
        )

    async def stop(self):
        log.info("Stopping Speaking List Discord bot")
        await self.broadcaster.disconnect()
        self._bg.stop()
        if self.api_client is not None:
            await self.api_client.disconnect()
        await super().close()
