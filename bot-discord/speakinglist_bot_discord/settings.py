from pathlib import Path

from dotenv import load_dotenv
from starlette.config import Config
from starlette.datastructures import URL, Secret

load_dotenv()

ROOT_DIR = Path(__file__).parent.absolute()

config = Config(".env.bot-discord")

DEBUG = config("DEBUG", cast=bool, default=False)
TESTING = config("TESTING", cast=bool, default=False)

INSTANCE_URL = config("INSTANCE_URL", default="wss://speakinglist.net")
INSTANCE_WEB_URL = config("INSTANCE_WEB_URL", default="https://speakinglist.net")
DISCORD_TOKEN = config("DISCORD_TOKEN", cast=Secret)

DATABASE_URL = config("DATABASE_URL", cast=URL)
DATABASE_MAX_OVERFLOW = config("DATABASE_MAX_OVERFLOW", cast=int, default=-1)

if TESTING:
    DATABASE_URL = DATABASE_URL.replace(database="test_" + DATABASE_URL.database)

BROADCAST_BACKEND = config("BROADCAST_BACKEND", cast=URL)

BROADCAST_PREFIX = config("BROADCAST_PREFIX", default="bot:discord:")

RESOURCES_DIR = config(
    "RESOURCES_DIR", default=ROOT_DIR.joinpath("..", "resources").absolute(), cast=Path
)
