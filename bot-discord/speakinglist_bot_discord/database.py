from sqlalchemy_helpers import Base
from sqlalchemy_helpers.aio import AsyncDatabaseManager

from . import settings
from . import models


def get_manager():
    db_url = str(settings.DATABASE_URL)
    engine_args = {
        "pool_recycle": 3600 * 2,
    }

    if db_url.startswith("sqlite://"):
        engine_args["connect_args"] = {"check_same_thread": False}
    else:
        engine_args["max_overflow"] = settings.DATABASE_MAX_OVERFLOW

    alembic_location = "speakinglist_bot_discord:migrations"
    return AsyncDatabaseManager(
        db_url, alembic_location, engine_args=engine_args,
    )
