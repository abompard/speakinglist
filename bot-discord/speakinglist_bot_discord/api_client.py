import asyncio
import logging
from dataclasses import dataclass, field
from typing import Optional

from gql import Client as GQLClient
from gql.transport import AsyncTransport
from gql.transport.aiohttp import AIOHTTPTransport
from gql.transport.exceptions import TransportQueryError
from gql.transport.websockets import WebsocketsTransport

from . import settings

log = logging.getLogger(__name__)


@dataclass
class Event:
    code: str
    adminCode: str
    title: str
    turnMaxDuration: int


@dataclass
class SocialCategory:
    id: int
    name: str


@dataclass
class Participant:
    id: int
    name: str
    social_categories: dict[str, str] = field(default_factory=dict)


@dataclass
class Turn:
    id: int
    startedAt: str
    requestedAt: str
    endedAt: str
    participantId: Optional[int] = None


@dataclass
class Notification:
    msg_id: str
    values: dict[str, str] = field(default_factory=dict)

    @classmethod
    def from_subscription(cls, message):
        values = {v["name"]: v["value"] for v in message.get("values", {})}
        return cls(msg_id=message["msgid"], values=values)


class ServerError(Exception):
    def __init__(self, message):
        self.message = message


class SplitTransport(AsyncTransport):
    def __init__(self, instance_url, token, **kwargs):
        ping_interval = kwargs.pop("ping_interval", None)
        pong_timeout = kwargs.pop("pong_timeout", None)
        self._token = None
        self.req = AIOHTTPTransport(url=f"{instance_url}/graphql/", **kwargs)
        self.ws = WebsocketsTransport(
            url=f"{instance_url}/ws/",
            ping_interval=ping_interval,
            pong_timeout=pong_timeout,
            **kwargs,
        )
        self.token = token

    @property
    def token(self):
        return self._token

    @token.setter
    def token(self, value):
        self._token = value
        if value:
            self.ws.init_payload = {"authToken": value}
            self.req.headers = {"Authorization": f"Bearer {value}"}
        else:
            del self.ws.init_payload["authToken"]
            del self.req.headers["Authorization"]

    async def connect(self):
        return await asyncio.gather(self.req.connect(), self.ws.connect())

    async def close(self):
        return await asyncio.gather(self.req.close(), self.ws.close())

    async def execute(self, *args, **kwargs):
        return await self.req.execute(*args, **kwargs)

    async def subscribe(self, *args, **kwargs):
        async for result in self.ws.subscribe(*args, **kwargs):
            yield result


class APIClient:
    def __init__(self, token=None):
        # transport = SplitTransport(
        #     settings.INSTANCE_URL, token=self._token, ping_interval=60, pong_timeout=10
        # )
        # self._gql_client = GQLClient(
        #     transport=transport, fetch_schema_from_transport=True
        # )
        self._session_ws = None
        transport_req = AIOHTTPTransport(url=f"{settings.INSTANCE_URL}/graphql/")
        self._client_req = GQLClient(transport=transport_req)
        transport_ws = WebsocketsTransport(
            url=f"{settings.INSTANCE_URL}/ws/",
            ping_interval=60,
            pong_timeout=10,
        )
        self._client_ws = GQLClient(transport=transport_ws)
        self.set_token(token)

    def set_token(self, value):
        if value:
            self._client_ws.transport.init_payload = {"authToken": value}
            self._client_req.transport.headers = {"Authorization": f"Bearer {value}"}
        else:
            try:
                del self._client_ws.transport.init_payload["authToken"]
            except KeyError:
                pass
            try:
                del self._client_req.transport.headers["Authorization"]
            except (KeyError, TypeError):
                # TypeError happens when headers is None
                pass

    async def connect(self):
        async with self._client_req as session:
            await session.fetch_schema()
        self._client_ws.schema = self._client_req.schema
        if self._session_ws is None:
            self._session_ws = await self._client_ws.connect_async(reconnecting=True)

    async def disconnect(self):
        await self._client_ws.close_async()
        self._session_ws = None

    async def execute(self, *args, **kwargs):
        async with self._client_req as session:
            try:
                return await session.execute(*args, **kwargs)
            except TransportQueryError as e:
                log.exception(e)
                raise ServerError(e.errors[0]["message"]) from e

    async def subscribe(self, *args, **kwargs):
        if self._session_ws is None:
            await self.connect()
        async for result in self._session_ws.subscribe(*args, **kwargs):
            yield result
