from sqlalchemy import BigInteger, Column, ForeignKey, Integer, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from ..database import Base
from .event import Event


class Participant(Base):
    __tablename__ = "participants"

    id = Column(Integer, primary_key=True, nullable=False)
    discord_id = Column(BigInteger, nullable=False)
    speakinglist_id = Column(Integer, nullable=False)
    event_id = Column(
        Integer,
        ForeignKey("events.id", ondelete="CASCADE"),
        nullable=False,
    )
    event = relationship("Event", back_populates="participants")

    @classmethod
    async def get_from_discord_id_in_channel(
        cls, session: AsyncSession, discord_id: int, channel_id: int
    ):
        query = (
            select(cls)
            .join(Event)
            .where(Event.channel_id == channel_id, cls.discord_id == discord_id)
        )
        result = await session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    async def get_from_speakinglist_id_in_channel(
        cls, session: AsyncSession, speakinglist_id: int, channel_id: int
    ):
        query = (
            select(cls)
            .join(Event)
            .where(
                Event.channel_id == channel_id, cls.speakinglist_id == speakinglist_id
            )
        )
        result = await session.execute(query)
        return result.scalar_one_or_none()

    @classmethod
    async def get_from_speakinglist_id_in_event(
        cls, session: AsyncSession, speakinglist_id: int, event_code: str
    ):
        query = (
            select(cls)
            .join(Event)
            .where(Event.code == event_code, cls.speakinglist_id == speakinglist_id)
        )
        result = await session.execute(query)
        return result.scalar_one_or_none()
