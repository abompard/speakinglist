import discord
from sqlalchemy import (
    BigInteger,
    Column,
    DateTime,
    ForeignKey,
    Integer,
    Unicode,
    select,
)
from sqlalchemy.exc import NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from ..database import Base


class Event(Base):
    __tablename__ = "events"

    id = Column(Integer, primary_key=True, nullable=False)
    code = Column(Unicode(16), nullable=False, index=True)
    channel_id = Column(BigInteger, nullable=False, index=True)
    created = Column(DateTime(timezone=False), nullable=True)
    locale = Column(Unicode(16), nullable=False)
    user_id = Column(
        Integer,
        ForeignKey("users.id", ondelete="CASCADE"),
        nullable=False,
    )
    user = relationship("SpeakingListUser", back_populates="events")
    participants = relationship(
        "Participant",
        back_populates="event",
        cascade="all, delete",
        passive_deletes=True,
    )

    @classmethod
    async def get_in_channel(cls, session: AsyncSession, channel_id: int):
        result = await session.execute(select(cls).where(cls.channel_id == channel_id))
        return result.scalar_one_or_none()

    @classmethod
    async def get_channel_of(cls, session: AsyncSession, code: str):
        result = await session.execute(select(cls.channel_id).where(cls.code == code))
        return result.scalar_one_or_none()

    @classmethod
    async def get_locale_of(cls, session: AsyncSession, code: str):
        result = await session.execute(select(cls.locale).where(cls.code == code))
        try:
            locale = result.scalar_one()
        except NoResultFound:
            return None
        else:
            return discord.Locale(locale)

    @classmethod
    async def all(cls, session: AsyncSession):
        result = await session.execute(select(cls))
        return result.scalars()
