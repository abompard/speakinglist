from datetime import datetime

from sqlalchemy import Column, DateTime, Integer, Unicode, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import relationship

from ..database import Base


class SpeakingListUser(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, nullable=False)
    token = Column(Unicode(254), nullable=False)
    created = Column(
        DateTime(timezone=False),
        index=True,
        default=datetime.now,
        server_default=func.now(),
        nullable=False,
    )
    events = relationship("Event", back_populates="user")

    @classmethod
    async def get_latest(cls, session: AsyncSession):
        result = await session.execute(
            select(cls).order_by(cls.created.desc()).limit(1)
        )
        return result.scalar_one_or_none()
