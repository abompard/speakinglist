import dataclasses
import json
import logging

import backoff
from gql import gql
from gql.transport.exceptions import TransportClosed
from websockets.exceptions import ConnectionClosedError

from . import models, settings
from .api_client import Event, Notification, Participant, SocialCategory, Turn

log = logging.getLogger(__name__)


class EventNotFound(Exception):
    pass


class Change:
    def dump(self):
        return dataclasses.asdict(self)

    @classmethod
    def load(cls, data: dict):
        values = {}
        for field in dataclasses.fields(cls):
            value = data[field.name]
            if dataclasses.is_dataclass(field.type):
                value = field.type.load(value)
            values[field.name] = value
        return cls(**values)

    def _get_shallow_dict(self):
        shallow_dict = dict(
            (field.name, getattr(self, field.name))
            for field in dataclasses.fields(self)
        )
        del shallow_dict["action"]
        return shallow_dict

    @classmethod
    def broadcast_channel_for(cls, code) -> str:
        ...

    @property
    def broadcast_channel(self) -> str:
        return self.__class__.broadcast_channel_for(**self._get_shallow_dict())


@dataclasses.dataclass
class EventChange(Change):
    code: str
    action: str

    @classmethod
    def broadcast_channel_for(cls, code) -> str:
        return f"event:{code}"


@dataclasses.dataclass
class TurnChange(Change):
    event_code: str
    participant_name: str
    speakinglist_id: str
    turn_id: int
    action: str

    @classmethod
    def broadcast_channel_for(cls, event_code, turn_id=None, **kwargs) -> str:
        if turn_id is not None:
            return f"event:{event_code}:turn:{turn_id}"
        else:
            return f"event:{event_code}:turn:new"


@dataclasses.dataclass
class ParticipantChange(Change):
    event_code: str
    speakinglist_id: int
    name: str
    action: str

    @classmethod
    def broadcast_channel_for(cls, event_code, speakinglist_id=None, **kwargs) -> str:
        if speakinglist_id is None:
            return f"event:{event_code}:participants"
        else:
            return f"event:{event_code}:participant:{speakinglist_id}"


def sc_to_dict(sc_list):
    return {sc["name"]: sc["value"] for sc in sc_list}


async def _publish(broadcaster, channel, change):
    await broadcaster.publish(
        channel=f"{settings.BROADCAST_PREFIX}{channel}",
        message=json.dumps(change.dump()),
    )


class EventManager:
    def __init__(
        self, db_manager, api_client, event: Event, user: models.SpeakingListUser
    ):
        self.db_manager = db_manager
        self.event = event
        self.user = user
        self.api_client = api_client

    @property
    def event_code(self):
        return self.event.code

    async def _update_user(self, me_response):
        async with self.db_manager.Session() as db:
            if self.user is None:
                self.user = models.SpeakingListUser(token=me_response["token"])
                db.add(self.user)
            else:
                self.user.token = me_response["token"]
            await db.commit()
        self.api_client.token = self.user.token
        await self.api_client.execute(
            gql(
                """
                subscription OnTokenChanged($token: String!) {
                    updateSubscriptionToken(token: $token)
                }
                """
            ),
            variable_values={"token": self.user.token},
        )

    # async def update_user(self):
    #     async with self.api_client as c:
    #         query = gql(
    #             """
    #           query GetMe($eventCode: ID!) {
    #             event(code: $eventCode) {
    #               code
    #               me {
    #                 organizer
    #                 token
    #               }
    #             }
    #           }
    #         """
    #         )
    #         response = await c.execute(query)
    #     await self._update_user(response["event"]["me"])

    @classmethod
    async def create(cls, title, db_manager, api_client, user=None):
        query = gql(
            """
            mutation CreateEvent($title: String!) {
                createEvent(title: $title) {
                    event {
                        code
                        title
                        adminCode
                        turnMaxDuration
                    }
                    me {
                        token
                    }
                }
            }
            """
        )
        response = await api_client.execute(query, variable_values={"title": title})
        event = Event(**response["createEvent"]["event"])
        instance = cls(db_manager, api_client, event=event, user=user)
        await instance._update_user(response["createEvent"]["me"])
        return instance

    @classmethod
    async def connect(cls, event_code, admin_code, db_manager, api_client, user=None):
        query = gql(
            """
            mutation CheckOrga($eventCode: ID!, $adminCode: String!) {
                event(code: $eventCode) {
                    code
                    title
                    turnMaxDuration
                    checkOrga(adminCode: $adminCode) {
                        me {
                            organizer
                            token
                        }
                    }
                }
            }
            """
        )
        response = await api_client.execute(
            query,
            variable_values={"eventCode": event_code, "adminCode": admin_code},
        )
        me = response["event"].pop("checkOrga")["me"]
        event = Event(**response["event"], adminCode=admin_code)
        instance = cls(db_manager, api_client, event=event, user=user)
        await instance._update_user(me)
        return instance

    @classmethod
    async def load(cls, event_code, db_manager, api_client, user):
        query = gql(
            """
            query GetEvent($code: ID!) {
                event(code: $code) {
                    code
                    title
                    adminCode
                    turnMaxDuration
                }
            }
            """
        )
        response = await api_client.execute(
            query,
            variable_values={"code": event_code},
        )
        if response["event"] is None:
            raise EventNotFound(event_code)
        event = Event(**response["event"])
        instance = cls(db_manager, api_client, event=event, user=user)
        return instance

    @backoff.on_exception(backoff.expo, (ConnectionClosedError, TransportClosed))
    async def subscribe_to_notifications(self, broadcaster):
        query = gql(
            """
            subscription OnNotifications($eventCode: ID!) {
                notifications(eventCode: $eventCode) {
                    msgid
                    values {
                        name
                        value
                    }
                }
            }
            """
        )
        async for message in self.api_client.subscribe(
            query, variable_values={"eventCode": self.event_code}
        ):
            print(f"Got notification: {message}")
            log.debug(f"Got notification: {message}")
            notif = Notification.from_subscription(message["notifications"])
            change = None
            if notif.msg_id == "EVENT_STATE_CHANGED":
                try:
                    action = notif.values["action"]
                except KeyError:
                    log.error(f"Not sure what to do with this: {message}")
                    continue
                change = EventChange(code=self.event_code, action=action)
            elif notif.msg_id == "EVENT_DELETED":
                change = EventChange(code=self.event_code, action="delete")
            elif notif.msg_id.startswith("TURN_"):
                try:
                    user_name = notif.values["participant_name"]
                    user_id = notif.values["participant_id"]
                    turn_id = notif.values["id"]
                except KeyError:
                    log.error(f"Not sure what to do with this: {message}")
                    continue
                if notif.msg_id == "TURN_REQUESTED":
                    action = "request"
                elif notif.msg_id == "TURN_STARTED":
                    action = "start"
                elif notif.msg_id == "TURN_STOPPED":
                    action = "stop"
                elif notif.msg_id == "TURN_CANCELLED":
                    action = "cancel"
                change = TurnChange(
                    event_code=self.event_code,
                    turn_id=turn_id,
                    participant_name=user_name,
                    speakinglist_id=user_id,
                    action=action,
                )
            if notif.msg_id == "PARTICIPANT_DELETED":
                try:
                    participant_name = notif.values["name"]
                    participant_id = notif.values["id"]
                except KeyError:
                    log.error(f"Not sure what to do with this: {message}")
                    continue
                change = ParticipantChange(
                    event_code=self.event_code,
                    speakinglist_id=participant_id,
                    name=participant_name,
                    action="delete",
                )
            if change is not None:
                await _publish(broadcaster, change.broadcast_channel, change)
                # Also broadcast to the event-generic channels
                if isinstance(change, TurnChange):
                    await _publish(
                        broadcaster,
                        TurnChange.broadcast_channel_for(self.event.code),
                        change,
                    )
                if isinstance(change, ParticipantChange):
                    await _publish(
                        broadcaster,
                        ParticipantChange.broadcast_channel_for(self.event.code),
                        change,
                    )

    async def is_closed(self):
        query = gql(
            """
            query GetEvent($code: ID!) {
                event(code: $code) {
                    endedAt
                }
            }
            """
        )
        response = await self.api_client.execute(
            query, variable_values={"code": self.event_code}
        )
        return response["event"] is None or response["event"]["endedAt"] is not None

    async def get_social_categories(self):
        query = gql(
            """
            query GetSocialCategories($eventCode: ID!) {
                event(code: $eventCode) {
                    code
                    socialCategories {
                        id
                        name
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query, variable_values={"eventCode": self.event_code}
        )
        result = []
        for sc in response["event"]["socialCategories"] or []:
            result.append(SocialCategory(id=sc["id"], name=sc["name"]))
        return result

    async def register_participant(self, name, social_categories):
        query = gql(
            """
            mutation AddParticipant(
            $eventCode: ID!
            $name: String!
            $socialCategories: [SocialCategoryInput]
            $me: Boolean
            ) {
                event(code: $eventCode) {
                    code
                    addParticipant(
                        name: $name
                        socialCategories: $socialCategories
                        me: $me
                    ) {
                        id
                        name
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "name": name,
                "socialCategories": [
                    {"name": key, "value": value}
                    for key, value in social_categories.items()
                ],
                "me": False,
            },
        )
        return Participant(**response["event"]["addParticipant"])

    async def update_participant(self, participant_id, name, social_categories):
        query = gql(
            """
            mutation UpdateParticipant(
                $eventCode: ID!
                $participantId: ID!
                $name: String!
                $socialCategories: [SocialCategoryInput]
                $me: Boolean
            ) {
                event(code: $eventCode) {
                    participant(id: $participantId) {
                        update(name: $name, socialCategories: $socialCategories, me: $me) {
                            id
                            name
                            socialCategories {
                                id
                                name
                                value
                            }
                        }
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
                "name": name,
                "socialCategories": [
                    {"name": key, "value": value}
                    for key, value in social_categories.items()
                ],
                "me": False,
            },
        )
        values = response["event"]["participant"]["update"]
        values["social_categories"] = sc_to_dict(values.pop("socialCategories"))
        return Participant(**values)

    async def get_participant(self, participant_id):
        query = gql(
            """
            query GetParticipant(
                $eventCode: ID!
                $participantId: ID!
            ) {
                event(code: $eventCode) {
                    participant(id: $participantId) {
                        id
                        name
                        socialCategories {
                            id
                            name
                            value
                        }
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
            },
        )
        values = response["event"]["participant"]
        values["social_categories"] = sc_to_dict(values.pop("socialCategories"))
        return Participant(**values)

    async def update_participant_presence(self, participant_id, is_present):
        query = gql(
            """
            mutation ParticipantSetActive(
                $eventCode: ID!
                $participantId: ID!
                $isActive: Boolean!
            ) {
                event(code: $eventCode) {
                    participant(id: $participantId) {
                        setActive(isActive: $isActive) {
                            id
                            isActive
                        }
                    }
                }
            }
        """
        )
        await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
                "isActive": is_present,
            },
        )

    async def request_turn(self, participant_id):
        query = gql(
            """
            mutation RequestTurn($eventCode: ID!, $participantId: ID!) {
                event(code: $eventCode) {
                code
                participant(id: $participantId) {
                    id
                    requestTurn {
                        turn {
                            id
                            startedAt
                            endedAt
                            requestedAt
                        }
                    }
                }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
            },
        )
        return Turn(**response["event"]["participant"]["requestTurn"]["turn"])

    async def cancel_turn(self, participant_id, turn_id):
        query = gql(
            """
            mutation CancelTurn($eventCode: ID!, $participantId: ID!, $turnId: ID!) {
            event(code: $eventCode) {
                code
                participant(id: $participantId) {
                    id
                    turn(id: $turnId) {
                        cancel {
                            turn {
                                id
                                startedAt
                                endedAt
                            }
                        }
                    }
                }
            }
            }
        """
        )
        await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
                "turnId": turn_id,
            },
        )

    async def get_pending_turn(self, participant_id: int):
        query = gql(
            """
            query GetPendingTurn(
                $eventCode: ID!
                $participantId: ID!
            ) {
                event(code: $eventCode) {
                    participant(id: $participantId) {
                        turns {
                            id
                            startedAt
                            endedAt
                            requestedAt
                        }
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
            },
        )
        for turn in response["event"]["participant"]["turns"]:
            if turn["startedAt"] is None and turn["endedAt"] is None:
                return Turn(**turn)

    async def get_turn(self, participant_id: int, turn_id: int):
        query = gql(
            """
            query GetTurn(
                $eventCode: ID!
                $participantId: ID!
                $turnId: ID!
            ) {
                event(code: $eventCode) {
                    participant(id: $participantId) {
                        turn(id: $turnId) {
                            id
                            startedAt
                            endedAt
                            requestedAt
                        }
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
                "participantId": participant_id,
                "turnId": turn_id,
            },
        )
        return Turn(**response["event"]["participant"]["turn"])

    async def get_pending_turns(self):
        query = gql(
            """
            query GetPendingTurns(
                $eventCode: ID!
            ) {
                event(code: $eventCode) {
                    turns {
                        id
                        startedAt
                        endedAt
                        requestedAt
                        participantId
                    }
                }
            }
        """
        )
        response = await self.api_client.execute(
            query,
            variable_values={
                "eventCode": self.event_code,
            },
        )
        for turn in response["event"]["turns"]:
            if turn["startedAt"] is None and turn["endedAt"] is None:
                yield Turn(**turn)
