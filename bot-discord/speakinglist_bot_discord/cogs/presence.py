import logging

import discord
from sqlalchemy.exc import NoResultFound

from .. import models
from ..event_manager import EventManager
from ..i18n import gettext_str as _
from ..i18n import use_locale
from .base import BaseCog
from .base import teardown as base_teardown

log = logging.getLogger(__name__)


class PresenceCog(BaseCog):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot.add_listener(self.on_voice_state_update, "on_voice_state_update")

    async def on_voice_state_update(
        self,
        member: discord.Member,
        before: discord.VoiceState,
        after: discord.VoiceState,
    ):
        if member.id == self.bot.user.id:
            return  # That's us.
        channel = after.channel or before.channel
        event_manager = self.bot.get_event_manager_in_channel(channel.id)
        if event_manager is None:
            return
        await self._check_last(channel)
        async with self.bot.db_manager.Session() as db:
            try:
                participant = await models.Participant.get_one(db, discord_id=member.id)
            except NoResultFound:
                return
        if before.channel is None and after.channel is not None:
            await self._participant_joined(participant, event_manager)
        elif before.channel is not None and after.channel is None:
            await self._participant_left(participant, event_manager)

    async def _participant_joined(
        self, participant: models.Participant, event_manager: EventManager
    ):
        await event_manager.update_participant_presence(
            participant.speakinglist_id, is_present=True
        )

    async def _participant_left(
        self, participant: models.Participant, event_manager: EventManager
    ):
        await event_manager.update_participant_presence(
            participant.speakinglist_id, is_present=False
        )

    async def _check_last(self, channel: discord.abc.GuildChannel):
        if len(channel.members) > 1 or (
            len(channel.members) == 1 and channel.members[0].id != self.bot.user.id
        ):
            return
        # Only the bot is left, disconnect from the event and cleanup.
        event_cog = self.bot.get_cog("EventCog")
        if event_cog is None:
            return
        locale = channel.guild.preferred_locale
        async with self.bot.db_manager.Session() as db:
            event = await models.Event.get_in_channel(db, channel.id)
            if event:
                locale = discord.Locale(event.locale)
        with use_locale(locale):
            await channel.send(
                str(_("I'm the only one left in this channel, disconnecting."))
            )
        await event_cog.leave_event(channel.id)


async def setup(bot):
    await bot.add_cog(PresenceCog(bot))


async def teardown(bot):
    await base_teardown(bot, "PresenceCog")
    await base_teardown(bot, "PresenceCog")
