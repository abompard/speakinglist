import json
import logging
from functools import wraps

import discord
from discord import app_commands

from .. import models, settings
from ..event_manager import EventChange, EventManager
from ..i18n import gettext_str as _
from ..i18n import use_locale
from ..utilities import (
    InteractionError,
    check_connect_permitted,
    connect_if_permitted,
    duration_to_text,
)
from .base import BaseCog
from .base import resume as base_resume
from .base import set_lang
from .base import teardown as base_teardown

log = logging.getLogger(__name__)


def require_admin(f):
    @wraps(f)
    async def wrapper(self, interaction: discord.Interaction, *args, **kwargs):
        if not interaction.user.guild_permissions.administrator:
            await interaction.response.send_message(
                embed=discord.Embed(
                    title=_("Forbidden"),
                    description=_(
                        "This command is only available to server administrators."
                    ),
                    colour=discord.Colour.red(),
                ),
                ephemeral=True,
            )
            return
        return await f(self, interaction, *args, **kwargs)

    return wrapper


def in_voice_channel(f):
    @wraps(f)
    async def wrapper(self, interaction: discord.Interaction, *args, **kwargs):
        if interaction.user.voice is None:
            await interaction.response.send_message(
                _("You are not in a voice channel!"), ephemeral=True
            )
            return
        return await f(self, interaction, *args, **kwargs)

    return wrapper


class EventCog(BaseCog):
    async def resume(self, current_events: list[models.Event]):
        connected_channels = set(vc.channel.id for vc in self.bot.voice_clients)
        for event in current_events:
            channel = self.bot.get_channel(event.channel_id)
            if channel is None:
                continue
            self.subscribe(channel=channel, event_code=event.code)
            if channel.id in connected_channels:
                continue
            await connect_if_permitted(self.bot, channel)

    async def _check_no_existing_event(self, channel_id):
        existing = await self.bot.get_event_in_channel(channel_id)
        if existing is not None:
            raise InteractionError(
                _("This channel is already linked to event {code}: {url}").format(
                    code=existing.code,
                    url=f"{settings.INSTANCE_WEB_URL}/e/{existing.code}",
                )
            )

    async def _register_event(self, interaction: discord.Interaction, event_manager):
        await self.bot.register_event_manager(event_manager, interaction.channel_id)
        self.subscribe(channel=interaction.channel, event_code=event_manager.event.code)
        await connect_if_permitted(self.bot, interaction.channel)

    async def _confirm_connection(self, channel: discord.abc.GuildChannel):
        event_manager = self.bot.get_event_manager_in_channel(channel.id)
        message = str(
            _(
                "In this channel, participants will speak one after another by asking for a turn "
                "with the `/turn` command.\nBut first, please register with the `/register` "
                "command."
            )
        )
        if event_manager.event.turnMaxDuration:
            message += "\n" + str(
                _("Speaking turns will be limited to {duration}.").format(
                    duration=duration_to_text(event_manager.event.turnMaxDuration)
                )
            )
        embed = discord.Embed(
            title=_("Speaking List activated!"),
            description=message,
            colour=discord.Color.purple(),
        )
        await channel.send(embed=embed)

    async def leave_event(self, channel_id: int):
        event_manager = self._get_event_manager_or_fail(channel_id)
        vc = self.bot.get_voice_client(channel_id)
        if vc is not None:
            await vc.disconnect()
        self.unsubscribe(event_code=event_manager.event.code)
        await self.bot.unregister_event_manager(event_manager)
        async with self.bot.db_manager.Session() as session:
            event = await models.Event.get_in_channel(session, channel_id)
            await session.delete(event)
            await session.commit()

    @app_commands.command(name=_("create"))
    @app_commands.describe(title=_("The event's title"))
    @set_lang
    @require_admin
    # @in_voice_channel
    async def create(self, interaction: discord.Interaction, title: str):
        """Create a new event"""
        log.info(f"Event creation request on channel {interaction.channel}")
        await self._check_no_existing_event(interaction.channel_id)
        check_connect_permitted(self.bot, interaction.channel)
        user = await self.bot.get_sl_user()
        event_manager = await EventManager.create(
            title, self.bot.db_manager, self.bot.api_client, user
        )
        async with self.bot.db_manager.Session() as session:
            event = models.Event(
                user_id=event_manager.user.id,
                code=event_manager.event_code,
                channel_id=interaction.channel_id,
                locale=interaction.locale.value,
            )
            session.add(event)
            await session.commit()

        await self._register_event(interaction, event_manager)

        url = f"{settings.INSTANCE_WEB_URL}/e/{event.code}/admin"
        embed = discord.Embed(
            title=_("Event created!").format(code=event.code),
            description=_(
                "The event {title} has been created and connected to channel {channel}.\n"
                "To get to the organizer page, click on the link below, and enter "
                "your admin code.\n{url}"
            ).format(
                title=event_manager.event.title,
                code=event.code,
                channel=interaction.channel.name,
                url=url,
            ),
            url=url,
            colour=discord.Color.green(),
        )
        embed.add_field(name=_("Event code"), value=event_manager.event.code)
        embed.add_field(name=_("Admin code"), value=event_manager.event.adminCode)
        await interaction.response.send_message(embed=embed, ephemeral=True)
        await self._confirm_connection(interaction.channel)

    @app_commands.command(
        name=_("connect"),
        description=_("Connect to an existing event"),
    )
    @app_commands.describe(
        event_code=_("The event code (4 characters)"),
        admin_code=_("The event admin code (6 characters)"),
    )
    @set_lang
    @require_admin
    # @in_voice_channel
    async def connect(
        self, interaction: discord.Interaction, event_code: str, admin_code: str
    ):
        event_code = event_code.upper()
        await self._check_no_existing_event(interaction.channel_id)
        check_connect_permitted(self.bot, interaction.channel)
        user = await self.bot.get_sl_user()
        event_manager = await EventManager.connect(
            event_code, admin_code, self.bot.db_manager, self.bot.api_client, user
        )
        async with self.bot.db_manager.Session() as session:
            event = models.Event(
                user_id=event_manager.user.id,
                code=event_code,
                channel_id=interaction.channel_id,
                locale=interaction.locale.value,
            )
            session.add(event)
            await session.commit()

        await self._register_event(interaction, event_manager)

        url = f"{settings.INSTANCE_WEB_URL}/e/{event.code}/admin"
        embed = discord.Embed(
            title=_("Event connected!"),
            description=_(
                "The event {title} ({code}) has been connected to channel {channel}.\n"
                "To get to the organizer page, click on the link below.\n{url}"
            ).format(
                title=event_manager.event.title,
                code=event.code,
                channel=interaction.channel.name,
                url=url,
            ),
            url=url,
            colour=discord.Color.green(),
        )
        await interaction.response.send_message(embed=embed, ephemeral=True)
        await self._confirm_connection(interaction.channel)

    @app_commands.command(
        name=_("disconnect"),
        description=_("Disconnect the event from this channel"),
    )
    @set_lang
    @require_admin
    # @in_voice_channel
    async def disconnect(self, interaction: discord.Interaction):
        event = await self.bot.get_event_in_channel(interaction.channel_id)
        await self.leave_event(interaction.channel_id)
        embed = discord.Embed(
            title=_("Event disconnected!"),
            description=_(
                "The event {code} is no longer connected to channel {channel}."
            ).format(code=event.code, channel=interaction.channel.name),
            url=f"{settings.INSTANCE_WEB_URL}/e/{event.code}",
            colour=discord.Color.green(),
        )
        await interaction.response.send_message(embed=embed, ephemeral=True)

    def subscribe(self, channel: discord.abc.GuildChannel, event_code: str):
        self.bg.run(
            self._subscribe(channel=channel, event_code=event_code),
            name=f"sub:{event_code}",
        )
        for cogname in ("TurnCog", "RegisterCog"):
            cog = self.bot.get_cog(cogname)
            if cog is not None:
                cog.subscribe_to_event(event_code)

    def unsubscribe(self, event_code: str):
        self.bg.cancel_named(f"sub:{event_code}")
        for cogname in ("TurnCog", "RegisterCog"):
            cog = self.bot.get_cog(cogname)
            if cog is not None:
                cog.unsubscribe_from_event(event_code)

    async def _subscribe(self, channel: discord.abc.GuildChannel, event_code: str):
        broadcast_channel = EventChange.broadcast_channel_for(code=event_code)
        async with self.bot.broadcaster.subscribe(
            channel=f"{settings.BROADCAST_PREFIX}{broadcast_channel}"
        ) as subscriber:
            async for msg in subscriber:
                log.debug(f"Event change message: {msg.message!r}")
                change = EventChange.load(json.loads(msg.message))
                async with self.bot.db_manager.Session() as db:
                    locale = await models.Event.get_locale_of(db, code=event_code)
                with use_locale(locale):
                    if change.action == "start":
                        await channel.send(
                            _("The event has started, speaking turns are now accepted.")
                        )
                        # await message.pin()
                    elif change.action == "pause":
                        await channel.send(
                            _(
                                "The event is paused, speaking turns aren't accepted until it "
                                "is resumed."
                            )
                        )
                    elif change.action == "unpause":
                        await channel.send(
                            _(
                                "The event is resumed, speaking turns are accepted again."
                            )
                        )
                    elif change.action == "stop":
                        await channel.send(
                            _(
                                "The event has ended, speaking turns are no longer accepted."
                            )
                        )
                    elif change.action == "restart":
                        await channel.send(
                            _(
                                "The event has restarted, speaking turns are accepted again."
                            )
                        )
                    if change.action == "delete":
                        await channel.send(
                            _(
                                "The event has been deleted, all information about it and "
                                "its participants is gone."
                            )
                        )
                        await self.leave_event(channel.id)
                        break


async def setup(bot):
    await bot.add_cog(EventCog(bot))


async def teardown(bot):
    await base_teardown(bot, "EventCog")


async def resume(bot, current_events: list[models.Event]):
    await base_resume(bot, current_events, "EventCog")
