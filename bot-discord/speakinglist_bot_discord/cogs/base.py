import logging
from functools import wraps

import discord
from discord.ext import commands

from .. import models
from ..background import BackgroundTasksManager
from ..i18n import gettext_str as _
from ..i18n import use_locale
from ..utilities import InteractionError

log = logging.getLogger(__name__)


async def _find_locale_in_args(self: commands.Cog, args, kwargs):
    interaction: discord.Interaction = kwargs.get("interaction", args[0])
    if interaction is not None:
        return interaction.locale
    event_code: str = kwargs.get("event_code")
    if event_code is None:
        return None
    async with self.bot.db_manager.Session() as db:
        return await models.Event.get_locale_of(db, code=event_code)


def set_lang(f):
    @wraps(f)
    async def wrapper(self: commands.Cog, *args, **kwargs):
        locale = await _find_locale_in_args(self, args, kwargs)
        with use_locale(locale):
            return await f(self, *args, **kwargs)

    return wrapper


class BaseCog(commands.Cog):
    def __init__(self, bot: commands.Bot):
        super().__init__()
        self.bot = bot
        self.bg = BackgroundTasksManager()

    async def stop(self):
        self.bg.stop()

    async def resume(self, current_events: list[models.Event]):
        ...

    @set_lang
    async def cog_app_command_error(
        self,
        interaction: discord.Interaction,
        error: discord.app_commands.AppCommandError,
    ):
        log.exception(error)
        if isinstance(error, discord.app_commands.errors.CommandInvokeError):
            message = str(error.original)
        else:
            message = str(error)
        embed = discord.Embed(
            title=_("Error"),
            description=message,
            colour=discord.Colour.red(),
        )
        try:
            await interaction.response.send_message(
                embed=embed,
                ephemeral=True,
            )
        except discord.InteractionResponded:
            await interaction.followup.send(embed=embed, ephemeral=True)

    def _get_event_manager_or_fail(self, channel_id, user=None):
        event_manager = self.bot.get_event_manager_in_channel(channel_id)
        if event_manager is None:
            message = _(
                "Error: this channel is not connected to a Speaking List event."
            )
            if user and user.guild_permissions.administrator:
                message += " " + _("Use the `/create` or `/connect` commands first.")
            raise InteractionError(message)
        return event_manager


async def teardown(bot, name):
    cog = await bot.get_cog(name)
    if cog is None:
        return
    await cog.stop()
    await bot.remove_cog(name)


async def resume(bot, current_events: list[models.Event], name):
    cog = bot.get_cog(name)
    if cog is None:
        return
    await cog.resume(current_events)


class LazyButton(discord.ui.Button):
    """A button capable of lazy-evaluating the label field."""

    def __init__(self, *args, **kwargs):
        try:
            kwargs["label"] = str(kwargs["label"])
        except KeyError:
            pass
        super().__init__(*args, **kwargs)


class ButtonWithCallback(LazyButton):
    def __init__(self, *args, **kwargs):
        self.callback = kwargs.pop("callback")
        super().__init__(*args, **kwargs)
