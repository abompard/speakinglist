import json
import logging
from functools import partial
from typing import Optional, Union

import discord
from discord import app_commands

from .. import models, settings
from ..event_manager import Participant, ParticipantChange, SocialCategory
from ..i18n import gettext_str as _
from ..utilities import get_participant
from .base import BaseCog, ButtonWithCallback
from .base import resume as base_resume
from .base import set_lang

log = logging.getLogger(__name__)


class SocialCategorySelect(discord.ui.Select):
    name = None

    def __init__(self, update: Union[models.Participant, False], *args, **kwargs):
        kwargs["options"].append(
            discord.SelectOption(label=_("No answer"), value=""),
        )
        self._set_default(update, kwargs["options"])
        for option in kwargs["options"]:
            option.label = str(option.label)
        if "placeholder" in kwargs:
            kwargs["placeholder"] = str(kwargs["placeholder"])
        super().__init__(*args, **kwargs)

    def _set_default(self, update, options):
        if not update:
            return
        for option in options:
            if update.get(self.name) == option.value:
                option.default = True

    async def callback(self, interaction):
        await self.view.set_value(("social_categories", self.name), self.values[0])
        await interaction.response.defer(ephemeral=True)


class GenderSelect(SocialCategorySelect):
    name = "gender"

    def __init__(self, *args, **kwargs):
        kwargs["placeholder"] = _("Your gender...")
        kwargs["options"] = [
            discord.SelectOption(label=_("Man"), value="man"),
            discord.SelectOption(label=_("Woman"), value="woman"),
            discord.SelectOption(label=_("Non Binary"), value="nb"),
        ]
        super().__init__(*args, **kwargs)


class RaceSelect(SocialCategorySelect):
    name = "race"

    def __init__(self, *args, **kwargs):
        kwargs["placeholder"] = _("Your racialization...")
        kwargs["options"] = [
            discord.SelectOption(label=_("White"), value="white"),
            discord.SelectOption(label=_("Person of Color"), value="poc"),
        ]
        super().__init__(*args, **kwargs)


SOCIAL_CATEGORY_SELECTS = [GenderSelect, RaceSelect]


class NameInput(discord.ui.TextInput):
    def __init__(self, *args, **kwargs):
        kwargs["label"] = str(_("Name"))
        super().__init__(*args, **kwargs)

    async def callback(self, interaction):
        await self.view.set_value("name", self.value)
        await interaction.response.defer(ephemeral=True)


class RegisterView(discord.ui.View):
    def __init__(
        self,
        social_categories: list[SocialCategory],
        ask_name,
        update: Optional[Participant],
        callback,
    ):
        super().__init__()
        self.callback = callback
        self.values = {}
        if ask_name:
            self.add_item(NameInput())
        selects = {cls.name: cls for cls in SOCIAL_CATEGORY_SELECTS}
        for sc in social_categories:
            self.add_item(
                selects[sc.name](update=update.social_categories if update else None)
            )
        self.submit_button = ButtonWithCallback(
            label=_("Edit").lazy if update else _("Register").lazy,
            style=discord.ButtonStyle.primary,
            callback=self.submit,
        )
        self.add_item(self.submit_button)

    async def set_value(self, key, value):
        if isinstance(key, str):
            self.values[key] = value
        else:
            values = self.values
            key = list(key)
            while len(key) > 1:
                keyitem = key.pop(0)
                if keyitem not in values:
                    values[keyitem] = {}
                values = values[keyitem]
            values[key[0]] = value
        # self.submit_button.disabled = not self.values

    async def submit(self, interaction):
        await self.callback(interaction, self.values)


class RegisterCog(BaseCog):
    @app_commands.command(name=_("register"), description=_("Register for the event"))
    @set_lang
    async def register(self, interaction: discord.Interaction):
        log.info(f"Registration request for user {interaction.user.display_name}")
        event_manager = self._get_event_manager_or_fail(interaction.channel_id)
        existing = await get_participant(self, interaction)
        if existing is not None:
            await self._register_existing(interaction, event_manager, existing)
        else:
            await self._register(interaction, event_manager)

    async def _register_existing(self, interaction, event_manager, existing):
        intro = _("You are already registered in this event.")
        social_categories = await event_manager.get_social_categories()
        if social_categories:
            form = discord.ui.View()
            form.add_item(
                ButtonWithCallback(
                    label=str(_("Edit registration")),
                    style=discord.ButtonStyle.primary,
                    callback=partial(self._register, existing=existing),
                )
            )
        else:
            form = None
        # We can't ask for text (the name) in views yet, textinputs only work on modals
        # form.add_item(
        #     ButtonWithCallback(
        #         label="Register someone else",
        #         style=discord.ButtonStyle.secondary,
        #         callback=partial(self._register, ask_name=True)
        #     )
        # )
        await interaction.response.send_message(intro, view=form, ephemeral=True)

    async def _register(
        self,
        interaction: discord.Interaction,
        event_manager,
        ask_name=False,
        existing: Union[models.Participant, False] = False,
    ):
        social_categories = await event_manager.get_social_categories()
        if existing:
            participant = await event_manager.get_participant(existing.speakinglist_id)
            callback = partial(
                self._on_update, event_manager=event_manager, existing=participant
            )
        else:
            participant = None
            callback = partial(self._on_submit, event_manager=event_manager)
        if not social_categories:
            await self._on_submit(interaction, {}, event_manager=event_manager)
            return
        form = RegisterView(
            social_categories,
            ask_name,
            update=participant,
            callback=callback,
        )
        intro = _(
            "Please tell us a few things about yourself. We use this information to "
            "make the discussion more inclusive."
        )
        if existing:
            await interaction.response.edit_message(content=intro, view=form)
        else:
            await interaction.response.send_message(intro, view=form, ephemeral=True)

    @set_lang
    async def _on_submit(self, interaction, values, *, event_manager):
        log.info(f"Registering {interaction.user.display_name} with {values}")
        participant = await event_manager.register_participant(
            name=values.get("name", interaction.user.display_name),
            social_categories=values.get("social_categories", {}),
        )
        async with self.bot.db_manager.Session() as db:
            db_event = await models.Event.get_one(db, code=event_manager.event.code)
            db_participant = models.Participant(
                event_id=db_event.id,
                discord_id=interaction.user.id,
                speakinglist_id=participant.id,
            )
            db.add(db_participant)
            await db.commit()
        embed = discord.Embed(
            title=_("You are registered!"), colour=discord.Colour.green()
        )
        if values.get("social_categories"):
            await interaction.response.edit_message(
                content=None,
                view=None,
                embed=embed,
                delete_after=10,
            )
        else:
            # No form was shown, so no response has been emitted yet
            await interaction.response.send_message(
                content=None, embed=embed, ephemeral=True, delete_after=10
            )

    @set_lang
    async def _on_update(
        self, interaction, values, *, event_manager, existing: Participant
    ):
        log.info(f"Updating registration for {existing} with {values}")
        await event_manager.update_participant(
            participant_id=existing.id,
            name=values.get("name", interaction.user.display_name),
            social_categories=values.get("social_categories"),
        )
        await interaction.response.edit_message(
            content=None,
            view=None,
            embed=discord.Embed(
                title=_("Your changes have been recorded!"),
                colour=discord.Colour.green(),
            ),
            delete_after=10,
        )

    def subscribe_to_event(self, event_code: str):
        self.bg.run(self._subscribe_to_event(event_code), name=f"event:{event_code}")

    def unsubscribe_from_event(self, event_code: str):
        self.bg.cancel_named(f"event:{event_code}")

    async def _subscribe_to_event(
        self,
        event_code: str,
    ):
        broadcast_channel = ParticipantChange.broadcast_channel_for(
            event_code=event_code
        )
        async with self.bot.broadcaster.subscribe(
            channel=f"{settings.BROADCAST_PREFIX}{broadcast_channel}"
        ) as subscriber:
            async for msg in subscriber:
                log.debug(f"Participant message: {msg.message!r}")
                change = ParticipantChange.load(json.loads(msg.message))
                if change.action != "delete":
                    continue
                async with self.bot.db_manager.Session() as db:
                    db_participant = await models.Participant.get_one(
                        db,
                        speakinglist_id=change.speakinglist_id,
                    )
                    await db.delete(db_participant)
                    await db.commit()


async def setup(bot):
    await bot.add_cog(RegisterCog(bot))


async def resume(bot, current_events: list[models.Event]):
    await base_resume(bot, current_events, "RegisterCog")
