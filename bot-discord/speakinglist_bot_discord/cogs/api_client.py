import asyncio
import logging

from discord.ext import commands

from ..api_client import APIClient
from .base import BaseCog
from .base import teardown as base_teardown

log = logging.getLogger(__name__)


class APIClientCog(BaseCog):
    def __init__(self, bot: commands.Bot):
        super().__init__(bot=bot)
        if not hasattr(self.bot, "api_client"):
            self.bot.api_client = None
            self.bot._api_client_connected = asyncio.get_running_loop().create_future()
        self.bot.add_listener(self.on_bot_ready, "on_ready")
        self.bot.add_listener(self.on_bot_connect, "on_connect")
        self.bot.add_listener(self.on_bot_connect, "on_resumed")
        self.bot.add_listener(self.on_bot_disconnect, "on_disconnect")

    async def on_bot_ready(self):
        if self.bot.api_client is not None:
            return
        user = await self.bot.get_sl_user()
        self.bot.api_client = APIClient(token=user.token if user is not None else None)
        await self.on_bot_connect()
        self.bot._api_client_connected.set_result(None)

    async def on_bot_connect(self):
        if self.bot.api_client is None:
            return
        await self.bot.api_client.connect()
        log.info("APIClient connected")

    async def on_bot_disconnect(self):
        if self.bot.api_client is None:
            return
        await self.bot.api_client.disconnect()
        log.info("APIClient disconnected")


async def setup(bot):
    await bot.add_cog(APIClientCog(bot))


async def teardown(bot):
    await base_teardown(bot, "APIClientCog")
