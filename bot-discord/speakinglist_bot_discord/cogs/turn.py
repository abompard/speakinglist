import asyncio
import json
import logging
from datetime import datetime
from functools import partial
from typing import ClassVar, Optional

import discord
from discord import app_commands
from sqlalchemy import select

from .. import models, settings
from ..api_client import ServerError
from ..event_manager import TurnChange
from ..i18n import gettext_str as _
from ..utilities import InteractionError, duration_to_text, get_member_in_channel
from .base import BaseCog, ButtonWithCallback
from .base import resume as base_resume
from .base import set_lang
from .base import teardown as base_teardown

log = logging.getLogger(__name__)


RAISED_HAND_EMOJI = "✋"
# RAISED_HAND_NICK_PREFIX = f".{RAISED_HAND_EMOJI}"
RAISED_HAND_NICK_PREFIX = f"{RAISED_HAND_EMOJI} "


class CancelTurnView(discord.ui.View):
    def __init__(self, on_cancel):
        super().__init__()
        self.add_item(
            ButtonWithCallback(
                label=_("Cancel turn").lazy,
                style=discord.ButtonStyle.danger,
                callback=on_cancel,
            )
        )


class TurnCog(BaseCog):
    sounds: ClassVar[dict] = {}

    async def resume(self, current_events: list[models.Event]):
        for event in current_events:
            # await self.subscribe_to_event(event.code)  # Done by the event cog
            await self.subscribe_to_pending_turns(event.code)

    # @app_commands.command(name=_("t"), description=_("Ask for a speaking turn"))
    # async def turn_alias(self, interaction: discord.Interaction):
    #     return await self.turn(interaction=interaction)

    @app_commands.command(name=_("turn"), description=_("Ask for a speaking turn"))
    @set_lang
    async def turn(self, interaction: discord.Interaction):
        log.info(f"Requesting turn for {interaction.user.display_name}")
        event_manager = self._get_event_manager_or_fail(interaction.channel_id)
        async with self.bot.db_manager.Session() as db:
            result = await db.execute(
                select(models.Participant)
                .join(models.Event)
                .where(
                    models.Event.code == event_manager.event.code,
                    models.Participant.discord_id == interaction.user.id,
                )
            )
            participant = result.scalar_one_or_none()
        if participant is None:
            raise InteractionError(
                _("You must register first! Use the `/register` command.")
            )
        message = _("You have been added to the speaking list.")
        try:
            turn = await event_manager.request_turn(
                participant_id=participant.speakinglist_id
            )
        except ServerError as e:
            if e.message == "EXISTING_PENDING_TURN":
                message = _("You already have a pending turn.")
                turn = await event_manager.get_pending_turn(
                    participant_id=participant.speakinglist_id
                )
                if turn is None:
                    raise InteractionError(
                        _("You already have a pending turn but I can't get it, sorry.")
                    ) from e
            elif e.message == "EVENT_NOT_RUNNING":
                raise InteractionError(
                    _("The event is not accepting turn requests at this time.")
                ) from e
            else:
                raise
        else:
            # This is a new request (no pending turn exist yet)
            self.subscribe(
                interaction,
                event_code=event_manager.event.code,
                member_id=interaction.user.id,
                turn_id=turn.id,
            )
            await self._edit_name(interaction.user, True)

        await interaction.response.send_message(
            message,
            view=CancelTurnView(
                partial(
                    self._on_cancel,
                    event_manager=event_manager,
                    participant=participant,
                    turn_id=turn.id,
                )
            ),
            ephemeral=True,
        )

    @set_lang
    async def _on_cancel(self, interaction, event_manager, participant, turn_id):
        await event_manager.cancel_turn(
            participant_id=participant.speakinglist_id, turn_id=turn_id
        )
        await interaction.response.defer(ephemeral=True)
        await self._edit_flash(
            interaction, _("Your turn has been successfully cancelled.")
        )
        await self._edit_name(interaction.user, False)

    @set_lang
    async def _edit_flash(self, interaction, message):
        if not interaction:
            return
        try:
            await interaction.edit_original_response(content=message, view=None)
        except discord.errors.NotFound:
            pass
        else:
            loop = asyncio.get_running_loop()
            loop.call_later(
                30, loop.create_task, interaction.delete_original_response()
            )

    async def _edit_name(self, user, raised=False):
        try:
            if raised and not user.display_name.startswith(RAISED_HAND_NICK_PREFIX):
                await user.edit(
                    nick=RAISED_HAND_NICK_PREFIX + user.display_name,
                    reason="User raised their hand",
                )
            if raised is False and user.display_name.startswith(
                RAISED_HAND_NICK_PREFIX
            ):
                await user.edit(
                    nick=user.display_name[len(RAISED_HAND_NICK_PREFIX) :],
                    reason="User lowered their hand",
                )
        except discord.errors.Forbidden as e:
            # Bots can't change the nick of server admins.
            if not user.guild_permissions.administrator:
                log.exception(e)

    def subscribe(
        self,
        interaction: Optional[discord.Interaction],
        event_code: str,
        member_id: int,
        turn_id: int,
    ):
        self.bg.run(
            self._subscribe(
                interaction,
                event_code=event_code,
                member_id=member_id,
                turn_id=turn_id,
            )
        )

    @set_lang
    async def _subscribe(
        self,
        interaction: Optional[discord.Interaction],
        event_code: str,
        member_id: int,
        turn_id: int,
    ):
        if interaction:
            channel = interaction.channel
        else:
            async with self.bot.db_manager.Session() as db:
                channel_id = await models.Event.get_channel_of(db, event_code)
            channel = self.bot.get_channel(channel_id)
        member = get_member_in_channel(channel, discord_id=member_id)

        broadcast_channel = TurnChange.broadcast_channel_for(
            event_code=event_code, turn_id=turn_id
        )
        # WARNING: I only get notifications for events *I did not initiate*.
        # It's on purpose on the API side.
        async with self.bot.broadcaster.subscribe(
            channel=f"{settings.BROADCAST_PREFIX}{broadcast_channel}"
        ) as subscriber:
            async for msg in subscriber:
                log.debug(f"Turn change message: {msg.message!r}")
                change = TurnChange.load(json.loads(msg.message))
                if change.action == "start":
                    await self._edit_flash(interaction, _("The turn has started."))
                    if member is None:
                        log.warning(
                            f"Turn of member {member_id} has started but they are no longer in channel {channel.name}"
                        )
                    else:
                        # Participant is in the channel
                        await self._edit_name(member, False)
                        await channel.send(
                            _(
                                "{mention}: your turn has started, the floor is yours!"
                            ).format(mention=member.mention),
                            delete_after=30,
                        )
                    self.track_turn(channel, member_id, change.turn_id)
                elif change.action == "stop":
                    await self._edit_flash(interaction, _("The turn has ended."))
                    self.bg.cancel_named(f"track:{channel.id}:{change.turn_id}")
                    bot_member = channel.guild.get_member(self.bot.user.id)
                    if bot_member:
                        await bot_member.edit(nick=None)
                    break
                elif change.action == "cancel":
                    await self._edit_flash(
                        interaction, _("The turn has been cancelled.")
                    )
                    if member:
                        await self._edit_name(member, False)
                    break

    def subscribe_to_event(self, event_code: str):
        self.bg.run(
            self.subscribe_to_external_requests(event_code), name=f"event:{event_code}"
        )

    def unsubscribe_from_event(self, event_code: str):
        self.bg.cancel_named(f"event:{event_code}")

    async def subscribe_to_external_requests(
        self,
        event_code: str,
    ):
        broadcast_channel = TurnChange.broadcast_channel_for(event_code=event_code)
        async with self.bot.broadcaster.subscribe(
            channel=f"{settings.BROADCAST_PREFIX}{broadcast_channel}"
        ) as subscriber:
            async for msg in subscriber:
                log.debug(f"Turn request message: {msg.message!r}")
                change = TurnChange.load(json.loads(msg.message))
                if change.action != "request":
                    continue
                async with self.bot.db_manager.Session() as db:
                    participant = (
                        await models.Participant.get_from_speakinglist_id_in_event(
                            db, change.speakinglist_id, event_code
                        )
                    )
                self.subscribe(
                    interaction=None,
                    event_code=event_code,
                    member_id=participant.discord_id,
                    turn_id=change.turn_id,
                )

    async def subscribe_to_pending_turns(self, event_code: str):
        async with self.bot.db_manager.Session() as db:
            channel_id = await models.Event.get_channel_of(db, event_code)
        event_manager = self.bot.get_event_manager_in_channel(channel_id)
        if event_manager is None:
            log.error(
                "Could not find the event manager for {event_code} in {channel_id}"
            )
            return
        async for turn in event_manager.get_pending_turns():
            async with self.bot.db_manager.Session() as db:
                participant = (
                    await models.Participant.get_from_speakinglist_id_in_channel(
                        db, turn.participantId, channel_id
                    )
                )
            self.subscribe(
                interaction=None,
                event_code=event_code,
                member_id=participant.discord_id,
                turn_id=turn.id,
            )

    def track_turn(self, channel, member_id, turn_id):
        self.bg.run(
            self._track_turn(channel, member_id, turn_id),
            name=f"track:{channel.id}:{turn_id}",
        )

    async def _track_turn(self, channel, member_id, turn_id):
        member = channel.guild.get_member(member_id)
        async with self.bot.db_manager.Session() as session:
            participant = await models.Participant.get_from_discord_id_in_channel(
                session, member.id, channel.id
            )
        event_manager = self.bot.get_event_manager_in_channel(channel.id)
        if event_manager is None:
            return
        turn = await event_manager.get_turn(participant.speakinglist_id, turn_id)
        bot_member = channel.guild.get_member(self.bot.user.id)
        is_only_event_in_guild = self.bot.is_only_event_in_guild(channel)
        max_duration = event_manager.event.turnMaxDuration
        warn_duration = max_duration - max_duration / 5
        _over_warn_played = False
        _over_max_played = False
        while True:
            duration = int(
                (
                    datetime.now() - datetime.fromisoformat(turn.startedAt)
                ).total_seconds()
            )
            # Discord lets us change the nick 20 times every 5 minutes: we can change it at most every 15s.
            if is_only_event_in_guild and duration % 15 == 0:
                await bot_member.edit(
                    nick=_("Stopwatch: {value}").format(
                        value=duration_to_text(duration)
                    )
                )
            if duration > warn_duration and not _over_warn_played:
                _over_warn_played = True
                await self.play(channel, "ding")
            if duration > max_duration and not _over_max_played:
                _over_max_played = True
                await self.play(channel, "ding-ding")
            await asyncio.sleep(1)

    def load_sounds(self):
        for filepath in settings.RESOURCES_DIR.iterdir():
            if filepath.suffix != ".webm":
                continue
            self.sounds[filepath.stem] = filepath.as_posix()

    async def play(self, channel: discord.abc.GuildChannel, soundname: str):
        if channel.type != discord.ChannelType.voice:
            return
        client = self.bot.get_voice_client(channel.id)
        if client is None:
            client = await channel.connect()
        sound = await discord.FFmpegOpusAudio.from_probe(self.sounds[soundname])
        if client.is_playing():
            client.stop()
        client.play(sound)


async def setup(bot):
    cog = TurnCog(bot)
    cog.load_sounds()
    await bot.add_cog(cog)


async def teardown(bot):
    await base_teardown(bot, "TurnCog")


async def resume(bot, current_events: list[models.Event]):
    await base_resume(bot, current_events, "TurnCog")
