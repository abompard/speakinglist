#!/usr/bin/env python

"""Extract, update and compile translations.

To add a new language, run:
    poetry run pybabel init -i speakinglist_bot_discord/locale/messages.pot -l LANGCODE -w 100 -d speakinglist_bot_discord/locale/

"""

from importlib.metadata import metadata
from subprocess import run

SOURCEDIR = "speakinglist_bot_discord"
DESTDIR = "speakinglist_bot_discord/locale"
md = metadata("speakinglist-bot-discord")

# Extract
run(
    [
        "pybabel",
        "extract",
        "-o", f"{DESTDIR}/messages.pot",
        "-k", "locale_str",
        "-w", "100",
        "--project", md["Name"],
        "--version", md["Version"],
        "--copyright-holder", f"{md['Author']} <{md['Author-email']}>",
        SOURCEDIR,
    ]
)

# Update
run(
    [
        "pybabel",
        "update",
        "-i", f"{DESTDIR}/messages.pot",
        "-d", DESTDIR,
        "-w", "100",
        SOURCEDIR,
    ]
)

# Compile
run(
    [
        "pybabel",
        "compile",
        "--statistics",
        "-d", DESTDIR,
    ]
)
